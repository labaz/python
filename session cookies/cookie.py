import requests
import datetime
import sys
from termcolor import colored
from bs4 import BeautifulSoup

if len(sys.argv) > 1:
  url = sys.argv[1]
else:
  print("first argument must be the URL")
  sys.exit()

print(colored((url),'magenta'))

# Send a GET request to the webpage
response = requests.get(url)

# Access the response cookies
cookies = response.cookies

print(f'cookies:', colored(len(cookies),'green'))

# sys.exit()

# Iterate over the cookies and print their attributes
i = 1
for cookie in cookies:
    print(colored(f'> == {i} == <','cyan'))
    print(f"Name: {cookie.name}")
    print(f"Value: {cookie.value}")
    print(f"Domain: {cookie.domain}")
    print(f"Path: {cookie.path}")
    print(f"Secure: {cookie.secure}")
    # print(f"HttpOnly: {cookie.is_httponly}")
    expires_datetime = None
    if cookie.expires:
      expires_datetime = datetime.datetime.fromtimestamp(cookie.expires)
    
    print(f"Expiration: {expires_datetime}")
    
    i = i + 1
    
# Parse the HTML content using BeautifulSoup
soup = BeautifulSoup(response.content, 'html.parser')

# Find all form elements on the page
forms = soup.find_all('form')

# Iterate over the forms and extract form elements
for form in forms:
    # Get the form's action URL
    action = form.get('action')

    # Get all input elements within the form
    inputs = form.find_all('input')

    # Extract information from each input element
    for input_elem in inputs:
        # Get the input's name attribute
        name = input_elem.get('name')

        # Get the input's type attribute
        input_type = input_elem.get('type')

        # Perform further processing as needed
        # ...

        # Print the extracted information
        print(f"Form Action: {action}")
        print(f"Input Name: {name}")
        print(f"Input Type: {input_type}")
        print("--------")