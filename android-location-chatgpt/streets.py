
import subprocess
import json
import time

cache_lifetime=20

def get_from_cache(name, cache_lifetime):
    # Check the cache for the named object
    cache_filename = f"{name}_cache.json"
    try:
        with open(cache_filename, "r") as f:
            cache_data = json.load(f)
        cache_time = cache_data.get("timestamp", 0)
        obj = cache_data.get("data")
        if time.time() - cache_time < cache_lifetime and obj:
            # Return the cached object
            print(f"Using cached {name}.")
            return obj
    except FileNotFoundError:
        pass

    # No cached object found
    return None

def cache_object(name, obj):
    # Cache the named object
    cache_filename = f"{name}_cache.json"
    cache_data = {"timestamp": time.time(), "data": obj}
    with open(cache_filename, "w") as f:
        json.dump(cache_data, f)

def get_device_location(cache_lifetime=60):
    # Check the cache for the device location
    location = get_from_cache("location", cache_lifetime)
    if location:
        # Return the cached location
        print(f"Using cached location: Latitude: {location[0]}, Longitude: {location[1]}")
        return location

    # Define the Termux command to get the location
    termux_command = "termux-location -p network"

    # Execute the command and capture the output
    output = subprocess.check_output(termux_command.split()).decode().strip()

    # Parse the JSON output into a Python dictionary
    location_data = json.loads(output)

    # Extract the latitude and longitude from the location data
    lat = location_data.get("latitude")
    lon = location_data.get("longitude")

    if lat and lon:
        # Cache the device location
        cache_object("location", (lat, lon))

        # Print the device location
        print(colored(f"Device location: Latitude: {lat}, Longitude: {lon}", "cyan"))

        # Return the latitude and longitude as a tuple
        return (lat, lon)
    else:
        # Failed to get the device location
        print("Failed to get the device location.")
        return None

from termcolor import colored

import requests
import json

def get_city(lat,lon):
  # get city (area with admin_level 8)
  q = f"""
  [out:json];
  is_in({lat},{lon});
  area._[admin_level="8"];
  out;
  """
  
  data = {
          'data': q
      }
  
  # print(q)
  
  # Send the query to the Overpass API and get the response
  response = requests.post(overpass_url, data=data)
  
  # Parse the JSON response
  response_data = json.loads(response.text)
  #respons
  j = json.dumps(response_data, indent = 2)
  highlighted_str = highlight(j, lexers.JsonLexer(), formatters.TerminalFormatter())
  # print(highlighted_str)
  
  # get the city
  city = response_data["elements"][0]["tags"]["name"]
  print(city)
  return city 

overpass_url = "https://overpass-api.de/api/interpreter"

def get_street_names(lat, lon, radius=100, cache_lifetime=60):
    # Define the URL for the Overpass API JSON endpoint

    # Build the Overpass query
    query = f"""[out:json];
                way["name"]["highway"](around:{radius}, {lat}, {lon});
                out;"""

    # Encode the query as a URL parameter
    data = {
        'data': query
    }

    # Send the query to the Overpass API and get the response
    response = requests.post(overpass_url, data=data)

    j = json.loads(response.text)

    # print(json.dumps(data, indent = 2))

    # Extract the street names from the query result
    street_names = []
    for element in j['elements']:
        if element['type'] == 'way' and 'name' in element['tags'] and element['tags']['name'] not in street_names:
            street_names.append(element['tags']['name'])

    # Convert the street names list to JSON
    street_names_json = json.dumps(street_names)

    # Return the JSON string
    return sorted(street_names)

import openai
import os

openai.api_key = os.environ.get('openai_api_key')

def get_city(lat,lon):
  # get city (city is area with admin_level 8)
  q = f"""
  [out:json];
  is_in({lat},{lon});
  area._[admin_level="8"];
  out;
  """
  
  data = {
          'data': q
      }
  
  # Send the query to the Overpass API and get the response
  response = requests.post(overpass_url, data=data)
  
  # Parse the JSON response
  response_data = json.loads(response.text)
  
  city = response_data["elements"][0]["tags"]["name"]
  print(f"The city is {city}")
  return city 

def generate_street_history(name, city):
    prompt = f"Give the origin of the street name '{name}' in the city of {city} and its history. If it is named after a historical person, first print their years of birth (and death). If possible, also tell me in which year the street was named."
    response = openai.Completion.create(
      engine="text-davinci-002",
      prompt=prompt,
      max_tokens=1024,
      n=1,
      stop=None,
      temperature=0.5,
    )

    return response.choices[0].text.strip()

if __name__ == "__main__":
    # Get the device location
    lat, lon = get_device_location()

    # Get the street names within a radius of x meters
    radius = 200
    
    street_names = get_street_names(lat, lon, radius, cache_lifetime)
    
    city = get_city(lat,lon)
   
    for street in street_names:
      print(colored(f"{street}", "magenta"))
      print (generate_street_history(street, city), "\n")