# What's with all these street names? Let's ask openai

A simple script for travelling Android users that produces historical information about the surrounding streets: when they were named and in honor of whom. The information is sourced from openai.

## What you will need
- termux (terminal emulator for Android)
- python on Android
- openai API key - to ask questions to openai text model

## Process
1. get device location with `termux-get-location`
2. get all street names within the specified radius from the location using OpenStreetMap's Overpass API
3. ask openai chat about each of these streets and return the results

## Caveats
- openai sometimes returns factually incorrect answers or straight bs. Don't treat openai as an encyclopedic source of truth - it's __good enough__.