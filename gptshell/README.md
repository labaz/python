# gpt from Linux shell
- uses completions API but works in a chat manner. This means, within one dialog, you can ask multiple questions.
- press enter to end the dialog.

## requires
- an API key of openai in environment variable `OPENAI_API_KEY`

## prerequisites 

```bash
# add your openai API key to environment variable 
cat "export OPENAI_API_KEY=sk-rGhhd97V6mwSZCMNsbDT3BlbkFJ4k9Vg80" >> ~/.bashrc

# for best results, make an alias
cat "alias gpt='python ~/0/git/python/gptshell/gpt.py'" >> ~/.bashrc
source ~/.bashrc
```

## run
```bash
gpt give me xpath to select all p children of body except for p class footer and p class sidebar
```

‼️ The initial prompt in the command line must not contain quotes, pipes and arrows -- or have all those escaped properly. Otherwise the shell will try to interpret these and the script will not receive the complete prompt or will not work.


![Screenshot_20230601_233329_Termux](Screenshot_20230601_233329_Termux.jpg)