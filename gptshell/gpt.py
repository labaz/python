import os
import sys
import datetime
import openai
import readline
from termcolor import colored
import re

# Retrieve the OpenAI API key from the environment variable
openai.api_key = os.getenv("OPENAI_API_KEY")

# Set up the directory to save the chat history
save_directory = "/storage/emulated/0/tmp/chat_history"

# Just a little colored formatted output to term
def format_style(text, style, do_not_reset = False):
    styles = {
        'bold': '\033[1m',
        'italic': '\033[3m',
        'underline': '\033[4m',
        'black': '\033[30m',
        'red': '\033[31m',
        'green': '\033[32m',
        'yellow': '\033[33m',
        'blue': '\033[34m',
        'magenta': '\033[35m',
        'cyan': '\033[36m',
        'white': '\033[37m',
        'gray': '\033[90m',
        'bright_gray': '\033[37;1m'
    }

    reset = '\033[0m'
    
    if do_not_reset:
      reset = ""
    
    non_alnum_char = re.search(r'\W', style).group() if re.search(r'\W', style) else ''

    if non_alnum_char:
        style_list = style.split(non_alnum_char)
    else:
        style_list = [style]

    for style_input in style_list:
        style_input = style_input.lower()
        for style_name in styles:
            if style_name.lower().startswith(style_input):
                text = styles[style_name] + text + reset

    return text


def cleanse_filename(filename):
    allowed_chars = "-_() "
    filename = re.sub(f"[^a-zA-Z0-9{allowed_chars}]", "", filename)
    return filename

def create_chat_file(prompt):
    timestamp = datetime.datetime.now().strftime("%Y%m%d %H%M%S")
    cleaned_prompt = cleanse_filename(prompt)
    filename = f"{timestamp} - {cleaned_prompt}.md"
    return os.path.join(save_directory, filename)

def save_chat_history(chat_file, chat_history):
    with open(chat_file, "w") as file:
        file.write(chat_history)

def generate_completion(prompt, chat_history):
    response = openai.Completion.create(
        engine="text-davinci-002",
        prompt=chat_history + prompt,
        max_tokens=500,
        temperature=0.3,
        n=1,
        stop=None
    )
    completion = response.choices[0].text.strip()
    return completion

def start_chat(prompt):
    chat_history = ""
    chat_file = create_chat_file(prompt)
    
    chat_history += f"{alias_user}: {prompt}\n"
    
    chat_print = format_style(alias_user + prompt, "gray italic")

    print(chat_print)

    completion = generate_completion("", prompt)
    chat_history += f"{alias_gpt}: {completion}\n"
    
    chat_print = format_style(alias_gpt, "bold green") + completion 
    
    print(chat_print)
    
    while True:
        user_input = input(format_style(alias_user, "gray italic", True))
        if user_input == "":
            break

        chat_history += f"{alias_user}: {user_input}\n"
        
        completion = generate_completion(user_input, chat_history)
        
        chat_history += f"{alias_gpt}: {completion}\n"

        chat_print = format_style(alias_gpt, "bold green") + completion 
    
        print(chat_print)

    save_chat_history(chat_file, chat_history)

# Check if the save directory exists, create it if necessary
if not os.path.exists(save_directory):
    os.makedirs(save_directory)

# Get the prompt from the command line arguments
if len(sys.argv) < 2:
    print("Please provide a prompt.")
    sys.exit(1)

alias_user = "> "
alias_gpt = "GPT: "

prompt = " ".join(sys.argv[1:])

start_chat(prompt)