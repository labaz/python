import re
import json
import os
import datetime
import time
import sqlite3
from dateutil import parser
import requests
import concurrent.futures

db_file_name = 'jobs.ch.sqlite'
current_file_dir = os.path.dirname(os.path.abspath(__file__))
subdir = ''
save_dir_path = os.path.join(current_file_dir, subdir)


# this is the main function to create tables from JSON
def insert_object_into_table(data, table_name, db_file_name, name_datatype_mapping_regex=None, pk_field_name=None, drop_table=False):
    """
    this function creates an sqlite table from json
    it's possible to process the values with a function before inserting

    # function that takes 01/11/2023 11:40 and makes 2023-11-01 11:40
    def dmyt_to_ymdt(value):
        # Parse the date string to datetime object
        dt = datetime.strptime(value, "%d/%m/%Y %H:%M")
        # Format the datetime object to the desired string format
        return dt.strftime("%Y-%m-%dT%H:%M:00")

    # include this function to process field 'createDate'
    name_datatype_mapping_regex = {
        r'salary': {'datatype': 'NUMERIC'}, 
        r'^createDate$': {'function': dmyt_to_ymdt}, 
        r'berater|verleih|bund': {'datatype': 'NUMERIC'}
    }
    """
    conn = sqlite3.connect(db_file_name)
    cursor = conn.cursor()

    drop_table_query = f"DROP TABLE IF EXISTS `{table_name}`"

    fields_in_object = data[0].keys()

    create_table_query = f"CREATE TABLE `{table_name}` ("

    # there should be a structure:
    # {field, datatype, function}
    fields_definitions = {} 
    fields_sql = []
    for field in fields_in_object:
        field_definition = {field: {}}
        datatype = 'TEXT'  # Default datatype is TEXT
        # Check if the field name matches any regex pattern in the mapping
        if name_datatype_mapping_regex:
            for pattern, mapped_datatype in name_datatype_mapping_regex.items():
                if re.search(pattern, field, flags=re.IGNORECASE):
                    datatype = mapped_datatype.get('datatype') if mapped_datatype.get('datatype') else datatype
                    field_transformation_function = mapped_datatype.get('function')
                    # apply the function
                    if field_transformation_function:
                        field_definition[field].update({'function': field_transformation_function})
                    break
        field_definition[field].update({'datatype': datatype})

        field_sql = f"`{field}` {datatype} "
        fields_sql.append(field_sql)
        fields_definitions.update(field_definition)

    create_table_query += ", \n".join(fields_sql)
    if pk_field_name:
        create_table_query += f"\n, PRIMARY KEY (`{pk_field_name}`)"
    create_table_query += ")"

    # check if table exists
    get_table_metadata_query = """
        SELECT *
        FROM pragma_table_info(?)
        """
    table_metadata = cursor.execute(get_table_metadata_query, (table_name, )).fetchall()

    table_exists = False
    if len(table_metadata) > 0:
        table_exists = True

    # print(f'table_exists: {table_exists}')

    # drop
    if drop_table:
        # print(f'{drop_table_query}')
        cursor.execute(drop_table_query)

    if not table_exists or drop_table:
        # print(f'{create_table_query}')
        cursor.execute(create_table_query)

    # re-run this after table is created
    table_metadata = cursor.execute(get_table_metadata_query, (table_name, )).fetchall()
    fields_in_db = [row[1] for row in table_metadata]

    # print('db')
    # print(fields_in_db)
    # print('object')
    # print(fields_in_object)

    # match object keys with table fields 
    insert_values = []
    for record in data:
        record_values = []
        for field in fields_in_db:
            # Check if key exists in dict, else use NULL
            if field in record:
                value = record[field]
                # check if there is a function mapping
                field_transformation_function = fields_definitions.get(field).get('function')
                if field_transformation_function:
                    value = field_transformation_function(value)
                else:
                    pass

                # Convert dict or list to JSON string
                if isinstance(value, (dict, list)):
                    value = json.dumps(value)
            else:
                value = None  # NULL value for SQLite
            record_values.append(value)
        insert_values.append(tuple(record_values))

    # ready to insert 
    # print('len(insert_values)')
    # print(len(insert_values))

    quoted_fields_in_db = [f'`{field}`' for field in fields_in_db]
    insert_statement = f"INSERT OR REPLACE INTO `{table_name}` ({', '.join(quoted_fields_in_db)}) VALUES ({', '.join(['?' for _ in fields_in_db])})"
    # print(insert_statement)

    cursor.executemany(insert_statement, insert_values)
    conn.commit()
    conn.close()

    return


def openai_complete_chat(model, messages, response_format="json_object", api_key=None):
    """
    messages = [
        {"role": "system", "content": role_system_prompt},
        {"role": "user", "content": role_user_prompt},
    ]

    """

    if not api_key:
        env_var_name = 'OPENAI_API_KEY'
        api_key = os.environ.get(env_var_name)

    if not api_key:
        raise ValueError(f"OpenAI API key should be in env var {env_var_name} but this env var not found or is empty.")

    url = "https://api.openai.com/v1/chat/completions"

    headers = {
        "Content-Type": "application/json",
        "Authorization": f"Bearer {api_key}"
    }

    payload = {
        "response_format": {"type": response_format},
        "temperature": 0.2,
        "model": model,
        "messages": messages,
    }

    response = post_with_retry(url, json=payload, headers=headers, timeout_duration=40)

    if response is None:
        return []

    if response_format == "json_object":
        json_path = 'choices[0].message.content'
        result = get_json_path(response.json(), json_path)

        result_json = json.loads(result)

        return result_json

    return response.json()


def post_with_retry(url, json, headers, timeout_duration, retry_delay=3, max_attempts=3):
    attempts = 0

    while attempts < max_attempts:
        try:
            # Attempt the POST request
            response = requests.post(url, json=json, headers=headers, timeout=timeout_duration)
            return response
        except requests.Timeout:
            # If a timeout occurs, wait for the specified delay before retrying
            print(f"Request timed out. Waiting for {retry_delay} seconds before retrying...")
            time.sleep(retry_delay)
            attempts += 1
            print(f"Retrying... Attempt {attempts}/{max_attempts}")

    # If all attempts fail, raise an exception or handle it as needed
    # raise Exception("Maximum number of attempts reached. Request failed.")
    return None


def print_time():
    format = '%H:%M:%S'
    now = datetime.datetime.now()
    print(now.strftime(format))


def print_duration(name=None):
    # Initialize a dictionary to store last call times for different names
    if not hasattr(print_duration, 'last_calls'):
        print_duration.last_calls = {}

    # Handle the case where a name is provided
    if name:
        # Check if the name already has a recorded last call time
        if name in print_duration.last_calls:
            current_time = time.time()
            elapsed = current_time - print_duration.last_calls[name]
            print_duration.last_calls[name] = current_time
            print(f"Duration {name}: {elapsed:.2f} seconds")
        else:
            # First call for this name
            print_duration.last_calls[name] = time.time()
            # print(f"Starting timer for {name}")
    else:
        # Handle the case where no name is provided
        if 'default' in print_duration.last_calls:
            current_time = time.time()
            elapsed = current_time - print_duration.last_calls['default']
            print_duration.last_calls['default'] = current_time
            print(f"Duration: {elapsed:.2f} seconds")
        else:
            # First call without a name
            print_duration.last_calls['default'] = time.time()
            # print("Starting default timer")


# def print_duration():
#     # Check if the function has a 'last_call' attribute
#     if not hasattr(print_duration, 'last_call'):
#         print_duration.last_call = time.time()
#     else:
#         current_time = time.time()
#         elapsed = current_time - print_duration.last_call
#         print_duration.last_call = current_time
#         print(f"Duration: {elapsed:.2f}")


def process_in_parallel(items, function, batch_size, extend=False):
    results = []  # List to hold all results

    # Process the items in batches
    for i in range(0, len(items), batch_size):
        # Select a batch of items
        batch = items[i:i + batch_size]

        # Use ThreadPoolExecutor to execute function in parallel
        with concurrent.futures.ThreadPoolExecutor(max_workers=batch_size) as executor:
            # Submit tasks to the executor
            futures = [executor.submit(function, item) for item in batch]

            # Wait for all futures to complete and collect results
            for future in concurrent.futures.as_completed(futures):
                try:
                    result = future.result()
                    if extend:
                        results.extend(result)
                    else:
                        results.append(result)  # Collecting result
                except Exception as exc:
                    print(f"An item generated an exception: {exc}")
                    # Optionally, append an error result or handle otherwise

    return results  # Return the accumulated results


def create_table_and_insert_records(data, table_name, db_file_name, pk_field_name=None):

    # Check if data is a single dict, if so, convert it to a list of dicts
    if isinstance(data, dict):
        data = [data]

    # Extract keys from the first dictionary as column names
    columns = data[0].keys()

    conn = sqlite3.connect(db_file_name)
    cursor = conn.cursor()

    default_datatype = 'TEXT'

    cursor.execute(f"DROP TABLE IF EXISTS `{table_name}`")

    column_definitions = [f'`{col}` {default_datatype}' for col in columns]
    create_table_query = f"CREATE TABLE `{table_name}` ("
    create_table_query += ", \n".join(column_definitions)

    if pk_field_name and pk_field_name in columns:
        create_table_query += f"\n, PRIMARY KEY (`{pk_field_name}`)"

    create_table_query += ")"
    cursor.execute(create_table_query)

    # Prepare data for insertion (convert dict values to tuple, convert nested objects to string)
    insert_values = []
    for record in data:
        record_values = []
        for col in columns:
            # Check if key exists in dict, else use NULL
            if col in record:
                value = record[col]
                # Convert dict or list to JSON string
                if isinstance(value, (dict, list)):
                    value = json.dumps(value)
            else:
                value = None  # NULL value for SQLite
            record_values.append(value)
        insert_values.append(tuple(record_values))

    # add quotes for the cases when there are dots or $ etc.
    quoted_columns = [f'`{column}`' for column in columns]
    cursor.executemany(f"INSERT INTO `{table_name}` ({', '.join(quoted_columns)}) VALUES ({', '.join(['?' for _ in columns])})", insert_values)

    conn.commit()
    conn.close()


def create_sqlite_table(table_name, fields, name_datatype_mapping_regex=None, pk_field_name=None, drop_table_if_exists=False):
    conn = sqlite3.connect(db_file_name)
    cursor = conn.cursor()
    drop_table_query = f"DROP TABLE IF EXISTS `{table_name}`"
    create_table_query = f"CREATE TABLE IF NOT EXISTS `{table_name}` ("
    fields_sql = []
    for field in fields:
        datatype = 'TEXT'  # Default datatype is TEXT

        # Check if the field name matches any regex pattern in the mapping
        if name_datatype_mapping_regex:
            for pattern, mapped_datatype in name_datatype_mapping_regex.items():
                if re.search(pattern, field):
                    datatype = mapped_datatype
                    break
        field_sql = f"`{field}` {datatype} "
        fields_sql.append(field_sql)

    create_table_query += ", \n".join(fields_sql)
    if pk_field_name:
        create_table_query += f"\n, PRIMARY KEY (`{pk_field_name}`)"
    create_table_query += ")"

    if drop_table_if_exists:
        cursor.execute(drop_table_query)

    cursor.execute(create_table_query)

    conn.commit()
    conn.close()


def insert_multiple_records_into_table(records, table_name, db_file_name):
    """
    records is an array of objects
    object keys taken from the first object
    only columns that exist in the table will be updated
    """
    conn = sqlite3.connect(db_file_name)
    cursor = conn.cursor()

    try:
        # Get the column names from the table in the database
        cursor.execute(f"PRAGMA table_info({table_name})")
        column_info = cursor.fetchall()
        column_names = [column[1] for column in column_info]

        # Get the keys from the first record and find the common keys with the table columns
        first_record = records[0]
        common_keys = [key for key in first_record.keys() if key in column_names]

        # Create an array of tuples containing the values for the common keys from each record

        values_to_insert = [
        ]
        for record in records:
            row = tuple(record[key] for key in common_keys)
            values_to_insert.append(row)

        # print(values_to_insert)

        # Create the INSERT query
        placeholders = ",".join(["?" for _ in common_keys])
        insert_query = f"INSERT OR REPLACE INTO {table_name} ({','.join(common_keys)}) VALUES ({placeholders})"

        # print(insert_query)

        # Execute the INSERT query with executemany
        cursor.executemany(insert_query, values_to_insert)

        # Commit the changes to the database
        conn.commit()
    except sqlite3.Error as e:
        print("SQLite error:", e)
    finally:
        # Close the database connection
        conn.close()


def get_json_path(json_object, path):
    keys = path.split('.')  # Split the path string into keys
    current = json_object  # Start with the original JSON object

    for key in keys:
        if "[" in key and "]" in key:
            # Handle case when key contains an index, like "regions[0]"
            key, index = key.split('[')
            index = int(index.strip(']'))
            try:
                current = current[key][index]
            except (KeyError, IndexError):
                return None  # Return None if the key or index is invalid
        else:
            try:
                current = current[key]
            except:
                return None

    return current


def insert_record_into_table(table_name, data_dict, array_of_fields, name_function_mapping_regex=None, pk_field_name=None):
    conn = sqlite3.connect(db_file_name)
    cursor = conn.cursor()

    insert_query = f"INSERT OR REPLACE INTO `{table_name}` ("

    # get pk from table
    get_pk_statement = """
        SELECT name 
        FROM pragma_table_info(?)
        WHERE pk = 1;
        """
    pk_field_name = cursor.execute(get_pk_statement, (table_name, )).fetchall()[0][0]

    values = []
    for field in array_of_fields:
        value = get_json_path(data_dict, field)
        if name_function_mapping_regex:
            for pattern, conversion_function in name_function_mapping_regex.items():
                if re.search(pattern, field) and value != None:
                    value = conversion_function(value)
        values.append(value)

    insert_query += ', '.join([f'`{field}`' for field in array_of_fields])

    value_placeholders = ', '.join(['?'] * len(array_of_fields))

    insert_query += f") VALUES ({value_placeholders})"

    cursor.execute(insert_query, values)

    conn.commit()
    conn.close()


def safe_filename(filename):
    # replacing disallowed chars in filenames a la yt-dlp
    replacements = {
        '<': '＜',  # Fullwidth less-than sign
        '>': '＞',  # Fullwidth greater-than sign
        ':': '：',  # Fullwidth colon
        '"': '＂',  # Fullwidth quotation mark
        '/': chr(10744),
        '\\': chr(10745),
        '|': chr(65372),  # Fullwidth vertical line
        '?': '？',  # Fullwidth question mark
        '*': '＊'   # Fullwidth asterisk
    }

    for disallowed, safe in replacements.items():
        filename = filename.replace(disallowed, safe)

    return filename


def dump_url(url, file_name=None, do_not_save=False):
    response = requests.get(url).json()
    if not do_not_save:
        if not file_name:
            file_name = safe_filename(url)
        output_file_name = f"{file_name}.json"
        output_file_path = os.path.join(save_dir_path, output_file_name)
        with open(output_file_path, 'w', encoding='utf-8') as f:
            json.dump(response, f)
    return response


def get_sqlite_sql(db_file_path, sql):
    conn = sqlite3.connect(db_file_path)
    cursor = conn.cursor()
    cursor.execute(sql)
    rows = cursor.fetchall()
    column_names = [description[0] for description in cursor.description]
    conn.close()
    return column_names, rows
