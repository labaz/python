import sys
import re
import math

def evaluate_hex_expression(expression):
    # Replace hex numbers with their decimal equivalents
    hex_numbers = []
    for hex_num in re.findall(r'0x[0-9A-Fa-f]+', expression):
        decimal_num = int(hex_num, 16)
        expression = expression.replace(hex_num, str(decimal_num))
        hex_numbers.append((hex_num, decimal_num))

    # Evaluate the expression and floor the result
    result = math.floor(eval(expression))

    # Convert the result to hexadecimal
    hex_result = hex(result)[2:]

    # Pad with zeros to ensure two-digit representation
    hex_result = '0x' + hex_result.zfill((len(hex_result) + 1) // 2 * 2).upper()

    # Return the result in decimal and hexadecimal format
    return result, hex_result, hex_numbers

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: python hc.py <expression>")
        sys.exit(1)

    expression = ' '.join(sys.argv[1:])
    decimal_result, hex_result, hex_numbers = evaluate_hex_expression(expression)

    print("Expression:", expression)
#    print("Decimal result:", decimal_result)

    print("Hexadecimal result:", hex_result)
    print("Hexadecimal numbers and their decimal equivalents:")
    for hex_num, decimal_num in hex_numbers:
        print(f"{hex_num} -> {decimal_num}")
