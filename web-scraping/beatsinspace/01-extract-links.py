import scrapy
import os
from pathlib import Path
from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings
from scrapy.utils.project import get_project_settings
import re
import json
from urllib.parse import urlsplit

# just to make sure we're in the same dir as file
os.chdir(os.path.dirname(__file__))

subdir_raw_files = 'html'
Path(f"./{subdir_raw_files}").mkdir(exist_ok=True)

subdir_scraped_data = 'json'
Path(f"./{subdir_scraped_data}").mkdir(exist_ok=True)

class JsonExportPipeline:
    def process_item(self, item, spider):
        if item.get('mp3'):
            file_name = f"{item['file_name']}.json"
            file_path = f"./{subdir_scraped_data}/{file_name}"
            with open(file_path, 'w') as file:
                line = json.dumps(dict(item)) + "\n"
                file.write(line)
        return item

url = 'https://beatsinspace.net/wp-admin/admin-ajax.php'
data = {
    'action': 'my_ajax_action',
    'year': '',
    'sort': 'hide',
    'count': '2555'
}


class bis(scrapy.Spider):
    name = "bis"

    # this is a hardcoded function name 
    # based on start_requests -> the callback function is called to get the list of requests
    def start_requests(self):
        yield scrapy.FormRequest(
            url,
            formdata=data,
            callback=self.get_urls_from_start_request
        )

    # after we process the initial page, we extract urls from it 
    # based on urls, we build scrapy.Request
    # for each Request, a callback is called
    def get_urls_from_start_request(self, response):
        links = response.xpath('//div[@class="playlist-image"]/a/@href').getall()
        for link in links:
            url = response.urljoin(link)
            yield scrapy.Request(url, self.parse_link) 

    # this is hardcoded function name 
    # this executes the requests on urls from start_urls 
    def parse_link(self, response):
        episode_number = re.sub(r'.*playlists/(.*)/.*', r'\1', response.url)
        file_name = f"{self.name}-{episode_number}.html"
        file_path = f"./{subdir_raw_files}/{file_name}"
        Path(file_path).write_bytes(response.body)
        self.log(file_path)

        part_number = 1
        
        # ☝ .get() and .getall() returns strings, 
        # to return the selection, do NOT use .get() ...
        episode_number_and_date = response.selector.xpath('//section[@class="top"]/div[@class="radio-title text-center"][1]/text()').get()
        episode_artists = response.selector.xpath('//section[@class="top"]/div[@class="radio-title text-center"][2]//text()').get()
        metadata = json.loads(response.selector.xpath('//script[@class="yoast-schema-graph"]/text()').get())
        datepublished = metadata.get("@graph")[0].get("datePublished")

        # cat bis-706.html | ws '//script[@class]/text()' | sed -E 's/<!\[CDATA\[(.*)\]\]>/\1/g' | jq


        for ep in response.xpath('//section[@class="playlist"]'):
            img = ep.xpath('.//div[@class="dj-image"]/img/@src').get()
            mp3 = ep.xpath('.//a[@class="download"]/@href').get()
            links = ep.xpath('.//ul[@class="menu"]//a/@href').get()
            tracklist = "\n".join(ep.xpath('.//div[@class="tracklist"]/p/text()').getall())
            part_name = ep.xpath('.//button/@data-title').get()
            
            mp3_urlpath = urlsplit(mp3).path
            mp3_url_filename = os.path.basename(mp3_urlpath)
            file_name = f"{mp3_url_filename}.json"
            file_path = f"./{subdir_scraped_data}/{file_name}"
            
            print(file_path)

            result = {
                "episode": episode_number,
                "datepublished": datepublished,
                "episode_number_and_date": episode_number_and_date,
                "episode_artists": episode_artists,
                "part": part_number,
                "part_name": part_name,
                "img": img,
                "mp3": mp3,
                "file_name": file_name,
                "links": links,
                "tracklist": tracklist,
                "metadata": metadata
            }
            
            # Path(file_path).write_bytes(response.body)

            yield result

            part_number = part_number + 1

settings = Settings()

# Dynamically setting the pipeline with the current module name
settings.set('ITEM_PIPELINES', {f'{__name__}.JsonExportPipeline': 300})

process = CrawlerProcess(settings=settings)
process.crawl(bis)
process.start()
