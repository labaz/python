import requests
from urllib.parse import urlparse
from lxml import etree
import json
import time
import os
import io
import sys
import re
# add helpers from the root dir
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
from helpers.helper_functions import *
from datetime import datetime, timedelta

# make working dir the file dir 
os.chdir(os.path.dirname(__file__))

headers = {
        'User-Agent': 'curl/7.64.1',  # Mimicking curl's User-Agent
        # Add any other headers that might be necessary
    }

file_name = 'playlist_links'

def get_playlists():
    # first get the playlists
    url = 'https://beatsinspace.net/wp-admin/admin-ajax.php'
    data = {
        'action': 'my_ajax_action',
        'year': '2011',
        'sort': 'hide',
        'count': '5555'
    }
    response = requests.post(url, headers=headers, data=data) #data_raw)
    response_text = response.text
    
    # now get playlist links:
    playlist_links = set()
    playlist_url_regex = r'data-link="(.*?)"'
    for line in response_text.split('\n'):
        match = re.search(playlist_url_regex, line)
        if match:
            playlist_links.add(match.group(1))
    
    playlist_links = sorted(playlist_links)
    with open(file_name, 'w') as f:
        json.dump(playlist_links, f)
    
    return

# get_playlists()

playlist_links = []
with open(file_name, 'r') as f:
    playlist_links = json.load(f)

xpaths = {
    'date': '//div[@class="radio-title text-center"]/text()',
    'images': '//section[@class="playlist"]/div[@class="dj-image"]/img/@src',
    'urls': '//section[@class="playlist"]//button[@id="play-icon"]/@data-source',
    'artists': '//div[@class="tracklist"]/div/text()',
    'tracklists': '//section[@class="playlist"]/div[@class="tracklist"]',
}


def process_link(link):
    # print(link)
    response_text = requests.get(link, headers=headers).text
    parser = etree.HTMLParser()
    tree = etree.parse(io.StringIO(response_text), parser)

    results = {}
    results.update({'url': link})

    for key in xpaths:
        xpath = xpaths[key]
        xpath_results = tree.xpath(xpath)
        # print(f'key: {key}, count: {len(xpath_results)}')
        # check if the result is an element
        text_results = []
        for xpath_result in xpath_results:
            if 'text' in dir(xpath_result):
                # print(f'text')
                text_result = '\n'.join(xpath_result.xpath('.//text()'))
                text_results.append(text_result)
        if text_results:
            xpath_results = text_results
        xpath_dict = {key: xpath_results}
        results.update(xpath_dict)
    
    return results

# playlist_links = playlist_links[800:806]

file_name = 'playlist_results.json'

def get_details():
    results = []
    results = process_in_parallel(playlist_links, process_link, 30)

    print(json.dumps(results, indent=2))
    with open(file_name, 'w', encoding='utf-8') as f:
        json.dump(results, f)

# get_details()

with open(file_name, 'r') as f:
    episodes = json.load(f)

print(f'episodes: {len(episodes)}')

episodes_with_urls = [episode for episode in episodes if len(episode['urls']) > 0]

print(f'episodes_with_urls: {len(episodes_with_urls)}')

def check_url(url):
    try:
        response = requests.head(url)
        return response.status_code == 200
    except:
        return False

# flatten the list
def split_dict(input_dict, split_key):
    values = input_dict.get(split_key, [])
    num_dicts = len(values) if isinstance(values, list) else 1

    result_dicts = []

    for i in range(num_dicts):
        new_dict = {}
        for key, value in input_dict.items():
            if isinstance(value, list):
                new_dict[key] = value[i] if i < len(value) else value[-1]
            else:
                new_dict[key] = value
        result_dicts.append(new_dict)

    return result_dicts

single_parts = []
for episode in episodes:
    single_parts.extend(split_dict(episode, 'urls'))


print(f'single parts: {len(single_parts)}')
file_name = 'single_parts.json'
with open(file_name, 'w', encoding='utf-8') as f:
    json.dump(single_parts, f, indent=2)

# check the urls
def check_url_wrapper(single_part):
    url = single_part['urls']
    sign = '✖'
    if check_url(url):
        sign = '✔'
        # print(f'{sign} {url}')
        return single_part
    else:
        # print(f'{sign} {url}')
        return None

# single_parts = single_parts[0:2]
file_name = 'single_parts_with_urls.json'

def get_single_parts_with_urls():
    single_parts_with_urls = process_in_parallel(single_parts, check_url_wrapper, 50)


    with open(file_name, 'w', encoding='utf-8') as f:
        json.dump(single_parts_with_urls, f, indent=2)

# get_single_parts_with_urls()

with open(file_name, 'r', encoding='utf-8') as f:
    single_parts_with_urls = json.load(f)



def download_file(url):
    try:
        response = requests.head(url)
        if response.status_code != 200:
            return None

        file_name = os.path.basename(urlparse(url).path)
        file_path = os.path.join(os.getcwd(), file_name)

        response = requests.get(url, stream=True)
        if response.status_code == 200:
            with open(file_path, 'wb') as file:
                for chunk in response.iter_content(chunk_size=1024):
                    if chunk:
                        file.write(chunk)
            return file_path
        else:
            return None
    except requests.exceptions.RequestException:
        return None


def download_part_wrapper(single_part):
    url = single_part['urls']
    download_file(url)

# single_parts_with_urls = single_parts_with_urls[0:33]
print(f'single parts with urls: {len(single_parts_with_urls)}')

process_in_parallel(single_parts_with_urls, download_part_wrapper, 5)
