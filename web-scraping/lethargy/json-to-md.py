import scrapy
import os
from pathlib import Path
import re
import json

# just to make sure we're in the same dir as file
os.chdir(os.path.dirname(__file__))

subdir_raw_files = 'html'
Path(f"./{subdir_raw_files}").mkdir(exist_ok=True)

subdir_scraped_data = 'json'
Path(f"./{subdir_scraped_data}").mkdir(exist_ok=True)


def clear_string(s):
    s = re.sub(r'^[\n]+|[\n]+$', '', s)
    s = re.sub(r'[\n]+', ' ', s)
    s = re.sub(r'^[\s]+|[\s]+$', '', s)
    return s


class JsonObject(dict):
    def __getattr__(self, name):
        return self.get(name, None)


def make_header(single_dict, specified_attrs):
    return [attr for attr in specified_attrs if attr in single_dict]

def make_item(single_dict, header):
    return [single_dict.get(attr, ' ').replace('|', '-') for attr in header]

def json_to_markdown_table(directory_path, specified_attrs):
    data = []
    header = None

    for filename in os.listdir(directory_path):
        if filename.endswith('.json'):
            with open(os.path.join(directory_path, filename), 'r') as file:
                single_dict = json.load(file)
                if header is None:
                    header = make_header(single_dict, specified_attrs)
                data.append(make_item(single_dict, header))

    if not data or header is None:
        return ""

    # Generate Markdown table
    markdown_lines = []
    markdown_lines.append('| ' + ' | '.join(header) + ' |')
    markdown_lines.append('| ' + ' | '.join(['---'] * len(header)) + ' |')

    for item in data:
        markdown_lines.append('| ' + ' | '.join(item) + ' |')

    return '\n'.join(markdown_lines)


directory_path = subdir_scraped_data  # Replace with your directory path
specified_attrs = ['perftype', 'artist', 'time', 'url']  # Replace with your desired attributes

markdown_table = json_to_markdown_table(directory_path, specified_attrs)

with open('output.md', 'w') as md_file:
    md_file.write(markdown_table)
