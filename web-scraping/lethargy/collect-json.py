import os
from pathlib import Path
import re
import json
from datetime import datetime
from ics import Calendar, Event

# just to make sure we're in the same dir as file
os.chdir(os.path.dirname(__file__))

subdir_raw_files = 'html'
Path(f"./{subdir_raw_files}").mkdir(exist_ok=True)

subdir_scraped_data = 'json'
Path(f"./{subdir_scraped_data}").mkdir(exist_ok=True)

directory_path = subdir_scraped_data
out_path = '.'

def clear_string(s):
    s = re.sub(r'^[\n]+|[\n]+$', '', s)
    s = re.sub(r'[\n]+', ' ', s)
    s = re.sub(r'^[\s]+|[\s]+$', '', s)
    return s


class JsonObject(dict):
    def __getattr__(self, name):
        return self.get(name, None)

def map_date(string):
    location = re.search(r'\| (.*) \|', string) 
    if not location:
        return

    location = location.group(1)

    date = re.search(r'([\d\.]{8})', string)
    if not date:
        return
    # print(string)

    date = date.group(0)

    day = re.sub(r'(.*)\.(.*)\.(.*)', r'\1', date)
    if not day:
        return
    start_day = int(day)
    end_day = start_day

    time = re.search(r'([\d:-]{11})', string)

    if not time:
        return
    time = time.group(0)

    start_time = re.sub(r'(.*)-(.*)', r'\1', time)

    start_hr = int(re.sub(r'(.*):(.*)', r'\1', start_time))
    start_min = int(re.sub(r'(.*):(.*)', r'\2', start_time))

    end_time = re.sub(r'(.*)-(.*)', r'\2', time)

    end_hr = int(re.sub(r'(.*):(.*)', r'\1', end_time))
    end_min = int(re.sub(r'(.*):(.*)', r'\2', end_time))

    if end_time.startswith('0') and not start_time.startswith('0'):
        end_day = end_day + 1
    start_datetime = datetime(2024, 8, start_day, start_hr, start_min, 0)
    end_datetime = datetime(2024, 8, end_day, end_hr, end_min, 0)

    # print(f'start: {start_datetime}, end: {end_datetime}')
    return start_datetime, end_datetime, location



all = [] 
ical_timeformat = '%Y%m%dT%H%M%S'
for filename in os.listdir(directory_path):
    if filename.endswith('.json'):
        with open(os.path.join(directory_path, filename), 'r') as file:
            single_dict = json.load(file)
        date_string = single_dict.get('time','')
        artist = single_dict.get('artist','')
        url = single_dict.get('url','')
        date_mapped = map_date(date_string)
        if not date_mapped:
            continue
        print(artist)
        start_datetime, end_datetime, location = map_date(date_string)
        print(f'location: {location}, start: {start_datetime}, end: {end_datetime}')
        new_object = {
            'artist': artist,
            'location': location,
            'start': start_datetime.strftime(ical_timeformat),
            'end': end_datetime.strftime(ical_timeformat),
            'url': url
        }
        all.append(new_object)

out_file_path = os.path.join(out_path, 'all.json')
with open(out_file_path, 'w') as f:
    json.dump(all, f)


import datetime
import uuid

def format_datetime(dt):
    """ Helper to format datetime for iCal, assuming dt is already in the correct timezone. """
    return dt.strftime(ical_timeformat)

def create_ical(cal_name, timezone, events, mapping):
    ical_content = [
        "BEGIN:VCALENDAR",
        "PRODID:kek",
        "VERSION:2.0",
        "CALSCALE:GREGORIAN",
        "METHOD:PUBLISH",
        f"X-WR-CALNAME:{cal_name}",
        f"X-WR-TIMEZONE:{timezone}",
    ]

    for idx, event in enumerate(events, 1):
        start_dt = event[mapping['start']]
        end_dt = event[mapping['end']]
        ical_content.extend([
            "BEGIN:VEVENT",
            f"DTSTART;TZID={timezone}:{start_dt}",
            f"DTEND;TZID={timezone}:{end_dt}",
            f"DTSTAMP:{format_datetime(datetime.datetime.utcnow())}",
            f"UID:KekId{idx}",
            f"CREATED:19000101T120000Z",
            f"DESCRIPTION:{event.get(mapping['description'], '')}",
            f"LAST-MODIFIED:{format_datetime(datetime.datetime.now())}",
            f"LOCATION:{event[mapping['location']]}",
            "SEQUENCE:0",
            "STATUS:CONFIRMED",
            f"SUMMARY:{event[mapping['summary']]}",
            "TRANSP:OPAQUE",
            "END:VEVENT"
        ])

    ical_content.append("END:VCALENDAR")
    return "\n".join(ical_content)

# Example usage

mapping = {
    "summary": "artist",
    "description": "url",
    "location": "location",
    "start": "start",
    "end": "end"
}

calendar_content = create_ical("Lethargy2024", "Europe/Berlin", all, mapping)
print(calendar_content)

ical_filename = 'lethargy2024.ics'
with open(ical_filename, 'w') as file:
    file.write(calendar_content)
