| perftype | artist | time | url |
| --- | --- | --- | --- |
| dj | ANOUCH | SA 10.08.24 - Club - 22:00-23:30 | https://lethargy.ch/line-up/anouche/ |
| visuals | KRATA & PRALUX | Club | https://lethargy.ch/line-up/paulinazybinska/ |
| dj | LazerGazer | FR 09.08.24 - Club - 01:30-03:00 | https://lethargy.ch/line-up/lazer-gazer/ |
| live | MD PALLAVI & ANDI OTTO | FR 09.08.24 - Theater -  23:30-00:30 | https://lethargy.ch/line-up/md-pallavi-andi-otto/ |
| live | KON FABER | SO 11.08.24 - Seebühne - 18:00-19:30 | https://lethargy.ch/line-up/kon-faber/ |
|  | RSS DISCO djs | SA 10.08.24 - Ziegel - 04:00-07:00 | https://lethargy.ch/line-up/rss-disco/ |
| video installation | BOGOMIR DORINGER | SO 11.08.24 - See - | https://lethargy.ch/line-up/bogomir-doringer/ |
| dj | P.BELL |  | https://lethargy.ch/line-up/p-bell/ |
| live | COSILI | FR 09.08.24 - Ziegel - 05:00-06:00 | https://lethargy.ch/line-up/cosili-live/ |
| dj | MEGA | FR 09.08.24 - Theater - 23:00-23:30 & 04:15-06:00 | https://lethargy.ch/line-up/mega/ |
| dj | rROXYMORE | FR 09.08.24 - Ziegel - 01:30-03:00 | https://lethargy.ch/line-up/rroxymore/ |
| djs | SCHNAUZKAUZ & KONKUBINE | SA 10.08.24 - Theater - 05:00-07:00 | https://lethargy.ch/line-up/schnauzkauz-konkubine/ |
| dj | PLAYLOVE | SO 11.08.24 - Seebühne - 15:00-16:30 | https://lethargy.ch/line-up/playlove/ |
| live | 69dB | SA 10.08.24 - Club - 02:30-04:00 | https://lethargy.ch/line-up/69db/ |
| design | VERINNERLICHT |  | https://lethargy.ch/line-up/verinnerlicht/ |
| dj | ECE ÖZEL | SA 10.08.24 - Club - 23:30-01:00 | https://lethargy.ch/line-up/ece-oezel/ |
| dj | RUMORY | SO 11.08.24 - Seebühne - 13:00-15:00 | https://lethargy.ch/line-up/rumory/ |
| live | UTO | SA 10.08.24 - Theater -  00:00-01:00 | https://lethargy.ch/line-up/uto/ |
| live | DMX Krew |  | https://lethargy.ch/line-up/lady-bruce-dj/ |
| concert | ODD BEHOLDER | FR 09.08.24 - Seebühne - 20:15-21:15 | https://lethargy.ch/line-up/odd-beholder/ |
| light | THOMAS STEINER |  | https://lethargy.ch/line-up/thomas-steiner/ |
| dj | ISA TCHESNOKOVA | SA 10.08.24 - Club - 05:30-07:00 | https://lethargy.ch/line-up/isa-tchesnokova/ |
| TRINA BAR lack & glitzer |  |  | https://lethargy.ch/line-up/trina-bar-lack-glitzer/ |
| dj | MS HYDE | SA 10.08.24 -  Ziegel -  22:00-00:00 | https://lethargy.ch/line-up/ms-hyde/ |
| live | FANTASTIC TWINS |  | https://lethargy.ch/line-up/fantastic-twins/ |
| dj | IDENTIFIED PATIENT |  | https://lethargy.ch/line-up/identified-patient/ |
| dj | NVST | FR 09.08.24 - Club - 04:30-06:00 | https://lethargy.ch/line-up/nvst/ |
| concert | SAALSCHUTZ | SA 10.08.24 -  Ziegel -  00:00-01:00 | https://lethargy.ch/line-up/saalschutz/ |
| concert | MEZERG |  | https://lethargy.ch/line-up/mezerg/ |
| NINA BEDNAROVA painting |  |  | https://lethargy.ch/line-up/nina-bednarova-painting/ |
| concert | ZÜRCHER HARDCHOR | DO 08.08.24 - Seebühne - 19:30-20:00 | https://lethargy.ch/line-up/hard-chor/ |
| concert | COLLIGNON | FR 09.08.24 - Theater - | https://lethargy.ch/line-up/collignon/ |
| concert | AFAR | SO 11.08.24 - Hof - 19:30-21:00 | https://lethargy.ch/line-up/afar-concert/ |
| dj | STEFAN GOLDMAN | SA 10.08.24 - Club - 01:00-02:30 | https://lethargy.ch/line-up/stefan-goldman/ |
|  | FABULUS | FR 09.08.24 - Ziegel - 23:00-00:30 | https://lethargy.ch/line-up/fabulus/ |
| live | DREAD J | FR 09.08.24 - Ziegel - 00:30-01:30 | https://lethargy.ch/line-up/dread-j/ |
| installation | LUKAS TRUNIGER |  | https://lethargy.ch/line-up/lukas-truniger/ |
| djs | CLITS MIT STICKS | SA 11.08.24  - Theater - 22:00-00:00 | https://lethargy.ch/line-up/dirty-slips/ |
| design | SYNTOSIL |  | https://lethargy.ch/line-up/syntosil/ |
| dj | KALEEMA | SO 11.08.24 - Hof - 16:00-18:00 | https://lethargy.ch/line-up/kaleema/ |
| dj | AUDREY DANZA | SA 10.08.24 - Club - 04:00-05:30 | https://lethargy.ch/line-up/audrey-danza/ |
| installation / performance | MORITZ SIMON GEIST | 09. & 10.08.24 - Shedhalle - | https://lethargy.ch/line-up/moritz-simon-geist/ |
|  | M. RUX | SO 11.08.24 - Hof - 15:00-16:00 | https://lethargy.ch/line-up/m-rux-2/ |
| live | JARL FLAMAR | SO 11.08.24 - Hof - 18:00-19:30 | https://lethargy.ch/line-up/jarl-flamar/ |
| dj | STYRO2000 |  | https://lethargy.ch/line-up/styro2000/ |
| live | SAMI GALBI | FR 09.08.24 - Theater - 03:15-04:15 | https://lethargy.ch/line-up/sami-galbi/ |
| dj | TAL FUSSMAN | SA 10.08.24 - Theater - 03:30-05:00 | https://lethargy.ch/line-up/tal-fussman/ |
| live | AFRORACK | SA 10.08.24 -  Ziegel -  03:00-04:00 | https://lethargy.ch/line-up/afrorack/ |
| live | REX THE DOG | SA 10.08.24 - Theater - 02:30-03:30 | https://lethargy.ch/line-up/rex-the-dog/ |
| concert | TRENTEMØLLER | DO 08.08.24 - Seebühne - 20:30-22:00 | https://lethargy.ch/line-up/trentemoller/ |
| visuals | TELOMERES STUDIO |  | https://lethargy.ch/line-up/sophie-le-meillour/ |
|  | MAXMABER ORKESTAR | FR 09.08.24 - Theater - 00:45-01:45 | https://lethargy.ch/line-up/maxmaber-orkestar/ |
| live | MANUEL FISCHER | FR 09.08.24 - Club - 23:15-00:30 | https://lethargy.ch/line-up/manuel-fischer/ |
| dj | YAME | SA 10.08.24 - Theater - 01:00-02:30 | https://lethargy.ch/line-up/yame/ |
| dj | RAFUSH | SA 10.08.24 -  Ziegel -  01:00-03:00 | https://lethargy.ch/line-up/rafush/ |
| dj | DOWNTWOPUSS | SO 11.08.24 - Hof - | https://lethargy.ch/line-up/downtopuss/ |