import scrapy
import os
from pathlib import Path
from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings
from scrapy.utils.project import get_project_settings
import re
import json
from urllib.parse import urlsplit

# just to make sure we're in the same dir as file
os.chdir(os.path.dirname(__file__))

subdir_raw_files = 'html'
Path(f"./{subdir_raw_files}").mkdir(exist_ok=True)

subdir_scraped_data = 'json'
Path(f"./{subdir_scraped_data}").mkdir(exist_ok=True)

url = 'https://lethargy.ch/line-up/'
url_prefix = 'https://lethargy.ch' # no prefix
xpaths = [
    '//a[@class="d-flex flex-column align-items-center text-decoration-none text-black"]/@href',
    '//main//div[contains(@class, "text-container") and .//strong]/p[1]//text()',
    '//main//div[contains(@class, "text-container")]/h1[1]/strong/text()',
    '//main//div[contains(@class, "text-container")]/h1[1]/text()'
]

def clear_string(s):
    s = re.sub(r'^[\n]+|[\n]+$', '', s)
    s = re.sub(r'[\n]+', ' ', s)
    s = re.sub(r'^[\s]+|[\s]+$', '', s)
    return s

class JsonExportPipeline:
    def process_item(self, item, spider):
        file_name = f"{item['file_name']}.json"
        file_path = f"./{subdir_scraped_data}/{file_name}"
        with open(file_path, 'w') as file:
            line = json.dumps(dict(item)) + "\n"
            file.write(line)
        return item

# for debugging
# link_filter_regex = r'.*erofeev.*'
link_filter_regex = r'.*'

class td(scrapy.Spider):
    name = "td"

    def start_requests(self):
        yield scrapy.Request(
            url,
            callback=self.get_urls
        )

    def get_urls(self, response):
        links = response.xpath(xpaths[0]).getall()
        print(f"got {len(links)} urls")
        print(f"first link {links[0]}")
        for link in links:
            url_with_prefix = f"{url_prefix}{link}"
            if re.match(link_filter_regex, link):
	            yield scrapy.Request(
	                url_with_prefix,
	                callback=self.get_single_page
	            )

    def get_single_page(self, response):
        # check if this is multi-author page
        print(f"get_single_page url: {response.request.url}")
        url = response.request.url

        print(url)
        urlparts = [part for part in url.split('/') if part]
        file_name = '_'.join([urlparts[-1]])
        
        # time = response.selector.xpath(xpaths[1]).get()
        # artist = response.selector.xpath(xpaths[2]).get()

        time = "\n".join(response.xpath(xpaths[1]).extract())
        artist = "\n".join(response.xpath(xpaths[2]).extract())
        perftype = response.selector.xpath(xpaths[3]).get()

        if not time:
            time = ''
        if not artist:
            artist = ''
        if not perftype:
            perftype = ''

        time = clear_string(time)
        artist = clear_string(artist)
        perftype = clear_string(perftype)

        result = {
            'url': url,
            'file_name': file_name,
            'perftype': perftype,
            'artist': artist,
            'time': time,
        }

        yield result
        
    
settings = Settings()

# Dynamically setting the pipeline with the current module name
settings.set('ITEM_PIPELINES', {f'{__name__}.JsonExportPipeline': 300})

process = CrawlerProcess(settings=settings)
process.crawl(td)
process.start()

