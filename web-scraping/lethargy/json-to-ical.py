import os
from pathlib import Path
import re
import json
from datetime import datetime
from ics import Calendar, Event

# just to make sure we're in the same dir as file
os.chdir(os.path.dirname(__file__))

subdir_raw_files = 'html'
Path(f"./{subdir_raw_files}").mkdir(exist_ok=True)

subdir_scraped_data = 'json'
Path(f"./{subdir_scraped_data}").mkdir(exist_ok=True)

directory_path = subdir_scraped_data
out_path = '.'

def clear_string(s):
    s = re.sub(r'^[\n]+|[\n]+$', '', s)
    s = re.sub(r'[\n]+', ' ', s)
    s = re.sub(r'^[\s]+|[\s]+$', '', s)
    return s


class JsonObject(dict):
    def __getattr__(self, name):
        return self.get(name, None)

string = 'FR 09.08.24 | Seebühne | 20:15-21:15'


def map_date(string):
    location = re.search(r'\| (.*) \|', string) 
    if not location:
        return

    location = location.group(1)

    date = re.search(r'([\d\.]{8})', string)
    if not date:
        return
    # print(string)

    date = date.group(0)

    day = re.sub(r'(.*)\.(.*)\.(.*)', r'\1', date)
    if not day:
        return
    start_day = int(day)
    end_day = start_day

    time = re.search(r'([\d:-]{11})', string)

    if not time:
        return
    time = time.group(0)

    start_time = re.sub(r'(.*)-(.*)', r'\1', time)

    start_hr = int(re.sub(r'(.*):(.*)', r'\1', start_time))
    start_min = int(re.sub(r'(.*):(.*)', r'\2', start_time))

    end_time = re.sub(r'(.*)-(.*)', r'\2', time)

    end_hr = int(re.sub(r'(.*):(.*)', r'\1', end_time))
    end_min = int(re.sub(r'(.*):(.*)', r'\2', end_time))

    if end_time.startswith('0') and not start_time.startswith('0'):
        end_day = end_day + 1
    start_datetime = datetime(2024, 8, start_day, start_hr, start_min, 0)
    end_datetime = datetime(2024, 8, end_day, end_hr, end_min, 0)

    # print(f'start: {start_datetime}, end: {end_datetime}')
    return start_datetime, end_datetime, location



all = [] 

for filename in os.listdir(directory_path):
    if filename.endswith('.json'):
        with open(os.path.join(directory_path, filename), 'r') as file:
            single_dict = json.load(file)
        date_string = single_dict.get('time','')
        artist = single_dict.get('artist','')
        date_mapped = map_date(date_string)
        if not date_mapped:
            continue
        print(artist)
        start_datetime, end_datetime, location = map_date(date_string)
        print(f'location: {location}, start: {start_datetime}, end: {end_datetime}')
        new_object = {
            'artist': artist,
            'location': location,
            'start': start_datetime.strftime('%Y-%m-%d %H:%M:%S'),
            'end': end_datetime.strftime('%Y-%m-%d %H:%M:%S')
        }
        all.append(new_object)

out_file_path = os.path.join(out_path, 'all.json')
with open(out_file_path, 'w') as f:
    json.dump(all, f)


calendar = Calendar()

# Add events to the calendar
for event_data in all:
    event = Event()
    event.name = event_data["artist"]
    event.location = event_data["location"]
    event.begin = datetime.strptime(event_data["start"], "%Y-%m-%d %H:%M:%S")
    event.end = datetime.strptime(event_data["end"], "%Y-%m-%d %H:%M:%S")
    calendar.events.add(event)

ical_filename = 'lethargy2024.ics'
# Serialize calendar to .ics file
with open(ical_filename, 'w') as file:
    file.writelines(calendar)

ical_lines = """
X-WR-CALNAME:Lethargy2024
X-WR-TIMEZONE:Europe/Berlin
"""

# Read the existing file
with open(ical_filename, 'r') as file:
    lines = file.readlines()

# Find where to insert the custom properties
with open(ical_filename, 'w') as file:
    for line in lines:
        file.write(line)
        if line.startswith('BEGIN:VCALENDAR'):
            file.write(ical_lines)
