from helper_functions import insert_object_into_table, process_in_parallel
import json
import io
import requests
import os
import re
from lxml import etree
from urllib.parse import urlparse

os.chdir(os.path.dirname(__file__))
db_file_name = 'coopers.sqlite'

head_url = 'https://www.coopers.ch/en/jobs/index.php'

xpath_definition = {
    'url': head_url,
    'xpath': '//li[@class="job__list__item"]',
    'children': {
        'title': 'h4//text()',
        'url': 'h4//a/@href',
        'date': 'span[1]/text()',
        'location': 'div[@class="job__list__item-informations"]/div[@class="job__list__item-informations__item"][1]//text()',
        'type': 'div[@class="job__list__item-informations"]/div[@class="job__list__item-informations__item"][2]//text()',
        'pensum': 'div[@class="job__list__item-informations"]/div[@class="job__list__item-informations__item"][3]//text()',
        'short_description': 'p//text()'
    }
}


def extract_xpaths(xpath_definition):
    """
    xpath_definition = {
        # this url will be scraped
        'url': head_url, 
        
        # parent object, usually some main div, may be more than one
        'xpath': '//li[@class="job__list__item"]', 
    
        # child objects of parent div
        # key: will be key name in the results, value: will be extracted
        'children': {
            'title': 'h4//text()',
            'url': 'h4//a/@href',
            'date': 'span[1]/text()',
            'location': 'div[@class="job__list__item-informations"]/div[@class="job__list__item-informations__item"][1]//text()',
            'type': 'div[@class="job__list__item-informations"]/div[@class="job__list__item-informations__item"][2]//text()',
            'pensum': 'div[@class="job__list__item-informations"]/div[@class="job__list__item-informations__item"][3]//text()',
            'short_description': 'p//text()'
        }
    }
    """

    header_url = xpath_definition.get('url')
    header_xpath = xpath_definition.get('xpath')

    response_text = requests.get(header_url).text
    parser = etree.HTMLParser()
    tree = etree.parse(io.StringIO(response_text), parser)
    header_xpath_results = tree.xpath(header_xpath)

    #xpath_definition.update({'results': header_xpath_results})

    # then extract nested elements out of them
    found_results = []
    for found_result in header_xpath_results:

        extracted_data = {}
        for child_key, child_xpath in xpath_definition['children'].items():

            # the result is array of strings, some are empty, many are \n\t+
            child_strings = found_result.xpath(child_xpath)
            string_value = ''
            # carefully setting up regex_to_exclude is important,
            # because e.g. items in a list have a single \n between them, 
            # and there also are garbage lines with \t\t\t that should be removed
            regex_to_exclude = r'^[\s\t\r\n]{2,}$'
            regex_to_clean = r'[\t]+'
            string_value = ' '.join([
                re.sub(regex_to_clean, '', string)
                for string in child_strings 
                if not re.match(regex_to_exclude, string)
            ])
            string_value = re.sub(r'^[\s\r\n]+|[\s\r\n]+$', '', string_value)

            extracted_data.update({child_key: string_value})

        extracted_data.update({"$source_url": header_url})

        found_results.append(extracted_data)

    xpath_definition.update({'data': found_results})
    return found_results


extract_xpaths(xpath_definition)

# add the base url to the relative job url
parsed_url = urlparse(head_url)
base_url = f'{parsed_url.scheme}://{parsed_url.netloc}'

for obj in xpath_definition['data']:
    obj['url'] = base_url + obj['url']

insert_object_into_table(data=xpath_definition['data'], table_name='headers', db_file_name=db_file_name, drop_table=True, pk_field_name='url')

file_name = 'headers.json'
with open(file_name, 'w', encoding='utf-8') as f:
    json.dump(xpath_definition, f)

print(file_name)

# print(xpath_definition['data'])

####################################


####
########
####
####
########
####
####
########
####


headers_data = xpath_definition['data']
# headers_data = headers_data[0:2]

# print(json.dumps(headers_data, indent=2))
# exit()


def process_item(url):
    xpath_items = {
        'xpath': '//div[@class="container"][2]//div[@class="row"]',
        'children': {
            'title': '//h1[@class="h2 mb-0"]//text()',
            'industry': '//div[@class="job__detail__information__item"]//li[contains(@class, "job__detail__information__item__list__item") and i[contains(@class, "sx-glyphter-icon-management")]]//text()',
            'region': '//div[@class="job__detail__information__item"]//li[contains(@class, "job__detail__information__item__list__item") and i[contains(@class, "sx-glyphter-icon-location")]]/text()',
            'employment_type': '//div[@class="job__detail__information__item"]//li[contains(@class, "job__detail__information__item__list__item") and i[contains(@class, "sx-glyphter-icon-contracting")]]/text()',
            'pensum': '//div[@class="job__detail__information__item"]//li[contains(@class, "job__detail__information__item__list__item") and i[contains(@class, "sx-glyphter-icon-time")]]//text()',
            'skills': '//div[@class="job__detail__information__item"]//li[contains(@class, "job__detail__information__item__list__item") and i[contains(@class, "sx-glyphter-icon-skills")]]//text()',
            'description': '//div[@class="sx-wysiwyg-style r-mb-3"]//text()',
        }
    }
    xpath_items.update({'url': url})
    return extract_xpaths(xpath_items)


urls = [item['url'] for item in headers_data]


# urls = urls[0:1]

# results = []
# for url in urls:
#     results.append(process_item(url))

# exit()

batch_size = 40
items = process_in_parallel(urls, process_item, batch_size=batch_size, extend=True)

insert_object_into_table(data=items, table_name='items', db_file_name=db_file_name, drop_table=True, pk_field_name='$source_url')


file_name = 'items.json'
with open(file_name, 'w', encoding='utf-8') as f:
    json.dump(items, f)

print(file_name)
