import scrapy
import os
from pathlib import Path
from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings
from scrapy.utils.project import get_project_settings
import re
import json
from urllib.parse import urlsplit

# just to make sure we're in the same dir as file
os.chdir(os.path.dirname(__file__))

subdir_raw_files = 'html'
Path(f"./{subdir_raw_files}").mkdir(exist_ok=True)

subdir_scraped_data = 'json'
Path(f"./{subdir_scraped_data}").mkdir(exist_ok=True)

url = 'https://briefly.ru/authors'
url_prefix = 'https://briefly.ru'
xpaths = [
    '//div[@class="alphabetic-index"]//a/@href',
    '//section[@class="author_works"]//a/@href'
]

def clear_string(s):
    s = re.sub(r'^[\n]+|[\n]+$', '', s)
    s = re.sub(r'[\n]+', ' ', s)
    s = re.sub(r'^[\s]+|[\s]+$', '', s)
    return s

class JsonExportPipeline:
    def process_item(self, item, spider):
        file_name = f"{item['file_name']}.json"
        file_path = f"./{subdir_scraped_data}/{file_name}"
        with open(file_path, 'w') as file:
            line = json.dumps(dict(item)) + "\n"
            file.write(line)
        return item

# for debugging
# link_filter_regex = r'.*erofeev.*'
link_filter_regex = r'.*'

class td(scrapy.Spider):
    name = "td"

    def start_requests(self):
        yield scrapy.Request(
            url,
            callback=self.get_urls
        )

    def get_urls(self, response):
        links = response.xpath(xpaths[0]).getall()
        print(f"got {len(links)} urls")
        print(f"first link {links[0]}")
        for link in links:
            url_with_prefix = f"{url_prefix}{link}"
            if re.match(link_filter_regex, link):
	            yield scrapy.Request(
	                url_with_prefix,
	                callback=self.get_author_page
	            )

    def get_author_page(self, response):
        # check if this is multi-author page
        multiauthor = response.xpath('//div[@class="content cf"]').getall()
        if(multiauthor):
            is_multiauthor_page = True
            # then get single author links and run get_author_page again
            author_links = response.xpath('//div[@class="author"]/a/@href').getall()
            for link in author_links:
                url_with_prefix = f"{url_prefix}{link}"
                yield scrapy.Request(
                   url_with_prefix,
                   callback=self.get_author_page
                )

        # for non-multiauthor
        print(f"get_author_page url: {response.request.url}")
        links = response.xpath(xpaths[1]).getall()
        print(f"got {len(links)} author works")
        for link in links:
            url_with_prefix = f"{url_prefix}{link}"
            yield scrapy.Request(
                url_with_prefix,
                callback=self.get_work
            )

    def get_work(self, response):
        url = response.request.url

        print(url)
        urlparts = [part for part in url.split('/') if part]
        file_name = '_'.join([urlparts[-2], urlparts[-1]])

        # check if is sponsored
        sponsored = False
        sponsor = response.selector.xpath('//div[@class="sponsor"]').get()
        if(sponsor):
            sponsored = True


        group = response.selector.xpath('//a[contains(@class,"breadcrumb_type_culture") or contains(@class,"breadcrumb_type_tag")]//div[@class="breadcrumb__title"]/text()').get()
        group = clear_string(group)

        author = response.selector.xpath('//a[@class="breadcrumb breadcrumb_type_author"]//div[@class="breadcrumb__name"]/text()').get()

        author_years = response.selector.xpath('//a[@class="breadcrumb breadcrumb_type_author"]//div[@class="breadcrumb__timespan"]/noindex//text()').getall()
        author_years = "".join(author_years)
        author_years = clear_string(author_years)

        title = response.selector.xpath('//h1[@id="title"]/span/text()').get()

        date = response.selector.xpath('//div[@class="original_title"]/noindex/span/text()').get()

        readingtime = "".join(response.selector.xpath('//div[@class="readingtime"]//text()').getall())
        readingtime = clear_string(readingtime)

        text_html = response.selector.xpath('//div[@id="text"]/*[not(contains(@class, "honey")) and not(contains(@class, "smartreading")) and not(contains(@class, "makeright"))]').getall()

        source = "".join(response.selector.xpath('//div[@class="source"]//text()').getall())
        source = clear_string(source)

        result = {
            'url': url,
            'file_name': file_name,
            'sponsored': sponsored,
            'group': group,
            'author': author,
            'author_years': author_years,
            'title': title,
            'date': date,
            'readingtime': readingtime,
            'text_html': text_html,
            'source': source
        }

        yield result


settings = Settings()

# Dynamically setting the pipeline with the current module name
settings.set('ITEM_PIPELINES', {f'{__name__}.JsonExportPipeline': 300})

process = CrawlerProcess(settings=settings)
process.crawl(td)
process.start()

