import os
import re
import json
from lxml import html, etree
from lxml.builder import ElementMaker,E
from ebooklib import epub

json_dir = 'json'
os.chdir(os.path.dirname(__file__))
work_dir = os.path.dirname(os.path.abspath(__file__))
json_dir_path = os.path.join(work_dir, json_dir)

book = epub.EpubBook()
spine = [] # the sequence of files
toc = [] # links and sections

author = "Портал „Такие дела“"
book_title = "Мы так не говорим"

book.set_title(book_title)
book.add_author(author)

file_name = f"{author} - {book_title}.epub"

results = []
os.chdir(json_dir_path)
for file_name in os.listdir(json_dir_path):
    file_path = os.path.join(json_dir_path, file_name)
    with open(file_path, 'r') as f:
        j = json.load(f)
    results.append(j)

results_sorted=sorted(results, key = lambda x: (x['category'], x['title_wrong']))


previous_category = ""

for element in results_sorted:
    first_in_category = False
    category = element['category']
    title_wrong = element['title_wrong']
    title_right = element['title_right']
    article = element['article']
    file_name = element['file_name']

    artile_file_name_html = f"{file_name}.html"

    category_header = ""

    if category != previous_category:
        category_header = f"<h1>{category}</h1>"
    
    previous_category = category
    
    article_header = f"<h2>{title_wrong} | {title_right}</h2>"

    full_article_html = f"{category_header}{article_header}{article}"
    
    c = epub.EpubHtml(
        title=title_wrong,
        file_name=artile_file_name_html
    )
    c.content = full_article_html

    book.add_item(c) 
    spine.append(c) 

spine.insert(0, 'nav')
book.spine = spine 

book.add_item(epub.EpubNcx())
book.add_item(epub.EpubNav())

epub_file_name = f"{book_title}.epub"
epub_file_path = os.path.join(work_dir, epub_file_name)

epub.write_epub(epub_file_path, book)
