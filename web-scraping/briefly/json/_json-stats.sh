#!/bin/sh

target_dir=$(dirname $0)

# write out to these files
target_filename="Briefly.json"
target_filename_csv="Briefly.csv"

jq_query='del(.text_html, .source, .readingtime, .file_name, .author_years)'

cd "$target_dir"

source_files=( *.json )
files=()

# get all jsons except ours
for file in "${source_files[@]}"; do
  if [ "$file" != "$target_filename" ]; then
    files+=("$file")
  fi
done

last_index=$(( ${#files[@]} - 1 ))

echo "[" > "$target_filename"

bar_length=50 # The length of the progress bar

for i in "${!files[@]}"; do
  file="${files[$i]}"

  entry=$(jq "$jq_query" "$file")

  # skip empty entries
  if [ -z "$entry" ]; then
    continue
  fi

  # add comma if not last element
  comma=","
  if [ "$i" -eq "$last_index" ]; then
    comma=""
  fi
  echo "$entry$comma" >> "$target_filename"

  # progress bar
  percentage=$(($i * 100 / $last_index))
  filled_length=$(($percentage * bar_length / 100))
  printf "\rProgress: [%-${bar_length}s] %d%%" $(printf "%-${filled_length}s" | tr ' ' '#') $percentage
done

echo "]" >> "$target_filename"

# make csv
jq -r '(.[0] | keys_unsorted) as $keys | $keys, map([.[ $keys[] ]])[] | @csv' "$target_filename" > "$target_filename_csv"

echo ""
echo "JSON: $target_filename"
echo "CSV: $target_filename_csv"
