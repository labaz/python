import scrapy
import os
from pathlib import Path
from scrapy.crawler import CrawlerProcess
from scrapy.settings import Settings
from scrapy.utils.project import get_project_settings
import re
import json
from urllib.parse import urlsplit

# just to make sure we're in the same dir as file
os.chdir(os.path.dirname(__file__))

subdir_raw_files = 'html'
Path(f"./{subdir_raw_files}").mkdir(exist_ok=True)

subdir_scraped_data = 'json'
Path(f"./{subdir_scraped_data}").mkdir(exist_ok=True)

url = 'https://takiedela.ru/dictionary_category/all/'

class JsonExportPipeline:
    def process_item(self, item, spider):
        file_name = f"{item['file_name']}.json"
        file_path = f"./{subdir_scraped_data}/{file_name}"
        with open(file_path, 'w') as file:
            line = json.dumps(dict(item)) + "\n"
            file.write(line)
        return item

class td(scrapy.Spider):
    name = "td"

    def start_requests(self):
        yield scrapy.Request(
            url,
            callback=self.get_urls
        )

    def get_urls(self, response):
        links = response.xpath('//ul[@class="b-dictionary-category"]//a/@href').getall()
        for link in links:
            yield scrapy.Request(
                link,
                callback=self.read_page
            )

    def read_page(self, response):
        url = response.request.url

        print(url)

        urlpath = urlsplit(url).path
        url_file_name = os.path.basename(os.path.split(urlpath)[0])

        category = response.selector.xpath('//ul[@class="b-single__text__categories b-single__text__categories_dictionary"]/li/a/text()').get()
        title_wrong = response.selector.xpath('//div[@class="b-dictionary-word__block b-dictionary-word__block_wrong"]/div[@class="b-dictionary-word__word"]/text()').get()
        title_right = response.selector.xpath('//div[@class="b-dictionary-word__block b-dictionary-word__block_right"]/div[@class="b-dictionary-word__word"]/text()').get()
        article = response.selector.xpath('//div[@class="b-line b-single js-dictionary-adult-content"]//p').get()

        result = {
            'file_name': url_file_name,
            'category': category,
            'title_wrong': title_wrong,
            'title_right': title_right,
            'article': article
        }

        yield result


settings = Settings()

# Dynamically setting the pipeline with the current module name
settings.set('ITEM_PIPELINES', {f'{__name__}.JsonExportPipeline': 300})

process = CrawlerProcess(settings=settings)
process.crawl(td)
process.start()

