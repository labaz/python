import sqlite3
import time
import pymorphy2
import re
import multiprocessing

def print_duration(text=None):
    if text:
        print(text)

    TIME_FORMAT = "%Y-%m-%d %H:%M:%S"
    current_time = time.time()  # Get the current time
    timestamp_text = f"Timestamp: {time.strftime(TIME_FORMAT, time.localtime(current_time))}"
    duration_text = ""

    if not hasattr(print_duration, 'previous_time'):
        print_duration.previous_time = current_time  # Initialize the previous time
        duration_text = ""
        return None, 0.0  # Return None and 0 duration for the first call
    else:
        duration = current_time - print_duration.previous_time
        print_duration.previous_time = current_time  # Update the previous time
        duration_text = f"Duration: {duration:.6f} seconds"
        print(f"{timestamp_text} {duration_text}")  # Corrected print statement
        return duration

# Function to extract and normalize words with part of speech
def extract_and_normalize_words(sentence_id, sentence, morph):
    words = re.findall(r'\b\w+\b', sentence, re.U)  # Extract alpha sequences
    unique_words_in_sentence = set()  # Create a local set for unique words in this sentence
    for word in words:
        word = word.replace('…', '')  # Remove ellipsis
        parsed_word = morph.parse(word)[0]
        if (
            'PREP' not in parsed_word.tag and  # Exclude prepositions
            'CONJ' not in parsed_word.tag and  # Exclude conjunctions
            'PRCL' not in parsed_word.tag and  # Exclude particles
            not word.startswith("<")  # Exclude XML tags
        ):
            normalized_word = parsed_word.normal_form
            pos = parsed_word.tag.POS  # Get part of speech
            if pos is None:
                pos = 'NONE'  
            unique_words_in_sentence.add((normalized_word, pos))  # Store both word and part of speech
    return sentence_id, unique_words_in_sentence


if __name__ == '__main__':
    sentence_sql_limit = ''

    # Connect to the SQLite database
    conn = sqlite3.connect("text_data.db")
    cursor = conn.cursor()

    # Initialize pymorphy2
    morph = pymorphy2.MorphAnalyzer()

    print_duration('cursor.execute')

    # Retrieve the first 10 sentences from the database
    cursor.execute(f"SELECT sentence_pk, sentence_text FROM sentences {sentence_sql_limit}")
    sentences = cursor.fetchall()

    print_duration()

    print_duration('extract_and_normalize_words')

    results = []
    for sentence_id, sentence in sentences:
        result = extract_and_normalize_words(sentence_id, sentence, morph)
        results.append(result)

    print_duration()

    # # Create a multiprocessing pool
    # with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
    #     # Parallel processing to extract and normalize words
    #     results = pool.starmap(extract_and_normalize_words, [(sentence_id, sentence, morph) for sentence_id, sentence in sentences])
    #     pool.close()
    #     pool.join()

    cursor.execute("DROP TABLE IF EXISTS unique_words")

    # Create a table for unique words if it doesn't exist
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS unique_words (
            word_pk INTEGER PRIMARY KEY AUTOINCREMENT,
            text TEXT UNIQUE,
            part_of_speech TEXT       
        )
    """)

    print_duration('INSERT unique_words')

    # Insert unique words into the database using INSERT OR IGNORE
    for sentence_id, words_with_pos in results:
        for word, pos in words_with_pos:
            cursor.execute("INSERT OR IGNORE INTO unique_words (text, part_of_speech) VALUES (?, ?)", (word, pos))
            # we cannot yet insert sentences_words here, because the word pk is not yet created in the db

    # Commit changes and close the database connection
    conn.commit()

    print_duration()

    # Retrieve the unique words from the "unique_words" table

    print_duration('fetch unique_words')

    cursor.execute("SELECT word_pk, text FROM unique_words")
    unique_words = cursor.fetchall()

    print_duration()

    cursor.execute("DROP TABLE IF EXISTS sentences_words")

    # Create a table to map sentences to words if it doesn't exist
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS sentences_words (
            sentence_pk INTEGER,
            word_pk INTEGER,
            FOREIGN KEY (sentence_pk) REFERENCES sentences (sentence_pk),
            FOREIGN KEY (word_pk) REFERENCES unique_words (word_pk)
        )
    """)

    print_duration('next((w[0] for w in unique_words if w[1] == word), None)')

    # Map unique words to sentences in the "sentences_words" table
    for sentence_id, words in results:
        for word,pos in words:
            # Use next() with a default value to avoid StopIteration
            word_pk = next((w[0] for w in unique_words if w[1] == word), None)
            if word_pk is not None:
                cursor.execute("INSERT INTO sentences_words (sentence_pk, word_pk) VALUES (?, ?)", (sentence_id, word_pk))

    # Commit changes and close the database connection
    conn.commit()
    conn.close()

    print_duration()
