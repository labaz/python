import os
import sqlite3
import re

# Define the path to your text files directory
text_path = "c:/temp/unity-texts/TextAsset"

# Create an SQLite database and cursor
conn = sqlite3.connect("text_data.db")
cursor = conn.cursor()

# Drop existing tables if they exist
cursor.execute("DROP TABLE IF EXISTS files")
cursor.execute("DROP TABLE IF EXISTS texts")
cursor.execute("DROP TABLE IF EXISTS files_texts")

# Create the "files" table
cursor.execute("""
    CREATE TABLE IF NOT EXISTS files (
        file_pk INTEGER PRIMARY KEY AUTOINCREMENT,
        filepath TEXT
    )
""")

# Create the "texts" table
cursor.execute("""
    CREATE TABLE IF NOT EXISTS texts (
        text_pk INTEGER PRIMARY KEY AUTOINCREMENT,
        text TEXT
    )
""")

# Create the "files_texts" table
cursor.execute("""
    CREATE TABLE IF NOT EXISTS files_texts (
        file_pk INTEGER,
        text_pk INTEGER,
        FOREIGN KEY (file_pk) REFERENCES files (file_pk),
        FOREIGN KEY (text_pk) REFERENCES texts (text_pk)
    )
""")

# Function to extract texts from a line
def extract_texts(line):
    texts = re.findall(r'"([^"]*)"', line)
    return texts

# Iterate through the files in the directory
for root, dirs, files in os.walk(text_path):
    for file_name in files:
        file_path = os.path.join(root, file_name)
        
        # Insert the file metadata once per file
        cursor.execute("INSERT INTO files (filepath) VALUES (?)", (file_path,))
        file_id = cursor.lastrowid
        
        with open(file_path, "r", encoding="utf-8") as file:
            for line in file:
                extracted_texts = extract_texts(line)
                for text in extracted_texts:
                    # Insert the extracted text
                    cursor.execute("INSERT INTO texts (text) VALUES (?)", (text,))
                    text_id = cursor.lastrowid
                    
                    # Map the file to the text
                    cursor.execute("INSERT INTO files_texts (file_pk, text_pk) VALUES (?, ?)", (file_id, text_id))

# Commit the changes and close the database connection
conn.commit()
conn.close()
