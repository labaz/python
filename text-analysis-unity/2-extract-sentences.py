import sqlite3
from nltk.tokenize import sent_tokenize

# Connect to the SQLite database
conn = sqlite3.connect("text_data.db")
cursor = conn.cursor()

cursor.execute("DROP TABLE IF EXISTS sentences")

# Create a table for sentences
cursor.execute("""
    CREATE TABLE IF NOT EXISTS sentences (
        sentence_pk INTEGER PRIMARY KEY AUTOINCREMENT,
        text_pk INTEGER,
        sentence_text TEXT,
        FOREIGN KEY (text_pk) REFERENCES texts (text_pk)
    )
""")

# Retrieve texts from the database
cursor.execute("SELECT text_pk, text FROM texts")
texts = cursor.fetchall()

# Function to split text into sentences
def split_sentences(text_id, text):
    sentences = sent_tokenize(text, language="russian")
    for sentence in sentences:
        cursor.execute("INSERT INTO sentences (text_pk, sentence_text) VALUES (?, ?)", (text_id, sentence))

# Process each text and split into sentences
for text_id, text in texts:
    split_sentences(text_id, text)

# Commit changes and close the database connection
conn.commit()
conn.close()
