import os
from telethon.sync import TelegramClient
import json
from datetime import datetime
import base64

api_id = os.environ.get('TELEGRAM_API_ID')
api_hash = os.environ.get('TELEGRAM_API_HASH')

channel_name = 'https://t.me/ixbtgames'
search_keyword = 'Ретроспектива игр Larian Studios'
working_dir = 'c:/temp/'  # Change this to your desired working directory

client = TelegramClient('anon', api_id, api_hash)

# Function to convert non-JSON-serializable types
def convert_to_serializable(obj):
    if isinstance(obj, datetime):
        return obj.strftime('%Y-%m-%d %H%M%S')
    elif isinstance(obj, bytes):
        return base64.b64encode(obj).decode('utf-8')
    elif isinstance(obj, dict):
        return {key: convert_to_serializable(value) for key, value in obj.items()}
    elif isinstance(obj, list):
        return [convert_to_serializable(item) for item in obj]
    else:
        return obj

async def main():
    entity = await client.get_entity(channel_name)
    async for message in client.iter_messages(entity, search=search_keyword):
        max_amp = 10
        media = []
        if message.grouped_id:
            search_ids = [i for i in range(message.id - max_amp, message.id + max_amp + 1)]
            posts = await client.get_messages(entity, ids=search_ids)
            
            for post in posts:
                if post is not None and post.grouped_id == message.grouped_id and post.media is not None:
                    path = await client.download_media(post.media, working_dir)
                    post_dict = post.to_dict()
                    post_dict['path'] = path
                    media.append(post_dict)    
        else:
            #path = os.path.join(working_dir, message.media.document.attributes[0].file_name)
            path = await client.download_media(message.media, working_dir)
            post_dict = message.to_dict()
            media_dict = post_dict['media']
            media_dict['path'] = path
            media.append(media_dict)

        message_dict = message.to_dict()
        # Apply the conversion function to all properties
        message_dict = {key: convert_to_serializable(value) for key, value in message_dict.items()}
        
        media_json = [convert_to_serializable(obj) for obj in media]
        message_dict['media'] = media_json

        # Save the post JSON to a file
        timestamp = message.date.strftime('%Y%m%d%H%M%S')
        json_filename = os.path.join(working_dir, f'{timestamp}.json')
        with open(json_filename, 'w', encoding='utf-8') as json_file:
            json.dump(message_dict, json_file, indent=2, ensure_ascii=False)

        pretty_message_json = json.dumps(message_dict, indent=2, ensure_ascii=False)
        print(pretty_message_json)

with client:
    client.loop.run_until_complete(main())
