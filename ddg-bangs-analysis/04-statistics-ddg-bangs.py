import json
import sqlite3
import os

def get_sqlite_sql(db_file_path, sql):
    conn = sqlite3.connect(db_file_path)
    cursor = conn.cursor()
    cursor.execute(sql)
    rows = cursor.fetchall()
    column_names = [description[0] for description in cursor.description]
    conn.close()
    return column_names, rows

def rows_to_markdown(column_names, rows):
    # Assuming rows is a list of tuples, where each tuple represents a row from the database.

    # Check if rows are empty
    if not rows:
        return "No data available."

    # Initialize markdown string with headers based on the first row
    # Assuming all rows have the same number of columns and data type
    header = '| ' + ' | '.join(column_names) + ' |\n'

    markdown = header
    markdown += '|' + '|'.join([' --- ' for _ in column_names]) + '|\n'
    # Add row data
    for row in rows:
        markdown += '| ' + ' | '.join(str(item).replace(',', '<br>') for item in row) + ' |\n'

    return markdown

def rows_to_json(column_names, rows):
    # Create a list of dictionaries, each representing a row from the database
    # Combine column names and row values
    data = [dict(zip(column_names, row)) for row in rows]
    
    # Convert the list of dictionaries to a JSON string
    return json.dumps(data, indent=4)

db_file_name = 'ddg-bangs.sqlite'
markdown_file_name = 'ddg-bangs-stats.md'

current_file_dir = os.path.dirname(os.path.abspath(__file__))
db_file_path = os.path.join(current_file_dir, db_file_name)

print(f'db_file_path: {db_file_path}')

queries_and_descriptions = [
#    {'description_md' : 'test', 'sql' : "SELECT name FROM sqlite_master WHERE type='table'"},
    {
        'file_name' : 'ddg-bangs-web-services.md',
        'description_md' : '''
## Web services that ever existed in DDG bang
- `[count of unique bangs]` - how many different bangs were created for the service.
There can be more bangs than search urls, because some searches had multiple bangs.
- `[count of unique search urls]` - how many search urls were created for the service.
- `[count of wayback snapshots downloaded]` - in how many snapshots (out of the **downloaded ones** -- not all have been downloaded from wayback machine) this service was present.
- `[count of rows]` - how many rows with this service in the downloaded dataset
    ''',
        'sql' : '''
        SELECT
        	d 						    as [website],
        	count(DISTINCT t) 			as [count of unique bangs],
        	count(DISTINCT u) 			as [count of unique search urls],
        	count(DISTINCT file_id) 	as [count of wayback snapshots downloaded],
        	count(*) 					as [count of rows]
        from bangs
        group by d
        order by 3 desc, 1 asc
        '''
    },
    {
        'file_name' : 'ddg-bangs.md',
        'description_md' : '''
## DDG bangs and search urls that ever existed in DDG
With some stats. 
        ''',
        'sql' : '''
        select 
        	s											as [web service],
        	GROUP_CONCAT(DISTINCT t)		as [bangs],
        	GROUP_CONCAT(DISTINCT u)		as [search urls],
        	count(DISTINCT t) 							as [count of unique bangs],
        	count(DISTINCT u) 							as [count of unique search urls],
        	count(DISTINCT file_id) 					as [count of wayback snapshots downloaded],
        	count(*) 									as [count of rows]
        from bangs
        group by 1
        order by 6 desc, 1 asc
        '''
    },
    {
        'file_name' : 'ddg-bangs.json',
        'sql' : '''
        select DISTINCT 
        	c, sc, s, d, t, u
        from bangs 
        order by 1, 2, 3, 5, 6
        '''
    }
]

for q in queries_and_descriptions:
    sql = q["sql"]
    description_md = q.get("description_md")
    file_name = q.get("file_name")

    file_path = os.path.join(current_file_dir, file_name)
    print(file_path)

    column_names, rows = get_sqlite_sql(db_file_path, sql)
    
    file_content = ''

    if file_name.__contains__('.md'):
        rows_md = rows_to_markdown(column_names, rows)
        file_content = description_md + '\n\n' + rows_md
    elif file_name.__contains__('.json'): 
        file_content = rows_to_json(column_names, rows)

    with open(file_path, 'w', encoding='utf-8') as f:
        f.write(file_content)

    print('file written')
