import sys
from datetime import datetime
import json
import time
import os
import concurrent.futures
from lxml import html
import re
import requests
from urllib.parse import urlparse

def get_xpath(xpath, content):
    tree = html.fromstring(content)
    result_xpath = tree.xpath(xpath)
    return result_xpath

# Define a function to fetch a URL
def fetch_url(url):
    try:
        response = requests.get(url, timeout=10)  # 10 seconds timeout
        response_text = response.text
        # encode workaround is needed to be able to print some utf8 characters in windows
        response_text = response_text.encode('utf-8')
        return {'url': url, 'content': response_text}
    except requests.exceptions.RequestException as e:
        return {'url': url, 'error': str(e)}

# Save the url in specified dir
def save_url_to_dir(url, dir_path):
    content = fetch_url(url)["content"]
    filename = re.sub('[:/?]+', '_', url)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    file_path = os.path.join(dir_path, filename)
    with open(file_path, 'wb', ) as file:
        if isinstance(content, str):
            file.write(content.encode('utf-8'))
        # If content is already bytes, write it directly
        elif isinstance(content, bytes):
            file.write(content)
def print_time():
    format = '%H:%M:%S'
    now = datetime.now()
    print(now.strftime(format))

def process_in_batches(items, function, batch_size):
    results = []  # List to hold all results

    # Process the items in batches
    for i in range(0, len(items), batch_size):
        # Select a batch of items
        batch = items[i:i+batch_size]

        # Use ThreadPoolExecutor to execute function in parallel
        with concurrent.futures.ThreadPoolExecutor(max_workers=batch_size) as executor:
            # Submit tasks to the executor
            futures = [executor.submit(function, item) for item in batch]

            # Wait for all futures to complete and collect results
            for future in concurrent.futures.as_completed(futures):
                try:
                    result = future.result()
                    results.append(result)  # Collecting result
                except Exception as exc:
                    print(f"An item generated an exception: {exc}")
                    # Optionally, append an error result or handle otherwise

    return results  # Return the accumulated results

########  ##     ## ##    ##    ##     ## ######## ########  ######## 
##     ## ##     ## ###   ##    ##     ## ##       ##     ## ##       
##     ## ##     ## ####  ##    ##     ## ##       ##     ## ##       
########  ##     ## ## ## ##    ######### ######   ########  ######   
##   ##   ##     ## ##  ####    ##     ## ##       ##   ##   ##       
##    ##  ##     ## ##   ###    ##     ## ##       ##    ##  ##       
##     ##  #######  ##    ##    ##     ## ######## ##     ## ######## 

print_time()

# save in current file dir
current_file_dir = os.path.dirname(os.path.abspath(__file__))
subdir = 'bangs'
save_dir_path = os.path.join(current_file_dir, subdir)

# the url of main history page containing the urls to all versions of page
url = 'https://wayback-classic.net/cgi-bin/history.cgi?q=https%3A%2F%2Fduckduckgo.com%2Fbang.js&utf8=%E2%9C%93'

# base_url is used to resolve relative urls
parsed_url = urlparse(url)
url_path = re.match(r'(.*/)', parsed_url.path).group(1)
base_url = parsed_url.scheme + "://" + parsed_url.netloc + url_path

main_content = fetch_url(url)["content"]

xpath = '//td/a/@href'
# get the links to the pages containing snapshots for periods

links = []
result_xpath = get_xpath(xpath, main_content)

for link in result_xpath:
    link = base_url + link
    links.append(link)

# for testing: limit to 2 pages
#links = links[0:2]

# workaround to make requests not so fast
def fetch_url_wait(url):
    print(f'fetch_url_wait("{url}")')
    print_time()
    time.sleep(0)
    return fetch_url(url)

# get all pages
page_results = process_in_batches(links, fetch_url_wait, 5)

print_time()

print(f'len(page_results): {len(page_results)}')
#print(f'{page_results}')

# structure of page_results = [{url:'http://...',content:'<html>...'}]

# this function is specific to our processing
# i.e. href needs to include 'bang.js' in order to be included
def get_links_from_downloaded(result):
    href_should_contain = 'bang.js'
    xpath = f'//a[contains(@href,"{href_should_contain}")]/@href'
    results = get_xpath(xpath, result["content"])
    processed_links = []
    for link in results:
        processed_link = 'https:' + link
        processed_links.append(processed_link)
    return processed_links

all_links = process_in_batches(page_results, get_links_from_downloaded, 30)

# flatten the links list
all_links = [item for sublist in all_links for item in sublist]

# dump all found links 
fp = os.path.join(current_file_dir, 'all_links.json')
with open(fp, 'w') as f:
    json.dump(all_links, f)

print(f'len(all_links): {len(all_links)}')

def save_url_to_dir_wrapper(url):
    time.sleep(2)
    print(f'save_url_to_dir_wrapper("{url}")')
    print_time()
    save_url_to_dir(url, save_dir_path)

# trying batch size 1 to process sequentially
process_in_batches(all_links, save_url_to_dir_wrapper, 1)
