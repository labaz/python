import os
import re
import json

def convert_line_to_json(file_path):
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            lines = file.readlines()
            # Read line 13 (index 12 since list index starts from 0)
            line_13 = lines[12].strip()  # using strip() to remove any leading/trailing whitespace
            # remove the wayback prefixes
            regex_find = r'"http.://web.archive.org/web/20[\d]{12}/'
            regex_replace = '"'
            line_13 = re.sub(regex_find, regex_replace, line_13)
            return json.loads(line_13)  # Convert the line to JSON
    except Exception as e:
        print(f"An error occurred while processing {file_path}: {e}")
        return None

def process_single_file(file_path, subdir_path):
    json_data = convert_line_to_json(file_path)

    # If conversion is successful, save the JSON
    if json_data is not None:
        file_name = os.path.basename(file_path)
        new_file_name = file_name.replace('.js', '_parsed.json')
        new_file_path = os.path.join(subdir_path, new_file_name)

        # Save the JSON data to a new file
        with open(new_file_path, 'w', encoding='utf-8') as new_file:
            json.dump(json_data, new_file, indent=2)
            print(f"Saved JSON to {new_file_path}")

def process_files_in_subdir(bangs_subdir):
    # Construct the path to the subdir
    current_file_dir = os.path.dirname(os.path.abspath(__file__))
    subdir_path = os.path.join(current_file_dir, bangs_subdir)

    # Iterate through all .js files in the directory
    for file_name in os.listdir(subdir_path):
        if file_name.endswith('.js'):
            file_path = os.path.join(subdir_path, file_name)
            process_single_file(file_path, subdir_path)

# Example usage:
# Replace 'bangs_subdir' with the actual name of your subdirectory
bangs_subdir = 'bangs'
process_files_in_subdir(bangs_subdir)

