import json
import sqlite3
import os

db_file_name = 'ddg-bangs.sqlite'

def create_db_and_tables():
    print('Creating tables')

    conn = sqlite3.connect(db_file_name)
    cursor = conn.cursor()
    sqls = [
      '''
      CREATE TABLE IF NOT EXISTS files (
          file_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
          file_name TEXT UNIQUE NOT NULL);
      ''',

      '''
      CREATE TABLE IF NOT EXISTS bangs (
          file_id INTEGER NOT NULL,
          a TEXT,
          c TEXT,
          d TEXT,
          p TEXT,
          r INTEGER,
          s TEXT,
          sc TEXT,
          t TEXT NOT NULL,  -- Ensuring 't' is NOT NULL for the UNIQUE constraint to make sense
          u TEXT,
          PRIMARY KEY (file_id, t),
          FOREIGN KEY (file_id) REFERENCES files(file_id),
          UNIQUE (file_id, t)
        ) WITHOUT ROWID;
      ''']

    for sql in sqls:
        print(sql)
        cursor.execute(sql)
    conn.commit()
    conn.close()

def insert_file_to_db(file_path):
    print('Inserting file')

    conn = sqlite3.connect(db_file_name)
    cursor = conn.cursor()

    # Step 1: Check the name of the file in files.file_name
    file_name = os.path.basename(file_path)
    cursor.execute('SELECT file_id FROM files WHERE file_name = ?', (file_name,))
    result = cursor.fetchone()
    if result:
        file_id = result[0]
    else:
        print('Creating new file record')
        cursor.execute('INSERT INTO files (file_name) VALUES (?)', (file_name,))
        file_id = cursor.lastrowid
        conn.commit()

    print(f'returned file_id: {file_id}')

    # Step 2: Serialize the file as an array of json objects
    with open(file_path, 'r', encoding='utf-8') as file:
        data = json.load(file)

    bangs_data = []
    for item in data:
        item['file_id'] = file_id
        bangs_data.append((
            file_id,
            # element a is not in every bang file
            ','.join(item.get('a')) if item.get('a') is not None else '',
            item.get('c'),
            item.get('d'),
            item.get('p'),
            item.get('r'),
            item.get('s'),
            item.get('sc'),
            item.get('t'),
            item.get('u')
        ))

    # Step 3: Perform an analog of SQL merge statement (upsert)
    for entry in bangs_data:
        cursor.execute('''
            INSERT INTO bangs (file_id, a, c, d, p, r, s, sc, t, u)
            VALUES (?,?,?,?,?,?,?,?,?,?)
            ON CONFLICT(file_id, t)
            DO UPDATE SET
                a=excluded.a,
                c=excluded.c,
                d=excluded.d,
                p=excluded.p,
                r=excluded.r,
                s=excluded.s,
                sc=excluded.sc,
                t=excluded.t,
                u=excluded.u;
            ''', entry)

    conn.commit()
    conn.close()


create_db_and_tables()

current_file_dir = os.path.dirname(os.path.abspath(__file__))
subdir = 'bangs'
source_dir_path = os.path.join(current_file_dir, subdir)

file_extension = '.json'
files = []
files = [f for f in os.listdir(source_dir_path) if f.endswith(file_extension)]

#files = files[0:2]

for file in files:
    file_path = os.path.join(source_dir_path, file)
    print(file_path)
    insert_file_to_db(file_path)
