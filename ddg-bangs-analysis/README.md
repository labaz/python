# history of DuckDuckGo bangs

ddg bangs allow to search some sites from ddg. Currently ddg supports a few thousand bangs: https://duckduckgo.com/bangs. 

However, in earlier days, there had been more bangs. That's what we do here.

A comprehensive list of historical ddg bangs has been reconstructed from **most** of the snapshots of `bang.js` available on [wayback machine](https://wayback-classic.net/cgi-bin/history.cgi?q=https%3A%2F%2Fduckduckgo.com%2Fbang.js&utf8=%E2%9C%93).

### Results

- [ddg-bangs-web-services.md](./ddg-bangs-web-services.md) is a heavy table providing an overview of web services covered in the bangs
- [ddg-bangs.md](./ddg-bangs.md) is a heavy table with the bangs and search urls
- [ddg-bangs.json](./ddg-bangs.json) is the same as above, in json
- [all_links.json](./all_links.json) contains the links to all wayback machine snapshots as of today

### Data coverage

As of 2024-01-06, the wayback machine contains 500+ snapshots of `bang.js`. Out of those, 377 have been downloaded and parsed in random order. They provide substantial coverage of the wayback machine snapshots since 2015.

Not all of the snapshots from [all_links.json](./all-links.json) have been downloaded because some of them timed out. A slow, sequential download should be able to solve this issue -- but it has not been done.
