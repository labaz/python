import base64
import json
import requests
import os
from flask import Flask, jsonify, redirect, render_template, request

app = Flask(__name__, template_folder="templates")
host = '127.0.0.1'  # Update with the appropriate host
port = 5000  # Update with the appropriate port

# save auth files in the same dir as script
script_dir = os.path.dirname(os.path.realpath(__file__))
auth_file_name = 'auth.json'
refresh_token_file_name = 'refresh_token.json'
auth_file_path = os.path.join(script_dir, auth_file_name) 
refresh_token_file_path = os.path.join(script_dir, refresh_token_file_name)

CLIENT_ID = os.environ.get('SPOTIFY_CLIENT_ID')
CLIENT_SECRET = os.environ.get('SPOTIFY_CLIENT_SECRET')

REDIRECT_URI = f'http://{host}:{port}/callback'
TOKEN_ENDPOINT = 'https://accounts.spotify.com/api/token'

AUTH_SCOPE = 'playlist-modify-private playlist-modify-public playlist-read-private playlist-read-collaborative user-library-modify user-library-read user-read-private user-library-read user-read-recently-played user-top-read'

spotify_market = "CH"

authenticated = False  # Track authentication status
access_token = None  # Store the access token

def get_from_spotify_api(spotify_endpoint):
    global access_token
    headers = {
        'Authorization': f"Bearer {access_token}"
    }
    # get stuff
    response = requests.get(spotify_endpoint, headers=headers)
    return response.json()

def get_spotify_playlist_meta(playlist_id):
    fields="name,description,uri,owner,external_urls,followers,href,public,images"
    url=f"https://api.spotify.com/v1/playlists/{playlist_id}?fields={fields}"
    return get_from_spotify_api(url)

def get_spotify_album_items(album_id):
    # album_ids can be a list or string
    items=[]
    limit=30
    url=f"https://api.spotify.com/v1/albums/{album_id}/tracks?limit={limit}"
    response=get_from_spotify_api(url)
    items=response["items"]
    next=response["next"]
    while(next):
        response=get_from_spotify_api(response["next"])
        more_items=response["items"]
        items.extend(more_items)
        next=response["next"]
    return items
 


def get_spotify_playlist_items(playlist_id):
    items=[]
    limit=100
    url=f"https://api.spotify.com/v1/playlists/{playlist_id}/tracks?limit={limit}"
    response=get_from_spotify_api(url)
    items=response["items"]
    # there is more tracks? fetch them
    next=response["next"]
    while(next):
        response=get_from_spotify_api(response["next"])
        more_items=response["items"]
        items.extend(more_items)
        next=response["next"]
    return items

def get_spotify_search(q, search_type='album'):
    global spotify_market
    separator = ","

    allowed_search_types = ["album", "artist", "playlist", "track", "show", "episode", "audiobook"]

    used_search_types = list(set(search_type.split(separator)).intersection(set(allowed_search_types)))

    used_search_types_string = separator.join(used_search_types)

    items=[]
    limit=50
    market=spotify_market
    market_string = f'&market={spotify_market}'

    market_string = '' # don't use market, let's see what playlists give

    url=f"https://api.spotify.com/v1/search?q={q}&type={used_search_types_string}&limit={limit}{market_string}"
    base_response=get_from_spotify_api(url)

    for search_type in used_search_types:
        search_type_element = f"{search_type}s"
        
        next = base_response[search_type_element]["next"]

        # check if more results: artist, album, track
        while(next):
            response = get_from_spotify_api(next)
            more_items = response[search_type_element]["items"]
            base_response[search_type_element]["items"].extend(more_items)
            next=response[search_type_element]["next"]
        
        # separate handling for playlist items
        if search_type in ['playlist','album']:
            playlists = base_response[search_type_element]["items"]
            item_id = 0
            for playlist in playlists:
                playlist_id = playlist["id"]
                
                if search_type == 'playlist':
                    nested_items = get_spotify_playlist_items(playlist_id)
                elif search_type == 'album':
                    nested_items = get_spotify_album_items(playlist_id)

                base_response[search_type_element]["items"][item_id]["items"] = nested_items
                item_id = item_id + 1

    return base_response
    

    

# Step 1: Redirect the user to the Spotify authorization page
@app.route('/login')
def login():
    global AUTH_SCOPE
    authorize_url = 'https://accounts.spotify.com/authorize'
    params = {
        'response_type': 'code',
        'client_id': CLIENT_ID,
        'redirect_uri': REDIRECT_URI,
        'scope': AUTH_SCOPE,
    }
    authorization_url = authorize_url + '?' + '&'.join([f'{key}={value}' for key, value in params.items()])
    return redirect(authorization_url)

# Step 2: Handle the callback from Spotify
@app.route('/callback')
def callback():
    global authenticated, access_token, auth_file_path
    code = request.args.get('code')
    if code:
        # Step 3: Exchange the authorization code for an access token
        data = {
            'grant_type': 'authorization_code',
            'code': code,
            'redirect_uri': REDIRECT_URI,
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        }
        response = requests.post(TOKEN_ENDPOINT, data=data)
        if response.status_code == 200:
            # Step 4: Use the access token to make API requests
            authenticated = True
            # save authentication response
            with open(auth_file_path, 'w') as file:
                json.dump(response.json(), file)
            print(f"Auth file written: {auth_file_path}")
            access_token = response.json()['access_token']
            return redirect('/')
        else:
            print(response.json())
            return 'Failed to retrieve access token.'
    else:
        return 'Authorization code not found.'

@app.route('/')
def show_main():
    global authenticated, access_token
    # try to obtain refresh token
    access_token = refresh_access_token()
    if(access_token):
        authenticated = True
        #index_path = os.path.join(script_dir,"templates","index.html")
        return render_template("index.html")
    else:
        # if auth file is present, refresh token
        return redirect('/login')

def refresh_access_token():
    global auth_file_path, refresh_token_file_path, CLIENT_ID, CLIENT_SECRET
    
    if not(os.path.exists(auth_file_path)):
        return
    
    # assumes that you are authenticated, and auth json file is present
    with open(auth_file_path, 'r') as file:
        auth_data = json.load(file)
    refresh_token = auth_data["refresh_token"]

    credentials = f"{CLIENT_ID}:{CLIENT_SECRET}"
    base64_client_id_client_secret = base64.b64encode(credentials.encode()).decode()
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': f"Basic {base64_client_id_client_secret}"
    }
    data = {
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token,
            'client_id': CLIENT_ID,
        }
    response = requests.post(TOKEN_ENDPOINT, headers=headers, data=data)

    if response.status_code == 200:
        # Step 4: Use the access token to make API requests
        authenticated = True
        # save authentication response
        with open(refresh_token_file_path, 'w') as file:
            json.dump(response.json(), file)
        print(f"Refresh token file written: {refresh_token_file_path}")
        # 
        access_token = response.json()['access_token']
        return access_token
    else:
        print(response.json())
        print('Failed to refresh access token.')
        return None

# Define a route to handle the search and return JSON results
@app.route('/search')
def search():
    q = request.args.get('q', '')
    search_type = request.args.get('search_type', '')

    # Perform the search (you can replace this with your own logic)
    # For demonstration, we'll return sample data
    if q:
        results = get_spotify_search(q, search_type)
        # write search_results to json
        
        results_file_name = 'search_result.json'
        results_file_path = os.path.join(script_dir, results_file_name)
        
        with open(results_file_path, 'w', encoding='utf-8') as f:
            json.dump(results, f, indent=2) 

        return results
    else:
        return None


if __name__ == '__main__':
    app.run(host=host, port=port)
