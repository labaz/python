import requests
import json
import os
import datetime, time
import io
import sys
import argparse

get_all_releases_for_types = ['artist', 'label']

# Create the parser
description = f"""
Writes a JSON with Discogs search results to
the same directory.

Preparation:
- Register and log on to Discogs
- Create a Discogs application
  https://www.discogs.com/settings/developers
- From that application, you will get Consumer Key
  and Consumer Secret
- Put them in environment variables:
  DISCOGS_CONSUMER_KEY & DISCOGS_CONSUMER_SECRET

Results:
- If no -t provided, will return the results exactly
  as in the search results on Discogs website.
- If -t is one of [{", ".join(get_all_releases_for_types)}],
  then all releases will be returned.

Examples:
- All releases of a label:
  python {__file__} -t label metalheadz
  python {__file__} -t label 'Konvex | Konkav'

- All releases of an artist
  python {__file__} -t artist ltj bukem

- All search results
  python {__file__} Ellen Allien

Note the rate limit on Discogs: 60 requests per 60 seconds.
One request contains up to 100 results.

"""

parser = argparse.ArgumentParser(description="Discogs searcher")

# Add arguments
parser.add_argument("-t", "--type", type=str, default='all', help="one of: release, master, artist, label")
parser.add_argument("-q", type=str, help="search query")

# Check if no arguments were provided
if len(sys.argv) == 1:
    parser.print_help()
    print(description)
    parser.exit()

# Parse the arguments
args, unknown_args = parser.parse_known_args()

if args.q is None and unknown_args is not None:
    args.q = ' '.join(unknown_args)

if args.q == '':
    print('provide search query')
    exit()

discogs_query = {'q': args.q, 'type': args.type}

current_file_dir = os.path.dirname(os.path.abspath(__file__))
subdir = ''
save_dir_path = os.path.join(current_file_dir, subdir)

# Set the standard output encoding to UTF-8, that's for windows
sys.stdout = io.TextIOWrapper(sys.stdout.detach(), encoding = 'utf-8', line_buffering = True)

discogs_consumer_key = os.getenv('DISCOGS_CONSUMER_KEY')
discogs_consumer_secret = os.getenv('DISCOGS_CONSUMER_SECRET')

discogs_api_url = 'https://api.discogs.com'
discogs_search_url = 'https://api.discogs.com/database/search'

# results per page is relevant for rate limiting: make fewer requests
per_page = 100

user_agent = 'discogssearcher'
headers = {
    "Authorization": f"Discogs key={discogs_consumer_key}, secret={discogs_consumer_secret}",
    "User-Agent": user_agent
}

discogs_ratelimit_headers = {
    "X-Discogs-Ratelimit-Remaining": "1",
    "X-Discogs-Ratelimit-Used": "1",
    "X-Discogs-Ratelimit": "1",
    'rolling_window_end': datetime.datetime.now()
        + datetime.timedelta(seconds=60),
    'rolling_window_seconds': 60,
}


def discogs_all_releases(type, id):
    url = f"/{type}s/{id}/releases?per_page={per_page}"

    request_url = f"{discogs_api_url}{url}"
    response = discogs_request_with_rate_limit(request_url)

    return pagination_get_all(response)


def discogs_search(params):
    url = "/database/search"
    request_url = f"{discogs_api_url}{url}"
    response = discogs_request_with_rate_limit(request_url, params=params)
    
    # add handling when there is an API error 
    if 'message' in response:
        print(response["message"])
        exit()

    # for search type artist and label we only need the first element
    # therefore, we don't need next pages and we can save requests
    if params.get('type') in get_all_releases_for_types:
        return response

    return pagination_get_all(response)


def pagination_get_all(object):
    # if there is ["pagination"].["urls"].["next"]
    try:
        next_url = object["pagination"]["urls"]["next"]
    except:
        return object

    # get the type of object (e.g. "releases")
    exclude_keys = ['pagination']
    object_type = [key for key in object.keys() if key not in exclude_keys][0]

    # get the next url and get the list of objects
    next_object = discogs_request_with_rate_limit(next_url)

    # add current list of objects to the next list
    # next list has actual "next" url
    next_object[object_type].extend(object[object_type])

    return pagination_get_all(next_object)


def discogs_request_with_rate_limit(url, params=None):
    if int(discogs_ratelimit_headers["X-Discogs-Ratelimit-Remaining"]) > 0:

        session = requests.Session()

        request = requests.Request('GET', url, params=params, headers=headers)
        prepared_request = request.prepare()
        full_url = prepared_request.url

        print(full_url, file=sys.stdout)

        response = session.send(prepared_request)

        for key in discogs_ratelimit_headers:
            value = response.headers.get(key)
            if value:
                discogs_ratelimit_headers[key] = value
        if int(discogs_ratelimit_headers["X-Discogs-Ratelimit-Used"]) == 1:
            discogs_ratelimit_headers["rolling_window_end"] = (
                datetime.datetime.now() +
                datetime.timedelta(seconds=discogs_ratelimit_headers["rolling_window_seconds"])
            )
        return response.json()
    else:
        time_to_wait_seconds = (
            discogs_ratelimit_headers["rolling_window_end"] -
            datetime.datetime.now()).seconds()
        # wait until the end of the 60 second window
        print(
            f"Need to wait {time_to_wait_seconds} seconds until quota refresh.",
            file=sys.stdout)
        time.sleep(time_to_wait_seconds)
        return discogs_ratelimit_headers(url, params)


def safe_filename(filename):
    # replacing disallowed chars in filenames a la yt-dlp
    replacements = {
                '<': '＜',  # Fullwidth less-than sign
                '>': '＞',  # Fullwidth greater-than sign
                ':': '：',  # Fullwidth colon
                '"': '＂',  # Fullwidth quotation mark
                '/': chr(10744),
                '\\': chr(10745),
                '|': chr(65372),  # Fullwidth vertical line
                '?': '？',  # Fullwidth question mark
                '*': '＊'   # Fullwidth asterisk
    }

    for disallowed, safe in replacements.items():
        filename = filename.replace(disallowed, safe)

    return filename


# these are search parameters
params = {
    'q': discogs_query['q'],
    'per_page': per_page,
    'type': discogs_query['type'], #args.type,  # release, master, artist, label
}

response = discogs_search(params)

#print(response)

# if artist or label - also get releases
# assume that the first one is the right one
if discogs_query['type'] in get_all_releases_for_types:
    id = response['results'][0]['id']
    type = response['results'][0]['type']
    response = discogs_all_releases(type, id)


output_file_name = f"discogs_{params['type']}_'{params['q']}'.json"
output_file_name = safe_filename(output_file_name)
output_file_path = os.path.join(save_dir_path, output_file_name)

with open(output_file_path, 'w', encoding='utf-8') as f:
    json.dump(response, f)

print(output_file_path)
