import pymorphy2
import sys
import inspect
#from typing import NoneType

def decline_word(word):
    morph = pymorphy2.MorphAnalyzer()
    parsed = morph.parse(word)[0]
    #print(parsed)
    #print(type(parsed))
    try:
      declined = parsed.inflect({'gent'}).word
    except:
      declined=word
    return declined

# Check if there are command line arguments
input_words=[]
if len(sys.argv) > 1:
    input_words.append(' '.join(sys.argv[1:]))
else:
    # Read input from pipe
    input_words = [line.strip() for line in sys.stdin.readlines()]

for w in input_words:
  #print(f"w: {w}")
  if w == None:
    continue
  result=[]
  for w2 in w.split(" "):
    word = decline_word(w2)
    capitalized_result = word.capitalize()
    result.append(capitalized_result)
  print(" ".join(result))
