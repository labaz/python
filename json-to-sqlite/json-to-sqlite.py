#!/usr/bin/env python3

import sqlite3
import json
import os
import sys

# Function to ensure data is in SQLite-compatible format
def convert_value(value):
    if isinstance(value, (dict, list)):
        return json.dumps(value)  # Convert to JSON string
    return str(value) if value is not None else None  # Convert to string or None

# Program usage note
NOTE = """
This application converts a JSON file into an SQLite database.
The JSON file should contain an array of objects.
Each object's keys will be used as columns in the SQLite database.
Include the parameter '--all-keys' to collect keys from all objects.
"""

# Parse command-line arguments
if len(sys.argv) < 2:
    print(NOTE)
    print(f"Usage: {sys.argv[0]} <path_to_json> [--all-keys]")
    sys.exit(1)

JSON_FILE_PATH = sys.argv[1]
get_columns_from_all_elements = False

# Parse optional arguments
args = sys.argv[2:]
if '--all-keys' in args:
    get_columns_from_all_elements = True

# Determine the SQLite file name from the JSON file name (change extension to .sqlite)
json_dir = os.path.dirname(JSON_FILE_PATH)
json_base = os.path.splitext(os.path.basename(JSON_FILE_PATH))[0]
SQLITE_DB_PATH = os.path.join(json_dir, f"{json_base}.sqlite")

# Determine table name from JSON file name (without extension)
TABLE_NAME = json_base

# Read the JSON file
with open(JSON_FILE_PATH, 'r') as f:
    data = json.load(f)

# Check that data is a list of dictionaries
if not isinstance(data, list) or not all(isinstance(obj, dict) for obj in data):
    raise ValueError("The JSON file should contain an array of objects")

# Determine columns from data
if get_columns_from_all_elements:
    columns = set(key for obj in data for key in obj.keys())
else:
    columns = data[0].keys()

# SQL statement templates
DROP_TABLE_SQL = f"DROP TABLE IF EXISTS {TABLE_NAME}"
column_defs = ", ".join(f"{col} TEXT" for col in columns)  # assuming all text fields
CREATE_TABLE_SQL = f"CREATE TABLE {TABLE_NAME} ({column_defs})"
INSERT_SQL = f"INSERT INTO {TABLE_NAME} ({', '.join(columns)}) VALUES ({', '.join(['?'] * len(columns))})"

# Create SQLite connection and cursor
conn = sqlite3.connect(SQLITE_DB_PATH)
cur = conn.cursor()

# Drop old table if it exists
cur.execute(DROP_TABLE_SQL)

# Create table
cur.execute(CREATE_TABLE_SQL)

# Prepare data for bulk insert
values_list = [[convert_value(obj.get(col)) for col in columns] for obj in data]

# Insert data in bulk
cur.executemany(INSERT_SQL, values_list)

# Commit and close
rowcount = cur.rowcount
conn.commit()
cur.close()
conn.close()

print(f"Data has been successfully imported into {SQLITE_DB_PATH}")
print(f"Total rows inserted: {rowcount}")

