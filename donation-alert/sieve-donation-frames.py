import os
from skimage.metrics import structural_similarity as compare_ssim
from skimage import io

# Paths to example donation frames
example_frame_paths = [
    'C:\\temp\\yt-dlp\\youtube.com\\iXBT games\\20230825 - [СТРИМ] AMD Gaming Festival 2023 [18_00 по МСК] + Armored Core 6 ⧸⧸ gamescom 2023\\0061_1.png', 
    'C:\\temp\\yt-dlp\\youtube.com\\iXBT games\\20230825 - [СТРИМ] AMD Gaming Festival 2023 [18_00 по МСК] + Armored Core 6 ⧸⧸ gamescom 2023\\3270_2.png',
    'C:\\temp\\yt-dlp\\youtube.com\\iXBT games\\20230825 - [СТРИМ] AMD Gaming Festival 2023 [18_00 по МСК] + Armored Core 6 ⧸⧸ gamescom 2023\\0088_1.png'
]

all_frames_dir = 'C:\\temp\\yt-dlp\\youtube.com\\iXBT games\\20230825 - [СТРИМ] AMD Gaming Festival 2023 [18_00 по МСК] + Armored Core 6 ⧸⧸ gamescom 2023\\'
output_directory = 'C:\\temp\\yt-dlp\\youtube.com\\iXBT games\\20230825 - [СТРИМ] AMD Gaming Festival 2023 [18_00 по МСК] + Armored Core 6 ⧸⧸ gamescom 2023\\donation_frames'

os.makedirs(output_directory, exist_ok=True)

# Load example frames
example_frames = [io.imread(path) for path in example_frame_paths]

# Set a fixed odd value for dynamic_win_size
dynamic_win_size = 3

for filename in os.listdir(all_frames_dir):
    if filename.endswith('.png'):
        frame_path = os.path.join(all_frames_dir, filename)
        frame = io.imread(frame_path)

        print(frame_path)

        # Calculate similarity for each example frame
        similarity_threshold = 0.7  # Adjust as needed
        is_donation_frame = any(
            compare_ssim(example_frame, frame, win_size=dynamic_win_size, multichannel=True) > similarity_threshold
            for example_frame in example_frames
        )

        if is_donation_frame:
            output_path = os.path.join(output_directory, filename)
            io.imsave(output_path, frame)
