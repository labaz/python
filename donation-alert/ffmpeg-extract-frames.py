import os
import re
import subprocess

video_file_path = 'c:\\temp\\yt-dlp\\youtube.com\\iXBT games\\20230825 - [СТРИМ] AMD Gaming Festival 2023 [18_00 по МСК] + Armored Core 6 ⧸⧸ gamescom 2023.webm'
alert_duration_seconds = 8
w = 300
h = 380
alert_display_offsets = [
    {'x': 803, 'y': 479, 'w': w, 'h': h},
    {'x': 1920-w, 'y': 1080-h, 'w': w, 'h': h}
]
threads = 16

alert_sampling_seconds = alert_duration_seconds - 1

# Create the subdirectory name by stripping non-ASCII characters
base_filename = os.path.splitext(os.path.basename(video_file_path))[0]
output_directory = os.path.join(os.path.dirname(video_file_path), base_filename)
os.makedirs(output_directory, exist_ok=True)

ffmpeg_command_parts = [
    'ffmpeg', '-i', f'"{video_file_path}"', '-threads', f'{threads}'
]

# Build the FFmpeg command with separate -vf options and output filenames
for idx, offset in enumerate(alert_display_offsets):
    vf_option = f'fps=1/{alert_sampling_seconds},crop={offset["w"]}:{offset["h"]}:{offset["x"]}:{offset["y"]}'
    output_filename = os.path.join(output_directory, f'%04d_{idx+1}.png')
    ffmpeg_command_parts.extend(['-vf', f'"{vf_option}"', f'"{output_filename}"'])

ffmpeg_command = ' '.join(ffmpeg_command_parts)

print(ffmpeg_command)


# Execute the FFmpeg command
try:
    subprocess.run(ffmpeg_command, shell=True, check=True)
except subprocess.CalledProcessError as e:
    print(f"Error executing FFmpeg command: {e}")