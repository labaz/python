import os
import numpy as np
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator
import tensorflow as tf
from tensorflow.keras.models import load_model
import cv2

# Define paths to labeled images
donation_frames_dir = 'C:\\temp\\training\\donation_frames'
non_donation_frames_dir = 'C:\\temp\\training\\non_donation_frames'
training_data_dir = 'C:\\temp\\training'
all_frames_dir = training_data_dir

model_file_path = os.path.join(training_data_dir, 'donation_frame_classifier.keras')

def train_model():
    # Define the neural network architecture
    model = Sequential()
    model.add(Conv2D(32, (3, 3), activation='relu', input_shape=(128, 128, 3)))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Conv2D(128, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

    # Data preprocessing and augmentation
    datagen = ImageDataGenerator(
        rescale=1./255,
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest'
    )

    train_generator = datagen.flow_from_directory(
        training_data_dir,
        target_size=(128, 128),
        batch_size=32,
        class_mode='binary'
    )

    # Train the model
    model.fit(train_generator, steps_per_epoch=len(train_generator), epochs=10)

    # Save the trained model in the training data directory using native Keras format
    model.save(model_file_path)

def run_model():
    # Load the trained model
    loaded_model = tf.keras.models.load_model(model_file_path)

    # Directory to store detected donation frames
    detected_donation_frames_dir = os.path.join(training_data_dir, 'detected_donation_frames')
    os.makedirs(detected_donation_frames_dir, exist_ok=True)

    # Loop through video frames and perform inference
    for filename in os.listdir(all_frames_dir):
        if filename.endswith('.png'):
            frame_path = os.path.join(all_frames_dir, filename)
            frame = cv2.imread(frame_path)
            
            if frame is not None:
                # Preprocess the frame (if needed)
                # ... (resize and normalize similar to training)

                # Perform inference
                prediction = loaded_model.predict(np.expand_dims(frame, axis=0))
                
                # Check if the frame is predicted as a donation frame
                if prediction > 0.5:  # You may need to adjust this threshold
                    output_path = os.path.join(detected_donation_frames_dir, filename)
                    cv2.imwrite(output_path, frame)


if not os.path.exists(model_file_path):
    train_model()
else:
    run_model()