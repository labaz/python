import os, sys
import chardet

filepath = ' '.join(sys.argv[1:])

def get_charset(filepath):
  if not os.path.isfile(filepath):
    print(f"File '{filepath}' does not exist.")
    return None
  with open(filepath, 'rb') as f:
    content = f.read()
    result = chardet.detect(content)
    encoding = result['encoding']
    return encoding
 
print(get_charset(filepath))




