import os
import re
from termcolor import colored
from lxml import html, etree
from lxml.builder import ElementMaker,E
from ebooklib import epub

def resolve_file_url(file_url):
    if re.match(r'^file:///', file_url):
        file_url = re.sub(r'^file:///', '', file_url)

    return file_url

# scrap single html with one or multiple xpaths
def extract_html_elements(file_path, xpath_selectors):
    resolved_path = resolve_file_url(file_path)

    with open(resolved_path, 'r', encoding='utf-8') as f:
        tree = html.fromstring(f.read())
        results = []

        # this namespace is mandatory to use re: in xpath
        namespaces={'re': 'http://exslt.org/regular-expressions'}

        if isinstance(xpath_selectors, str):
            # If there's only one selector, use it directly
            elements = tree.xpath(xpath_selectors, namespaces = namespaces)
            if elements:
                results.append(elements)
            else:
                results.append([])
        else:
            # If there are multiple selectors, loop through them
            for selector in xpath_selectors:
                elements = tree.xpath(selector, namespaces = namespaces)
                if elements:
                    results.append(elements)
                else:
                    results.append([])
        
        # return a single object without array
        if len(results) == 1:
            return results[0]
        # otherwise return tuple, i.e. many values
        return tuple(results)

 ######  ########    ###    ########  ######## 
##    ##    ##      ## ##   ##     ##    ##    
##          ##     ##   ##  ##     ##    ##    
 ######     ##    ##     ## ########     ##    
      ##    ##    ######### ##   ##      ##    
##    ##    ##    ##     ## ##    ##     ##    
 ######     ##    ##     ## ##     ##    ##

file_path = f'c:\\Temp\\wget\\namednibook.ru\\years\\index.html'

########   #######   #######  ######## 
##     ## ##     ## ##     ##    ##    
##     ## ##     ## ##     ##    ##    
########  ##     ## ##     ##    ##    
##   ##   ##     ## ##     ##    ##    
##    ##  ##     ## ##     ##    ##    
##     ##  #######   #######     ##

# load root file and get the links + metadata from it

link_include_regex = r'^file'
link_exclude_regex = r'[\d]{4}\.htm|/volumes|/index\.htm'

print(f'Collecting data from root html file', colored(file_path, 'magenta'))

book_title, lang, links, link_texts, years_links_results, content = \
extract_html_elements(
    file_path, 
    [
        "string(//title/text())", # book_title    
        "string(//html/@lang)", # lang
        f'//a[@href][re:match(@href, "{link_include_regex}") and not(re:match(@href, "{link_exclude_regex}"))]/@href', # links
        f'//a[@href][re:match(@href, "{link_include_regex}") and not(re:match(@href, "{link_exclude_regex}"))]/text()', # link_texts
        '//li[@class="phenomena-list__item"]', # years_links_results
        "//main/div[@class='wrapper']" # content
    ]
    )

##       #### ##    ## ##    ##  ######  
##        ##  ###   ## ##   ##  ##    ## 
##        ##  ####  ## ##  ##   ##       
##        ##  ## ## ## #####     ######  
##        ##  ##  #### ##  ##         ## 
##        ##  ##   ### ##   ##  ##    ## 
######## #### ##    ## ##    ##  ######

# the result of this section will be used to build the file
# epub section names are created here, too

print(f'Extracting links')

years_links = {}
just_links = []
link_mapping_old_new = {}

link_index = 1
for r in years_links_results:
    year = r.xpath('./a[@class="phenomena-list__year-link"]/text()')[0]
    links = r.xpath('.//li[@class="phenomena-list__year-item"]/a')
    if not links:
        continue

    links_data = []
    for link in links:
        link_data = {}

        link_data['year'] = year
        link_data['title'] = link.attrib['title']
        link_data['href'] = link.attrib['href']
        link_data['text'] = link.xpath('./text()')[0]

        link_data['first_letter'] = re.sub(r'[^a-zA-Z0-9а-яА-Я]','',link.text)[0].upper()

        basename = os.path.splitext(os.path.basename(link_data['href']))[0]
        link_data['filename_inside_epub'] = f'Article_{link_index:05d}_{basename}.xhtml'

        link_mapping_old_new[link_data['href']] = link_data['filename_inside_epub']

        links_data.append(link_data)
        link_index = link_index + 1

    just_links.extend(links_data)
    years_links[year] = links_data

# sort contents
for key in years_links:
    years_links[key].sort(key=lambda x: x['text'])

# sort keys
years_links = dict(sorted(years_links.items()))

print(f'Links extracted:', colored(len(just_links),'green'))

######## ########  ##     ## ########  
##       ##     ## ##     ## ##     ## 
##       ##     ## ##     ## ##     ## 
######   ########  ##     ## ########  
##       ##        ##     ## ##     ## 
##       ##        ##     ## ##     ## 
######## ##         #######  ########

# start creating ebook

book = epub.EpubBook()
spine = [] # the sequence of files
toc = [] # links and sections

author = "Леонид Парфёнов"
book.set_title(book_title)
book.add_author(author)
book.set_language(lang)

file_name = f"{author} - {book_title}.epub"

#### ##    ## ########  ######## ##     ## 
 ##  ###   ## ##     ## ##        ##   ##  
 ##  ####  ## ##     ## ##         ## ##   
 ##  ## ## ## ##     ## ######      ###    
 ##  ##  #### ##     ## ##         ## ##   
 ##  ##   ### ##     ## ##        ##   ##  
#### ##    ## ########  ######## ##     ##

print("Creating alphabetical index")

first_letter_and_links = {}

for link in just_links:
    first_letter = link['first_letter']

    letter = first_letter
    if re.match(r'\d', first_letter):
        letter = '#'
    
    if not letter in first_letter_and_links.keys():
        first_letter_and_links[letter] = []
    
    first_letter_and_links[letter].append(link)

# sort contents
for key in first_letter_and_links:
    first_letter_and_links[key].sort(key=lambda x: x['text'])

# sort keys
first_letter_and_links = dict(sorted(first_letter_and_links.items()))

# create html for index

M=ElementMaker(namespace=None,
               nsmap={None: "http://www.w3.org/1999/xhtml"})
body = E.body()

index_title = 'По алфавиту'
body.append(M.h1(index_title))

for key in first_letter_and_links:
    nr_items = len(first_letter_and_links[key])

    subheading = f"{key} [{nr_items}]"

    a_name = f'_index_alphabetical_{key}'

    body.append(M.h2(subheading, name=a_name))

    ul = M.ul()
    for item in first_letter_and_links[key]:
        link_text = f"{item['title']} ({item['year']})"
        link_href = item['filename_inside_epub']
        a = M.a(link_text,href=link_href)
        li = M.li(a)
        ul.append(li)
    body.append(ul)

index_html = M.html(body)
index_content = etree.tostring(index_html,
                        xml_declaration=True,
                        doctype='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">',
                        encoding='utf-8',
                        standalone=False,
                        with_tail=False,
                        method='xml',
                        pretty_print=True)

index_file_name = '_index_alphabetical.xhtml'
index = epub.EpubHtml(title=index_title, file_name=index_file_name)
index.content = index_content

# add index to the beginning
spine.insert(0,index)
book.add_item(index)

index_link = epub.Link(index_file_name, index_title,'index')
# prepend index to toc
toc.insert(0,index_link) 

##    ## ########    ###    ########   ######  
 ##  ##  ##         ## ##   ##     ## ##    ## 
  ####   ##        ##   ##  ##     ## ##       
   ##    ######   ##     ## ########   ######  
   ##    ##       ######### ##   ##         ## 
   ##    ##       ##     ## ##    ##  ##    ## 
   ##    ######## ##     ## ##     ##  ######

print("Creating index by years")

# create html for index

M=ElementMaker(namespace=None,
               nsmap={None: "http://www.w3.org/1999/xhtml"})
body = E.body()

years_title = 'По годам'
body.append(M.h1(years_title))

for key in years_links:
    nr_items = len(years_links[key])

    subheading = f"{key} [{nr_items}]"

    a_name = f'_index_years_{key}'

    body.append(M.h2(subheading, name=a_name))

    ul = M.ul()
    for item in years_links[key]:
        link_text = f"{item['title']} ({item['year']})"
        link_href = item['filename_inside_epub']
        a = M.a(link_text,href=link_href)
        li = M.li(a)
        ul.append(li)
    body.append(ul)

years_html = M.html(body)
years_content = etree.tostring(years_html,
                        xml_declaration=True,
                        doctype='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">',
                        encoding='utf-8',
                        standalone=False,
                        with_tail=False,
                        method='xml',
                        pretty_print=True)

years_file_name = '_index_years.xhtml'
index = epub.EpubHtml(title=years_title, file_name=years_file_name)
index.content = years_content

# add years in second position
spine.insert(1,index)
book.add_item(index)

index_link = epub.Link(years_file_name, years_title, 'years')
# prepend index to toc
toc.insert(0,index_link)

 ######   #######  ##    ## ######## ######## ##    ## ######## 
##    ## ##     ## ###   ##    ##    ##       ###   ##    ##    
##       ##     ## ####  ##    ##    ##       ####  ##    ##    
##       ##     ## ## ## ##    ##    ######   ## ## ##    ##    
##       ##     ## ##  ####    ##    ##       ##  ####    ##    
##    ## ##     ## ##   ###    ##    ##       ##   ###    ##    
 ######   #######  ##    ##    ##    ######## ##    ##    ##

print(f'Looping over links and extracting content')

for link in just_links:
    
    title = link['title']
    year = link['year']
    file_path = link['href'] 
    epub_section_filename = link['filename_inside_epub'] 
    first_letter = link['first_letter']


    content_xpath = '//article[@class="post"]/*[not(@class="post__banner")]'

    content = extract_html_elements(file_path, content_xpath)

    content_string = ''.join([etree.tostring(element, encoding='unicode') for element in content])

    # prepend the navigation piece
    index_alpha_href = f'{index_file_name}#_index_alphabetical_{first_letter}'
    index_years_href = f'{years_file_name}#_index_years_{year}'
    navbar = f'''
    <p>Вернуться: год <a href="{index_years_href}">{year}</a>, буква <a href="{index_alpha_href}">{first_letter}</a>.</p>
    '''

    content_string = navbar + content_string

    c = epub.EpubHtml(title=title, file_name=epub_section_filename)
    c.content = content_string

    book.add_item(c)
    spine.append(c)

##       #### ##    ## ##    ##  ######  
##        ##  ###   ## ##   ##  ##    ## 
##        ##  ####  ## ##  ##   ##       
##        ##  ## ## ## #####     ######  
##        ##  ##  #### ##  ##         ## 
##        ##  ##   ### ##   ##  ##    ## 
######## #### ##    ## ##    ##  ######

print(f'Converting links to epub internal')

for item in book.get_items(): 
    
    if item.content is None: 
        continue

    # Parse the contents of the HTML item with etree
    tree = html.fromstring(item.content)

    # Use an XPath expression to select all the <a> elements
    a = tree.xpath('//a')

    # Iterate over the <a> elements
    for link in a:

        # Check if the href attribute is in internal_links
        if link.get('href') in link_mapping_old_new.keys():
            # Replace the href attribute with the internal link
            link.set('href', link_mapping_old_new[link.get('href')])

    # Serialize the modified tree back to a string
    modified_content = html.tostring(tree, encoding='unicode')

    # Update the contents of the HTML item with the modified content
    item.content = modified_content

#### ##     ##  ######   
 ##  ###   ### ##    ##  
 ##  #### #### ##        
 ##  ## ### ## ##   #### 
 ##  ##     ## ##    ##  
 ##  ##     ## ##    ##  
#### ##     ##  ######

print(f'Embedding images')

included_images = {}
image_index = 1

for item in book.get_items(): 
    if item.content is None: 
        continue

    # Parse the contents of the HTML item with etree
    tree = html.fromstring(item.content)

    # Use an XPath expression to select all the <a> elements
    imgs = tree.xpath('//img')

    if len(imgs) == 0: 
        continue

    # Iterate over the <a> elements
    for img in imgs:
        src = img.get('src')
        
        if src in included_images.keys(): 
            img.set('src', included_images[src]) 
            del img.attrib['class']  
            continue

        if src.startswith('http'): continue

        # if img src already in included_images then use its file path - otherwise upload

        original_img_name_ext = os.path.split(src)[1]
        new_img_src = f'IMG{image_index:05d} - {original_img_name_ext}'

        # add image to epub
        img_path = resolve_file_url(src)

        with open(img_path, 'rb') as image_file:
            image_content = image_file.read()
            book.add_item(epub.EpubItem(uid=f"IMG{image_index:05d}", file_name=f"{new_img_src}", media_type="image/jpeg", content=image_content))

        included_images[src] = new_img_src
        image_index = image_index + 1    
        
        img.set('src', included_images[src]) 
        del img.attrib['class']         

    # Serialize the modified tree back to a string
    modified_content = html.tostring(tree, encoding='unicode')

    # Update the contents of the HTML item with the modified content
    item.content = modified_content

print(f'Images embedded:', colored(len(included_images.keys()),'green'))

########  #######   ######  
   ##    ##     ## ##    ## 
   ##    ##     ## ##       
   ##    ##     ## ##       
   ##    ##     ## ##       
   ##    ##     ## ##    ## 
   ##     #######   ######

# loop over years, year will be a section, and links will be sections

for key in years_links.keys():
    items = years_links[key]
    year = key

    sub_links = []
    for item in items:
        # the third parameter in epub.Link() is uid, and it is mandatory 
        # without this parameter, book.add_item(epub.EpubNcx()) will not work
        # without epub.EpubNcx(), some readers will not see the ToC
        link = epub.Link(item['filename_inside_epub'], item['text'], item['filename_inside_epub'])
        sub_links.append(link)

    section = tuple([epub.Section(year,''), tuple(sub_links)])
    toc.append(section)

 ######     ###    ##     ## ######## 
##    ##   ## ##   ##     ## ##       
##        ##   ##  ##     ## ##       
 ######  ##     ## ##     ## ######   
      ## #########  ##   ##  ##       
##    ## ##     ##   ## ##   ##       
 ######  ##     ##    ###    ########

print(f'Saving ebook')

# no more changing toc
book.toc = tuple(toc)

spine.insert(0,'nav')
book.spine = spine

book.add_item(epub.EpubNcx())
book.add_item(epub.EpubNav())

script_dir = os.path.dirname(os.path.abspath(__file__))
epub_path = os.path.join(script_dir,file_name)

epub.write_epub(epub_path, book)

print(f'Ebook saved:', colored((epub_path),'green'))
