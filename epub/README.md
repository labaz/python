# Make an ebook from a website
## Use case
You are at the airport awaiting boarding for a long flight. There's still quite a bit of time left until the boarding, and you are enjoing the complimentary airport wifi browsing the internet.

In search of interesting content to read, a website draws your attention, `https://interesting.site/`. This website hosts valuable knowledge, and you would love to read all the interesting articles from that website, that happen to be conveniently listed in a page `https://interesting.site/interesting-articles/`. 

Would be cool to read them all in your long flight! 

## Goal

Make an ebook (e.g. in epub format) from all those articles for offline reading.

## Requirements

* ✂ Only **specific parts of webpages** should get into the ebook. That is, no sidebars, headers, footers, etc.

* 📗 It should be a **single ebook** containing only the required pages from the website.

* 🗃 The ebook should have a **table of contents**.

* 🌆 The **images** from the webpage should be included in the ebook, too.

* 🔗 If there are **hyperlinks** between the source webpages, they should be functional within the ebook, too.

* 📱🛫 You only have an **Android phone** on you, and there is about 1 hour left until the boarding.

## Approach

There may be different tactics to accomplish this task. Here is what worked for me. 

I see the following distinct stages/activities in this process:

1. Download required webpages and images and save them locally on PC or phone. This will be a job for a website download tool (`wget`).

2. Extract and process the information from the downloaded html files. This is the process of **web scraping**. For best results, `xpath` knowledge is required. `xpath` is a special syntax that allows to select xml/html elements in the manner like _'all contents of div with class "post" but excluding child elements with class "post__banner"'_

3. Make an ebook. There are converters and libraries for that.

## Tools to use

We are going to do this in `bash` and `python`. 

Both are available and functioning on my Samsung S20+ phone running Android OS. Having a Linux terminal on Android is a great pleasure in itself and deserves a separate story.

**TL/DR:** thanks to [Termux](https://termux.dev/en/), you get a Linux environment with full-fledged python on your Android phone.

## Step by step

### 1. Download webpage and its successors (recursive download)
If you download a page and its related pages, it's called "recursive" download. Linux offers a number of tools for recursive website downloading.

* `httrack` - did not work out for my website because it did not support redirects

* `wget` - worked out really great, and here is how:

```sh
wget --recursive --level=1 --no-clobber --page-requisites --html-extension --convert-links --domains interesting.site https://interesting.site/interesting-articles/
```

Used `wget` command line parameters:

* `--recursive ` follow the links in the source webpage and download those webpages, too

* `--level=1 ` when following and downloading the links recursively, only do that for the first page, i.e. download the page plus the pages linked from it (and their images will be included, too, thanks to `--page-requisites`)

* `--no-clobber` do not re-download the files if they are already present in the destination directory

* `--page-requisites ` download all the files needed to display the page properly (like images, but we will further restrict this with `--accept` parameter)

* `--html-extension ` save all files with a `.html` extension. Especially useful if there is no visible file behind a link

* `--convert-links ` convert the links so that they work locally, pointing to the downloaded files

* `--domains interesting.site ` only download files and webpages from this domain, ignore all external links

* `https://interesting.site/interesting-articles/ ` that's the URL of our root page

My webpage and its related pages and files took 11 minutes to load. This is an excerpt of the `wget` output:

```
FINISHED --2023-04-25 22:21:46--
Total wall clock time: 11m 45s
Downloaded: 2882 files, 195M in 1m 37s (2.01 MB/s)
```

Here's how many files have been downloaded, by extension:

```
css   1
html  1971
jpg   905
png   4
```

To make this overview, I ran the following bash one-liner in the root directory `interesting.site` automatically created by `wget`:

```sh
find . -type f | grep -Eio '[^.]+$' | sort | uniq -c | awk '{print $2, $1}' | column -t
```

The files are here within 12 minutes. That's very fortunate, because downloading was the only thing that required an internet connection.

### 2. Identify the needed html elements, and scrap them and modify

Now that the files are here, we are going to scrap them with `python`. To "scrap" means to "extract needed information". I use python module `lxml` because it supports `xpath` very well, and I have prior experience with `xpath`. Python module `bs4` would do this job as well, but I don't have experience with it.

To know what to scrap, it's inevitable that you need to look into the webpages and analyze their structure. Then you find, which combination of element, id, and class would fully encompass your article content and contain as little irrelevant information as possible. 

I found out that in my downloaded pages, the relevant content was in an `<article class = "post">`. But in every page, this `<article>` element contained a footer `<div class="post__banner">`, containing some general information about the website. I did not need that one.

That's why I made this `xpath` query: 
```xpath
//article[@class="post"]/*[not(@class="post__banner")]
```
which means: give me all contents of `<article class = "post">` except for those that have `class="post__banner"`. 

I create a very helpful python function `extract_html_elements(file_path, xpath_selectors)` that allows me to open an `html` file and execute multiple xpath queries on it, returning a variety of information (whether lists, objects, or scalar values) in a single call.

# To be continued

### Make html links also work in epub - i.e. normalize links
* Convert all relative links to absolute - then a link to the same file will be the same in every page, regardless of level. This makes possible easy link mapping from `html` in the source file to `xhtml` in epub. 

### Assemble html files into a single epub using `ebooklib`
* Sections
* Images
* Table of contents & navigation
* Pitfalls (link id is necessary)

### Create fancy nav pages and elements using `lxml`
* Alphabetical index
* Index by years
* Links "back"