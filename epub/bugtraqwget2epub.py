import os 
import sys
import re
import chardet
from termcolor import colored
from lxml import html, etree
from lxml.builder import ElementMaker,E
from ebooklib import epub

import os

def get_absolute_path(main_html_path, relative_href):
  main_dir = os.path.dirname(main_html_path)
  absolute_path = os.path.normpath(os.path.join(main_dir, relative_href))
  return absolute_path

def resolve_file_url(file_url):
  if re.match(r'^file:///', file_url):
    file_url = re.sub(r'^file:///', '', file_url)
  return file_url

def get_charset(filepath):
  if not os.path.isfile(filepath):
    print(f"File '{filepath}' does not exist.")
    return None
  with open(filepath, 'rb') as f:
    content = f.read()
    result = chardet.detect(content)
    encoding = result['encoding']
    return encoding

# scrap single html string or file with one or multiple xpaths
def extract_html_elements(path_or_html, xpath_selectors):
  # it should accept file path or html string
  try:
    if (os.path.isfile(path_or_html)):
      resolved_path = path_or_html
      file_charset = get_charset(resolved_path)
      with open(resolved_path, 'r', encoding=file_charset) as f:
        tree = html.fromstring(f.read())
    else:
      html_string = path_or_html
      tree = html.fromstring(html_string)
  except:
    print(colored("[extract_html_elements] The input should be an html string or a path to existing file. The supplied input is not: ", "red"), path_or_html)
    return None
  
  results = []

  # this namespace is mandatory to use re: in xpath
  namespaces={'re': 'http://exslt.org/regular-expressions'}

  if isinstance(xpath_selectors, str):
    # If there's only one selector, use it directly
    elements = tree.xpath(xpath_selectors, namespaces = namespaces)
    if elements:
      results.append(elements)
    else:
      results.append([])
  else:
    # If there are multiple selectors, loop through them
    for selector in xpath_selectors:
      elements = tree.xpath(selector,namespaces = namespaces)
      if elements:
        results.append(elements)
      else:
        results.append([])
  
  # return a single object without array
  if len(results) == 1:
    return results[0]
  # otherwise return tuple, i.e. many values
  return tuple(results)

################################

file_path = f'/storage/emulated/0/wget/bugtraq.ru/library/underground/index.html'
author = "bugtraq.ru"

################################
"""
ROOT FILE
load root file and get the links + metadata from it 
the root file contains the links to further files that will become book chapters 
"""

link_include_regex = r'^.*'
link_exclude_regex = r'[\d]{4}\.htm|/volumes|/index\.htm'

print(f'Collecting data from root html file', colored(file_path, 'magenta'))

book_title, lang, chapters_input = extract_html_elements(file_path, [
    "string(//title/text())", # book_title
    "string(//meta[@http-equiv='Content-language']/@content)", # lang, 
    "//index/table//a" # chapters_input is a set of links from the root file, they will become ebook chapters
    ])

################################
"""
CHAPTERS
create a data structure for chapters:
- all links from main
- in the order that will be used in the book
- with chapter names for index, file paths and new file names in the epub
"""

print(f'Collecting the links that will become book chapters')

book_structure=[]
link_mapping_old_new = {}

# this input contains all data that goes inside this book
link_index = 1
for link in chapters_input:
  link_data = {}
  # href, title, first let, basename, name inside epub, resolved path - related to main
  try:
    href = link.attrib["href"]
  except:
    link_print=html.tostring(link, encoding='unicode')
    print(colored(link_print,'red'))
    print('no attribute href')
    continue
  basename = os.path.splitext(os.path.basename(href))[0]
  abs_path = get_absolute_path(file_path, href)

  title = link.text
  
  if title == None:
    title = basename

  link_data['title'] = title
  
  link_data['epub_section_filename'] = f'Chapter_{link_index:05d}_{basename}.xhtml'
  
  link_data['first_letter'] = re.sub(r'[^a-zA-Z0-9а-яА-Я]','',title)[0].upper()
  
  link_data['href'] = abs_path
  link_data['text'] = title
  
  link_mapping_old_new[link_data['href']] = link_data['epub_section_filename']
  link_index = link_index + 1

  book_structure.append(link_data)

print(f'Book structure collected. Number of elements:', colored(len(book_structure),'green'))

################################
"""
CHAPTERS
create a data structure for chapters:
- all links from main
- in the order that will be used in the book
- with chapter names for index, file paths and new file names in the epub
"""

book = epub.EpubBook()
spine = [] # the sequence of files
toc = [] # links and sections

book_title = re.sub(r"[\r\n]+","",book_title)
book.set_title(book_title)
book.add_author(author)
book.set_language(lang)

print(f'Looping over chapter links and attempting to extract their content')

# up to but not including p class r
# chapter_content_xpath = "//index/*[following-sibling::p[@class='r']]"

chapter_content_xpath = "//index"

for link in book_structure:
  title = link['title']
  file_path = link['href'] 

  print(colored(title, 'yellow'), colored(file_path, 'magenta'))
  
  epub_section_filename = link['epub_section_filename'] 
  first_letter = link['first_letter']
  
  content = extract_html_elements(file_path, chapter_content_xpath)
  
  content_string = ''.join([etree.tostring(element, encoding='unicode') for element in content])

  if content_string == "" :
    print (colored("No content returned from xpath ","red"))
    continue 
  
  c = epub.EpubHtml(title=title, file_name=epub_section_filename)
  c.content = content_string
  book.add_item(c)
  spine.append(c)

################################
"""
IMAGES
loop over every chapter
find IMG, A to image files
resolve path
if file exists - add
"""

print(f'Embedding images')

images_included_in_epub = {}
image_index = 1

for item in book.get_items(): 
  if item.content is None: 
    continue

  tree = html.fromstring(item.content)
  imgs = tree.xpath('//img')
  if len(imgs) == 0: 
    continue

  for img in imgs:
    src = img.get('src')
    if src in images_included_in_epub.keys(): 
      img.set('src', images_included_in_epub[src]) 
      del img.attrib['class']  
      continue
    if src.startswith('http'): continue

    # if img src already in images_included_in_epub then use its file path - otherwise upload

    original_img_name_ext = os.path.split(src)[1]
    img_uid = f'IMG{image_index:05d}'
    new_img_src = f'{img_uid}-{original_img_name_ext}'
    img_media_type = "image/jpeg" # todo: get this from img file
    
    # add image to epub
    img_path = resolve_file_url(src)
    with open(img_path, 'rb') as image_file:
      image_content = image_file.read()
      book.add_item(epub.EpubItem(uid=img_uid, file_name=new_img_src, media_type=img_media_type, content=image_content))
      
    images_included_in_epub[src] = new_img_src
    image_index = image_index + 1
    img.set('src', images_included_in_epub[src]) 
    del img.attrib['class']         

  modified_content = html.tostring(tree, encoding='unicode')

    # Update the contents of the HTML item with the modified content
  item.content = modified_content

print(f'Images embedded:', colored(len(images_included_in_epub.keys()),'green'))

########  #######   ######  
   ##    ##     ## ##    ## 
   ##    ##     ## ##       
   ##    ##     ## ##       
   ##    ##     ## ##       
   ##    ##     ## ##    ## 
   ##     #######   ######

# there will be a nested section structure
# now it's mostly flat 
section_name = book_title
sub_links = []
for item in book_structure:
  # the third parameter in epub.Link() is uid, and it is mandatory 
  # without this parameter, book.add_item(epub.EpubNcx()) will not work
  # without epub.EpubNcx(), some readers will not see the ToC
  link = epub.Link(item['epub_section_filename'], item['text'], item['epub_section_filename'])
  sub_links.append(link)
  
section = tuple([epub.Section(section_name,''), tuple(sub_links)])
toc.append(section)

 ######     ###    ##     ## ######## 
##    ##   ## ##   ##     ## ##       
##        ##   ##  ##     ## ##       
 ######  ##     ## ##     ## ######   
      ## #########  ##   ##  ##       
##    ## ##     ##   ## ##   ##       
 ######  ##     ##    ###    ########

print(f'Saving ebook')

# no more changing toc
book.toc = tuple(toc)

spine.insert(0,'nav')
book.spine = spine

book.add_item(epub.EpubNcx())
book.add_item(epub.EpubNav())

book_title = re.sub(r"[^\w ]+","",book_title)
file_name = f"{author} - {book_title}.epub"
script_dir = os.path.dirname(os.path.abspath(__file__))
epub_path = os.path.join(script_dir,file_name)

epub.write_epub(epub_path, book)

print(f'Ebook saved:', colored((epub_path),'green'))
