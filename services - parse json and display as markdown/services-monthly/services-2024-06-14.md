Source: https://stats.chatons.org/services.json

Capture date: 2024-06-14 12:48:51 CEST+0200

# Services
## 0bin.net
 * 🇫🇷 __0bin.net__ @ Axialys: https://paste.parinux.org
## baikal
 * 🇫🇷 __Baikal__ @ Bechamail.fr: https://baikal.bechamail.fr
 * 🇫🇷 __Baikal__ @ Bechamail.fr: https://baikal.bechamail.fr
## bigbluebutton
 * 🇫🇷 __bigbluebutton__ @ Devloprog: https://bbb.ethicit.fr
 * 🇩🇪 __BigBlueButton__ @ Hetzner: https://bbb.paquerette.eu/b/
## bitlbee
 * 🇩🇪 __BitlBee__ @ Hetzner: irc.immae.eu
## bookstack
 * 🇫🇷 __Bookstack__ @ Kimsufi: https://girofle.cloud
## bénévalibre
 * 🇩🇪 __Bénévalibre__ @ Hetzner: https://benevalibre.chapril.org/
## codenames green
 * 🇩🇪 __Codenames Green__ @ Hetzner: https://codenames.games.immae.eu/
## codimd
 * 🇫🇷 __CodiMD__ @ Tetaneutral: https://md.picasoft.net
## commento
 * 🇩🇪 __Commento__ @ Hetzner: https://commento.immae.eu
## converse
 * 🇩🇪 __Converse__ @ Hetzner: https://im.immae.fr/converse
## coturn
 * 🇩🇪 __coturn__ @ Hetzner: {service.website}
## cryptpad
 * 🇫🇷 __cryptpad__ @ Deuxfleurs: https://pad.deuxfleurs.fr
 * 🇫🇷 __CryptPad__ @ Exarius: https://exarius.org
 * 🇩🇪 __Cryptpad__ @ Hetzner: https://cryptpad.immae.eu/
 * 🇫🇷 __Cryptpad__ @ OVH: https://cryptpad.linux07.fr
 * 🇩🇪 __Cryptpad__ @ Hetzner: https://cryptpad.roflcopter.fr
## cyberchef
 * 🇩🇪 __CyberChef__ @ Hetzner: https://wtf.roflcopter.fr/cyberchef
## davical
 * 🇫🇷 __Davical__ @ Axialys: https://agendas.parinux.org
 * 🇫🇷 __Davical__ @ Axialys: https://contacts.parinux.org
 * 🇩🇪 __Davical__ @ Hetzner: https://dav.immae.eu
## diacamma
 * 🇫🇷 __Diacamma__ @ OVH: https://www.sleto.net
 * 🇫🇷 __Diacamma__ @ OVH: https://www.sleto.net
## diaspora*
 * 🇩🇪 __Diaspora*__ @ Hetzner: https://diaspora.immae.eu
## discourse
 * 🇫🇷 __Discourse__ @ Kimsufi: https://girofle.cloud
## docker
 * 🇫🇷 __docker__ @ Gandi: https://www.le-pic.org/hebergement
## dokuwiki
 * 🇫🇷 __DokuWiki__ @ Axialys: https://wiki.parinux.org/
 * 🇩🇪 __Dokuwiki__ @ Hetzner: https://tools.immae.eu/dokuwiki/
 * 🇩🇪 __Dokuwiki__ @ Hetzner: https://wiki.nomagic.uk
## dolomon
 * 🇩🇪 __Dolomon__ @ Hetzner: https://dol.roflcopter.fr
## dovecot
 * 🇩🇪 __Dovecot__ @ Hetzner: https://mail.immae.eu/
## drawio
 * 🇫🇷 __Drawio__ @ Ikoula: https://schema.le-filament.com/
 * 🇩🇪 __Drawio__ @ Hetzner: https://drawio.roflcopter.fr
 * 🇫🇷 __Drawio__ @ underworld.fr: https://diag.underworld.fr/
## ejabberd
 * 🇫🇷 __ejabberd__ @ Axialys: https://parinux.org/La-messagerie-instantanee
 * 🇫🇷 __ejabberd__ @ Libre en Communs: https://www.chalec.org/services/xmpp.html
 * 🇩🇪 __ejabberd__ @ Hetzner: https://xmpp.chapril.org/
 * 🇩🇪 __ejabberd__ @ Hetzner: https://im.immae.fr/
## element-web
 * 🇫🇷 __Element-Web__ @ underworld.fr: https://riot.underworld.fr/
## ethercalc
 * 🇩🇪 __Ethercalc__ @ HETZNER: https://accueil.framacalc.org/
 * 🇩🇪 __Ethercalc__ @ Hetzner: https://ethercalc.nomagic.uk
 * 🇫🇷 __Ethercalc__ @ OVH: https://calc.ti-nuage.fr/
## etherpad
 * 🇫🇷 __Etherpad__ @ Alsace Réseau Neutre: https://pad.sans-nuage.fr/
 * 🇫🇷 __Etherpad__ @ LinuxMario: https://pad.automario.eu
 * 🇫🇷 __Etherpad__ @ Axialys: https://bn.parinux.org
 * 🇫🇷 __Etherpad__ @ Libre en Communs: https://pad.chalec.org
 * 🇩🇪 __Etherpad__ @ Hetzner: https://pad.chapril.org/
 * 🇩🇪 __Etherpad__ @ Hetzner: https://pad.colibris-outilslibres.org
 * 🇫🇷 __Etherpad__ @ Devloprog: https://pad.devloprog.org
 * 🇩🇪 __Etherpad__ @ Hetzner: https://framapad.org/
 * 🇫🇷 __Etherpad__ @ Hadoly: https://pad.hadoly.fr
 * 🇩🇪 __Etherpad__ @ Hetzner: https://ether.immae.eu
 * 🇫🇷 __Etherpad__ @ Infini: https://pad.infini.fr/
 * 🇫🇷 __Etherpad__ @ Ikoula: https://pad.le-filament.com/
 * 🇫🇷 __Etherpad__ @ OVH: https://pad.linux07.fr
 * 🇫🇷 __Etherpad__ @ OVH: https://pad.nebulae.co/
 * 🇩🇪 __Etherpad__ @ Hetzner: https://pad.nomagic.uk
 * 🇫🇷 __Etherpad__ @ Ovh: https://pad.opendoor.fr
 * 🇫🇷 __Etherpad__ @ Tetaneutral: https://pad.picasoft.net
 * 🇩🇪 __Etherpad__ @ Hetzner: https://pad.roflcopter.fr
 * 🇫🇷 __Etherpad__ @ O2Switch: https://pad.siick.fr/
 * 🇫🇷 __Etherpad__ @ OVH: https://pad.sleto.net
 * 🇫🇷 __Etherpad__ @ OVH: https://apps.ti-nuage.fr/pad/
 * 🇫🇷 __Etherpad__ @ underworld.fr: https://pad.underworld.fr/
 * 🇫🇷 __Etherpad__ @ OVHCloud: https://pad.vulpecula.fr
## etherpad-lite
 * 🇩🇪 __Etherpad-lite__ @ Hetzner: https://pad.libre-service.eu/
## file2link
 * 🇩🇪 __file2link__ @ Retzo.net: https://dl.retzo.net
 * 🇫🇷 __file2link__ @ O2Switch: https://drop.siick.fr/
 * 🇫🇷 __file2link__ @ Zici: https://dl.zici.fr
## firefoxsend
 * 🇩🇪 __FirefoxSend__ @ Hetzner: https://drop.chapril.org/
 * 🇫🇷 __Firefox Send__ @ OVH: https://send.ti-nuage.fr/
## forgejo
 * 🇫🇷 __Forgejo__ @ Garbaye: https://git.garbaye.fr
## framadate
 * 🇫🇷 __Framadate__ @ Alsace Réseau Neutre: https://date.sans-nuage.fr/
 * 🇫🇷 __Framadate__ @ Axialys: https://rendez-vous.parinux.org/
 * 🇫🇷 __Framadate__ @ TetaNeutral: https://caracos.net/caracorss/
 * 🇩🇪 __Framadate__ @ Hetzner: https://date.chapril.org/
 * 🇩🇪 __Framadate__ @ Hetzner: https://date.colibris-outilslibres.org/
 * 🇩🇪 __Framadate__ @ Hetzner: https://framadate.org/
 * 🇫🇷 __Framadate__ @ Garbaye: https://sondage.garbaye.fr
 * 🇫🇷 __Framadate__ @ Infini: https://date.infini.fr/
 * 🇫🇷 __Framadate__ @ Ikoula: https://date.le-filament.com/
 * 🇫🇷 __Framadate__ @ OVH: https://sondage.linux07.fr
 * 🇩🇪 __Framadate__ @ Hetzner: https://framadate.nomagic.uk
 * 🇨🇭 __Framadate__ @ Infomniak: https://date.paquerette.eu/
 * 🇩🇪 __Framadate__ @ Retzo.net: https://rdv.retzo.net/
 * 🇩🇪 __Framadate__ @ Hetzner: https://wtf.roflcopter.fr/framadate
 * 🇫🇷 __Framadate__ @ OVH: https://date.sleto.net
 * 🇫🇷 __Framadate__ @ OVH: https://apps.ti-nuage.fr/date/
 * 🇫🇷 __Framadate__ @ Zici: https://rdv.zici.fr/
## freshrss
 * 🇫🇷 __FreshRSS__ @ La Becasse: https://rss.cheredeprince.net/
 * 🇫🇷 __FreshRSS__ @ Kimsufi: https://rss.siick.fr/
## funkwhale
 * 🇩🇪 __Funkwhale__ @ Hetzner: https://music.nomagic.uk
## galene
 * 🇩🇪 __Galene__ @ Hetzner: https://visio.immae.eu/
 * 🇫🇷 __Galene__ @ OVH: https://visioconf.sleto.net
## garage
 * 🇫🇷 __Garage__ @ Deuxfleurs: https://guide.deuxfleurs.fr/prise_en_main/web/
## gitea
 * 🇫🇷 __Gitea__ @ LinuxMario: https://git.automario.eu
 * 🇫🇷 __Gitea__ @ Libre en Communs: https://forge.chalec.org/
 * 🇩🇪 __Gitea__ @ Hetzner: https://forge.chapril.org/
 * 🇫🇷 __Gitea__ @ Exarius: https://git.exarius.org
 * 🇫🇷 __gitea__ @ Hadoly: https://git.hadoly.fr
 * 🇫🇷 __Gitea__ @ OVH: https://git.zoocoop.com/
 * 🇫🇷 __Gitea__ @ OVH: https://git.nebulae.co
## gitlab
 * 🇩🇪 __GitLab__ @ Hetzner: https://framagit.org/
 * 🇩🇪 __GitLab__ @ Hetzner: https://gitlab.nomagic.uk
 * 🇩🇪 __GitLab__ @ Hetzner: https://git.paquerette.eu/
 * 🇩🇪 __GitLab__ @ Hetzner: https://git.roflcopter.fr
 * 🇫🇷 __GitLab__ @ OVHCloud: https://git.vulpecula.fr
## gitolite
 * 🇩🇪 __Gitolite__ @ Hetzner: https://git.immae.eu
## glowing bear
 * 🇩🇪 __Glowing bear__ @ Hetzner: https://im.immae.fr/glowing-bear
## gogocarto
 * 🇫🇷 __Gogocarto__ @ Scaleway: https://gogocarto.fr
## gotify
 * 🇫🇷 __Gotify__ @ OVH: https://gotify.zoocoop.com/
## healthchecks
 * 🇩🇪 __Healthchecks__ @ Hetzner: https://cron.roflcopter.fr
## hedgedoc
 * 🇫🇷 __hedgedoc__ @ Axialys: https://codimd.parinux.org/
 * 🇩🇪 __Hedgedoc__ @ Hetzner: https://hedgedoc.nomagic.uk
 * 🇩🇪 __Hedgedoc__ @ Hetzner: https://md.roflcopter.fr
## icecast
 * 🇫🇷 __Icecast__ @ Infini: https://radios.infini.fr:8443
## ihatemoney
 * 🇫🇷 __ihatemoney__ @ Axialys: https://money.parinux.org
## ispconfig
 * 🇩🇪 __ISPconfig__ @ Retzo.net: https://retzo.net/services/hebergement/web/
 * 🇫🇷 __ISPconfig__ @ Zici: https://hebergement.zici.fr/
## jitsi meet
 * 🇨🇭 __Jitsi Meet__ @ Infomaniak: https://meet.altertek.org/
 * 🇩🇪 __Jitsi Meet__ @ Hetzner: https://visio.chapril.org/
 * 🇩🇪 __Jitsi Meet__ @ Hetzner: https://visio.colibris-outilslibres.org/
 * 🇫🇷 __jitsimeet__ @ Deuxfleurs: https://jitsi.deuxfleurs.fr
 * 🇫🇷 __jitsimeet__ @ Devloprog: https://video.devloprog.org
 * 🇩🇪 __jitsimeet__ @ Hetzner: https://framatalk.org/
 * 🇫🇷 __Jitsi Meet__ @ Garbaye: https://jitsi.garbaye.fr
 * 🇫🇷 __jitsimeet__ @ Hadoly: https://jitsi.hadoly.fr
 * 🇫🇷 __jitsimeet__ @ Ikoula: https://video.le-filament.com/
 * 🇩🇪 __Jitsi Meet__ @ Hetzner: https://visio.libre-service.eu/
 * 🇩🇪 __jitsimeet__ @ Hetzner: https://meet.nomagic.uk
 * 🇩🇪 __jitsimeet__ @ Hetzner: https://meet.roflcopter.fr
 * 🇫🇷 __Jitsi Meet__ @ OVH: https://visio.sleto.net
 * 🇫🇷 __Jitsimeet__ @ underworld.fr: https://conf.underworld.fr/
## joomla!
 * 🇫🇷 __Joomla!__ @ OVH: https://www.sleto.net
## kanboard
 * 🇫🇷 __kanboard__ @ Hadoly: https://projet.hadoly.fr
 * 🇩🇪 __Kanboard__ @ Hetzner: https://tools.immae.eu/kanboard
 * 🇩🇪 __Kanboard__ @ Hetzner: https://task.nomagic.uk
 * 🇫🇷 __Kanboard__ @ O2Switch: https://services.siick.fr/
## libreqr
 * 🇨🇭 __LibreQR__ @ Anancus: https://anancus.ynh.fr/qr/
 * 🇫🇷 __LibreQr__ @ Axialys: https://codeqr.parinux.org/
 * 🇩🇪 __LibreQR__ @ Hetzner: https://qrcode.chapril.org/
 * 🇩🇪 __LibreQR__ @ Hetzner: https://qrcode.libre-service.eu/
## libreto
 * 🇫🇷 __Libreto__ @ Alsace Réseau Neutre: https://libreto.sans-nuage.fr/
 * 🇫🇷 __Libreto__ @ OVH: https://libreto.linux07.fr
## limesurvey
 * 🇫🇷 __Limesurvey__ @ Axialys: https://sondages.parinux.org/admin
 * 🇩🇪 __Limesurvey__ @ Hetzner: https://survey.nomagic.uk
## loomio
 * 🇩🇪 __Loomio__ @ Hetzner: https://framavox.org/
## lstu
 * 🇨🇭 __LSTU__ @ Anancus: https://anancus.ynh.fr/lstu/
 * 🇫🇷 __Lstu__ @ Axialys: https://l.parinux.org/
 * 🇫🇷 __LSTU__ @ Bechamail.fr: https://bechamail.fr/
 * 🇫🇷 __Lstu__ @ Infini: https://link.infini.fr/
 * 🇩🇪 __LSTU__ @ Hetzner: https://ln.nomagic.uk
## lufi
 * 🇫🇷 __Lufi__ @ Alsace Réseau Neutre: https://drop.sans-nuage.fr/
 * 🇫🇷 __Lufi__ @ Axialys: https://transfert.parinux.org/
 * 🇫🇷 __Lufi__ @ Exarius: https://exarius.org
 * 🇫🇷 __Lufi__ @ Infini: https://drop.infini.fr/
 * 🇫🇷 __Lufi__ @ OVH: https://drop.zoocoop.com/
 * 🇩🇪 __Lufi__ @ Hetzner: https://lufi.nomagic.uk
 * 🇫🇷 __Lufi__ @ Tetaneutral: https://drop.picasoft.net
 * 🇫🇷 __Lufi__ @ underworld.fr: https://drop.underworld.fr/
## lutim
 * 🇫🇷 __Lutim__ @ Axialys: https://transfert-images.parinux.org/
 * 🇩🇪 __Lutim__ @ Hetzner: https://framapic.org/
 * 🇫🇷 __Lutim__ @ Infini: https://pic.infini.fr/
 * 🇩🇪 __Lutim__ @ Hetzner: https://pic.nomagic.uk
## mailcow
 * 🇫🇷 __Mailcow__ @ Kimsufi: https://girofle.cloud/boites-mails/
## mantisbt
 * 🇩🇪 __MantisBT__ @ Hetzner: https://git.immae.eu/mantisbt
## mastodon
 * 🇩🇪 __Mastodon__ @ Hetzner: https://pouet.chapril.org/
 * 🇩🇪 __Mastodon__ @ Hetzner: https://framapiaf.org/
 * 🇩🇪 __Mastodon__ @ Hetzner: https://mastodon.immae.eu/
 * 🇩🇪 __Mastodon__ @ Hetzner: https://mastodon.roflcopter.fr
 * 🇫🇷 __Mastodon__ @ underworld.fr: https://mastodon.underworld.fr/
## matrix
 * 🇫🇷 __Matrix__ @ Axialys: https://matrix.parinux.org/
## mattermost
 * 🇩🇪 __Mattermost__ @ Hetzner: https://tchat.colibris-outilslibres.org/
 * 🇩🇪 __Mattermost__ @ Hetzner: https://framateam.org/
 * 🇫🇷 __Mattermost__ @ Kimsufi: https://girofle.cloud
 * 🇫🇷 __Mattermost__ @ OVH: https://chat.linux07.fr
 * 🇨🇭 __Mattermost__ @ Infomniak: https://tchat.paquerette.eu/
 * 🇫🇷 __Mattermost__ @ Tetaneutral: https://team.picasoft.net
 * 🇫🇷 __Mattermost__ @ OVH: https://equipe.sleto.net/
## mediagoblin
 * 🇩🇪 __Mediagoblin__ @ Hetzner: https://mgoblin.immae.eu/
## metronome
 * 🇫🇷 __Metronome__ @ Alsace Réseau Neutre: https://sans-nuage.fr/
## minetest
 * 🇫🇷 __Minetest__ @ Libre en Communs: https://libreverse.chalec.org/
 * 🇩🇪 __Minetest__ @ Hetzner: https://minetest.libre-service.eu/
## minio
 * 🇫🇷 __Minio__ @ OVH: https://objects.zoocoop.com/
## mobilizon
 * 🇫🇷 __Mobilizon__ @ Alsace Réseau Neutre: https://mobilizon.sans-nuage.fr/
 * 🇫🇷 __Mobilizon__ @ Axialys: https://mobilizon.parinux.org/
 * 🇩🇪 __Mobilizon__ @ Hetzner: https://mobilizon.chapril.org/
 * 🇩🇪 __Mobilizon__ @ Hetzner: https://mobilizon.fr/
 * 🇫🇷 __Mobilizon__ @ OVH: https://keskonfai.fr/
 * 🇫🇷 __Mobilizon__ @ OVH: https://mobilizon.linux07.fr
 * 🇩🇪 __Mobilizon__ @ Hetzner: https://rendezvous.nomagic.uk
 * 🇫🇷 __Mobilizon__ @ Tetaneutral: https://mobilizon.picasoft.net
 * 🇩🇪 __Mobilizon__ @ Hetzner: https://mobilizon.roflcopter.fr
 * 🇫🇷 __Mobilizon__ @ underworld.fr: https://mobilizon.underworld.fr/
## movim
 * 🇫🇷 __Movim__ @ Axialys: https://movim.parinux.org/
## mumble
 * 🇫🇷 __Mumble__ @ Alsace Réseau Neutre: https://audio.sans-nuage.fr/
 * 🇫🇷 __Mumble__ @ Libre en Communs: https://audio.chalec.org/
 * 🇩🇪 __Mumble__ @ Hetzner: https://mumble.chapril.org/
 * 🇩🇪 __Mumble__ @ Hetzner: https://audio.libre-service.eu/
 * 🇫🇷 __Mumble__ @ Tetaneutral: https://voice.picasoft.net
 * 🇫🇷 __Mumble__ @ Tetaneutral: https://audio.le-pic.org
## my mind
 * 🇨🇭 __My Mind__ @ Anancus: https://anancus.ynh.fr/mind/
## mycryptochat
 * 🇫🇷 __MyCryptoChat__ @ O2Switch: https://chat.siick.fr/
## mypads
 * 🇫🇷 __Mypads__ @ Axialys: https://mes-bn.parinux.org
## nextcloud
 * 🇫🇷 __Nextcloud__ @ Alsace Réseau Neutre: https://sans-nuage.fr/file
 * 🇫🇷 __Nextcloud__ @ LinuxMario: https://cloud.automario.eu
 * 🇫🇷 __Nextcloud__ @ Axialys: https://nuage.parinux.org
 * 🇫🇷 __Nextcloud__ @ TetaNeutral: https://caracos.net/caracloud/
 * 🇩🇪 __Nextcloud__ @ Hetzner: https://valise.chapril.org/
 * 🇩🇪 __Nextcloud__ @ Hetzner: https://nc.clawd.fr
 * 🇫🇷 __Nextcloud__ @ Devloprog: https://nextcloud.devloprog.org
 * 🇫🇷 __Nextcloud__ @ Exarius: https://cloud.exarius.org
 * 🇩🇪 __Nextcloud__ @ Hetzner: https://framadrive.org/
 * 🇫🇷 __Nextcloud__ @ Hadoly/Grenode: https://nuage.hadoly.fr
 * 🇩🇪 __Nextcloud__ @ Hetzner: https://cloud.immae.eu/
 * 🇫🇷 __Nextcloud__ @ Infini: https://cloud.infini.fr/
 * 🇫🇷 __Nextcloud__ @ Kimsufi: https://girofle.cloud
 * 🇩🇪 __Nextcloud__ @ Liberta: https://cloud.liberta.vip
 * 🇫🇷 __Nextcloud__ @ OVH: https://nc.linux07.fr
 *  __Nextcloud__ @ {host.name}: https://monespace.defis.info
 * 🇫🇷 __Nextcloud__ @ OVH: https://cloud.nebulae.co
 * 🇫🇷 __Nextcloud__ @ Ovh: https://nuage.opendoor.fr
 * 🇩🇪 __Nextcloud__ @ Hetzner: https://cloud.paquerette.eu/
 * 🇫🇷 __Nextcloud__ @ Tetaneutral: https://cloud.le-pic.org
 * 🇩🇪 __Nextcloud__ @ Retzo.net: https://cloud.retzo.net
 * 🇫🇷 __Nextcloud__ @ Kimsufi: https://cloud.siick.fr/
 * 🇫🇷 __NextCloud__ @ OVH: https://cloud.sleto.net
 * 🇫🇷 __Nextcloud__ @ OVH: https://cloud.ti-nuage.fr
## nextcloud calendar
 * 🇩🇪 __Nextcloud Calendar__ @ Hetzner: https://framagenda.org/
 * 🇫🇷 __Nextcloud Calendar__ @ OVH: https://cloud.nebulae.co
 * 🇫🇷 __Nextcloud Calendar__ @ Zici: https://cloud.zici.fr/index.php/apps/calendar/
## nextcloud contact
 * 🇫🇷 __Nextcloud Contact__ @ OVH: https://cloud.nebulae.co
## nextcloud passman
 * 🇫🇷 __Nextcloud Passman__ @ Zici: https://www.zici.fr/Password.html
## ntfy
 * 🇫🇷 __ntfy__ @ Garbaye: https://notif.garbaye.fr
## nullboard
 * 🇨🇭 __Nullboard__ @ Anancus: https://anancus.ynh.fr/board
## onlyoffice
 * 🇫🇷 __OnlyOffice__ @ Kimsufi: https://girofle.cloud
 * 🇫🇷 __Onlyoffice__ @ Tetaneutral: https://cloud.le-pic.org
## opensondage
 * 🇨🇭 __OpenSondage__ @ Anancus: https://anancus.ynh.fr/date/
 * 🇫🇷 __OpenSondage__ @ TetaNeutral: https://caracos.net/caradate/
## openssh
 * 🇩🇪 __Openssh__ @ Hetzner: pub.immae.eu
## openvpn
 * 🇫🇷 __OpenVPN__ @ Alsace Réseau Neutre: https://arn-fai.net/internet-alternatif/tunnel-vpn
## owncloud
 * 🇫🇷 __Owncloud__ @ OVH: https://cloud.zoocoop.com/
## paheko
 * 🇫🇷 __Paheko__ @ Kimsufi: https://girofle.cloud
## paste
 * 🇩🇪 __Paste__ @ Hetzner: https://tools.immae.eu/paste/
## peertube
 * 🇫🇷 __Peertube__ @ Scaleway: https://video.altertek.org/
 * 🇫🇷 __Peertube__ @ Scaleway: https://video.colibris-outilslibres.org
 * 🇩🇪 __Peertube__ @ Hetzner: https://peertube.immae.eu/
 * 🇩🇪 __Peertube__ @ Liberta: https://video.liberta.vip
 * 🇩🇪 __Peertube__ @ Hetzner: https://peertube.nomagic.uk
 * 🇫🇷 __Peertube__ @ underworld.fr: https://peertube.underworld.fr/
## pixelfed
 * 🇩🇪 __PixelFed__ @ Hetzner: https://pxl.roflcopter.fr
## pleroma
 * 🇩🇪 __Pleroma__ @ Hetzner: https://social.nomagic.uk
## plume
 * 🇫🇷 __Plume__ @ Deuxfleurs: https://plume.deuxfleurs.fr/
## polr
 * 🇩🇪 __Polr__ @ Hetzner: https://colibris.link/
 * 🇫🇷 __Polr__ @ O2Switch: https://url.siick.fr/
## postfix
 * 🇫🇷 __Postfix__ @ OVH: https://mail.nebulae.co
## privatebin
 * 🇨🇭 __PrivateBin__ @ Anancus: https://anancus.ynh.fr/zerobin/
 * 🇫🇷 __PrivateBin__ @ Libre en Communs: https://ctrlv.chalec.org
 * 🇩🇪 __PrivateBin__ @ Hetzner: https://paste.chapril.org/
 * 🇩🇪 __PrivateBin__ @ Hetzner: https://framabin.org/
 * 🇫🇷 __PrivateBin__ @ Garbaye: https://bin.garbaye.fr
 * 🇫🇷 __PrivateBin__ @ Hadoly: https://postit.hadoly.fr
 * 🇫🇷 __PrivateBin__ @ Infini: https://bin.infini.fr/
 * 🇫🇷 __PrivateBin__ @ Ikoula: https://cles.le-filament.com/
 * 🇩🇪 __PrivateBin__ @ Hetzner: https://paste.libre-service.eu/
 * 🇩🇪 __PrivateBin__ @ Hetzner: https://paste.nomagic.uk
 * 🇫🇷 __PrivateBin__ @ Ovh: https://private.opendoor.fr
 * 🇫🇷 __PrivateBin__ @ Tetaneutral: https://paste.picasoft.net/
 * 🇩🇪 __PrivateBin__ @ Hetzner: https://wtf.roflcopter.fr/paste
 * 🇫🇷 __PrivateBin__ @ O2Switch: https://bin.siick.fr/
 * 🇫🇷 __PrivateBin__ @ underworld.fr: https://paste.underworld.fr/
 * 🇫🇷 __PrivateBin__ @ Zici: https://pastebin.zici.fr/
## proftpd
 * 🇩🇪 __ProFTPD__ @ Hetzner: ftp.immae.eu
## prosody
 * 🇫🇷 __Prosody__ @ https://www.ikoula.com/fr: https://jabberfr.org
 * 🇫🇷 __Prosody__ @ https://www.ikoula.com/fr: https://chat.jabberfr.org
## rainloop
 * 🇫🇷 __rainloop__ @ Hadoly: https://webmail.hadoly.fr
 * 🇫🇷 __rainloop__ @ Ovh: https://courrier.opendoor.fr
## rocketchat
 * 🇫🇷 __RocketChat__ @ Axialys: https://rocket.parinux.org
## roundcube
 * 🇫🇷 __Roundcube__ @ Alsace Réseau Neutre: https://sans-nuage.fr/webmail/
 * 🇫🇷 __RoundCube__ @ LinuxMario: https://mail.automario.eu
 * 🇫🇷 __Roundcube__ @ Bechamail.fr: https://mail.bechamail.fr
 * 🇫🇷 __roundcube__ @ TetaNeutral: https://caracos.net/caramail/
## rss bridge
 * 🇫🇷 __RSS Bridge__ @ Alsace Réseau Neutre: https://rss-bridge.sans-nuage.fr/
 * 🇩🇪 __RSS Bridge__ @ Hetzner: https://wtf.roflcopter.fr/rss-bridge
## scrumblr
 * 🇫🇷 __Scrumblr__ @ Axialys: https://notes.parinux.org/
 * 🇩🇪 __Scrumblr__ @ Hetzner: https://postit.colibris-outilslibres.org/
 * 🇩🇪 __Scrumblr__ @ Hetzner: https://framemo.org/
## seafile
 * 🇫🇷 __Seafile__ @ Garbaye: https://seafile.garbaye.fr
 * 🇩🇪 __Seafile__ @ Hetzner: https://seafile.nomagic.uk
## searx
 * 🇫🇷 __SearX__ @ Exarius: https://exarius.org
 * 🇫🇷 __Searx__ @ underworld.fr: https://searx.underworld.fr/
## shaarli
 * 🇩🇪 __Shaarli__ @ Hetzner: https://tools.immae.eu/Shaarli/
## sogo
 * 🇩🇪 __SOGo__ @ Hetzner: https://webmail.nomagic.uk
## strut
 * 🇫🇷 __Strut__ @ Alsace Réseau Neutre: https://slide.sans-nuage.fr/
## sympa
 * 🇩🇪 __Sympa__ @ Hetzner: https://framalistes.org/
 * 🇩🇪 __Sympa__ @ Hetzner: https://mail.immae.eu/sympa
 * 🇫🇷 __Sympa__ @ Infini: https://listes.infini.fr/
 * 🇫🇷 __Sympa__ @ OVH: https://list.nebulae.co
 * 🇩🇪 __Sympa__ @ Hetzner: https://sympa.nomagic.uk
 * 🇫🇷 __Sympa__ @ Gandi: https://sympa.le-pic.org
## synapse
 * 🇫🇷 __Synapse__ @ Alsace Réseau Neutre: https://chat.sans-nuage.fr/
 * 🇫🇷 __Synapse__ @ Deuxfleurs: https://riot.deuxfleurs.fr
 * 🇫🇷 __Synapse__ @ Exarius: https://exarius.org/_matrix/static/
 * 🇫🇷 __Synapse__ @ Garbaye: https://matrix.garbaye.fr
 * 🇫🇷 __Synapse__ @ Hadoly: https://conversation.hadoly.fr/
 * 🇩🇪 __Synapse__ @ Hetzner: https://chat.nomagic.uk
 * 🇫🇷 __Synapse__ @ underworld.fr: https://matrix.underworld.fr/
## taskwarrior
 * 🇩🇪 __Taskwarrior__ @ Hetzner: https://task.immae.eu/
## terraforming mars
 * 🇩🇪 __Terraforming Mars__ @ Hetzner: https://terraforming-mars.games.immae.eu/
## tiny tiny rss
 * 🇫🇷 __Tiny Tiny Rss__ @ Axialys: https://rss.parinux.org/
 * 🇩🇪 __Tiny Tiny RSS__ @ Hetzner: https://tools.immae.eu/ttrss/
 * 🇫🇷 __TinyTinyRSS__ @ OVH: https://rss.nebulae.co
 * 🇩🇪 __Tiny tiny RSS__ @ Hetzner: https://ttrss.nomagic.uk
## umap
 * 🇩🇪 __umap__ @ Hetzner: https://framacarte.org/
 * 🇫🇷 __uMap__ @ Infini: https://map.infini.fr/
## vaultwarden
 * 🇫🇷 __Vaultwarden__ @ LinuxMario: https://passwords.automario.eu
 * 🇫🇷 __Vaultwarden__ @ OVH: https://bitwarden.nebulae.co
 * 🇩🇪 __vaultwarden__ @ Hetzner: https://vault.nomagic.uk
 * 🇩🇪 __Vaultwarden__ @ Hetzner: https://wtf.roflcopter.fr/vault
## vger
 * 🇫🇷 __Vger__ @ LinuxMario: https://unbon.cafe
## wallabag
 * 🇫🇷 __Wallabag__ @ La Bécasse: https://wallabag.cheredeprince.net/
 * 🇩🇪 __Wallabag__ @ Hetzner: https://tools.immae.eu/wallabag/
 * 🇫🇷 __Wallabag__ @ OVH: https://bag.zoocoop.com/
## wekan
 * 🇫🇷 __Wekan__ @ OVH: https://kan.nebulae.co
 * 🇫🇷 __Wekan__ @ Tetaneutral: https://kanban.picasoft.net
## whiteboard
 * 🇫🇷 __whiteboard__ @ Tetaneutral: https://board.picasoft.net/
## wisemapping
 * 🇩🇪 __WiseMapping__ @ Hetzner: https://framindmap.org/
## wordpress
 * 🇩🇪 __WordPress__ @ Hetzner: https://nomagic.uk/fr/services/#Blogging
 * 🇨🇭 __WordPress__ @ Infomaniak: https://paquerette.eu/
 * 🇫🇷 __Wordpress__ @ OVH: https://www.sleto.net
## yakforms
 * 🇫🇷 __Yakforms__ @ Alsace Réseau Neutre: https://sondage.sans-nuage.fr/
 * 🇩🇪 __Yakforms__ @ Hetzner: https://framaforms.org/
## yeswiki
 * 🇩🇪 __YesWiki__ @ Hetzner: https://colibris-wiki.org
 * 🇨🇭 __YesWiki__ @ Infomaniak: https://yeswiki.paquerette.eu/?PagePrincipale
