import datetime
import os, json

from fuzzywuzzy import fuzz, process
import json
from pygments import highlight, lexers, formatters
from collections import Counter
from pygments.styles import get_style_by_name
from typing import Any
import itertools

import pytz
import requests


def json_profile_elements(data, element_names):
    try:
        # Load json if input is a string
        if isinstance(data, str):
            data = json.loads(data)

        # Initialize dictionary to store results
        profiling_results = {}

        # Loop through element names
        for element_name in element_names:
            distinct_values = []  # list to store distinct values
            empty_values = 0  # counter for empty/null/none values
            total_values = 0  # counter for total values

            # Loop through data to count values
            for item in data:
                if element_name in item:
                    total_values += 1
                    if not item[element_name]:  # check empty/null/none values
                        empty_values += 1
                    else:
                        distinct_values.append(item[element_name])
                    
            # Count the occurrences of each distinct value
            value_counts = Counter(distinct_values)
        
            # Sort the distinct values by count in descending order
            sorted_distinct_values = sorted(value_counts.items(), key=lambda x: x[1], reverse=True)

            # Store results in dictionary
            profiling_results[element_name] = {
                "distinct_values_count": len(distinct_values),
                "empty_values_count": empty_values,
                "total_values_count": total_values,
                "distinct_values": sorted_distinct_values
            }

        return profiling_results

    except Exception as e:
        print(f"Error in json_element_profiler(): {e}")
        return {"Error": "json_element_profiler()"}

def json_pretty_print(data):
    try:
        # Load the json data
        if isinstance(data, str):
            json_obj = json.loads(data)
        else:
            json_obj = data
        
        # Format the json with 4 spaces of indentation
        formatted_json_str = json.dumps(json_obj, indent=4)
        
        # Highlight the json with colors
        colorful_json_str = highlight(formatted_json_str, lexers.JsonLexer(), formatters.TerminalFormatter())
        
        # Print the colorful json
        print(colorful_json_str, )
    except (TypeError, ValueError, ClassNotFound):
        print("Error: Invalid JSON data")

def string_normalize(value):
    try:
        # Check if value is NoneType or not
        if value is None:
            return ""
        
        # Convert to lowercase and strip whitespace
        value = str(value).lower().strip()
        return value
    except Exception as e:
        print(f"[normalize_string] Error: {e}")
        return ""


def json_normalize_properties(data, properties_to_normalize):
    fuzzy_match_ratio = 90
    for obj in data:
        # Loop through the properties
        for prop in properties_to_normalize:
            # Get the original value
            org_value = obj.get(prop)
            # Normalize the value
            norm_value = string_normalize(org_value)
            # Fuzzy match the normalized value with all previous values
            match_value = None
            for prev_obj in data:
                prev_norm_value = string_normalize(prev_obj.get(prop))
                ratio = fuzz.ratio(norm_value, prev_norm_value)
                if ratio >= fuzzy_match_ratio:
                    match_value = prev_obj.get(prop + ".normalized")
                    break
            # If there is a match, use the match value
            if match_value is not None:
                obj[prop + ".normalized"] = match_value
            # Otherwise, use the normalized value as the new value
            else:
                obj[prop + ".normalized"] = norm_value
    return data

def get_country_info(iso2, refresh_country_json=False):
    cache_path = os.path.join(os.path.dirname(__file__), 'restcountries.json')
    if refresh_country_json or not os.path.exists(cache_path):
        url = 'https://restcountries.com/v2/all'
        response = requests.get(url)
        if response.status_code != 200:
            return None
        with open(cache_path, 'w', encoding='utf-8') as f:
            f.write(response.text)
        json_data = response.json()
    else:
        with open(cache_path, 'r', encoding='utf-8') as f:
            json_data = json.load(f)
    country = next((c for c in json_data if c['alpha2Code'] == iso2), None)
    if country is None:
        return None
    country_info = {}
    country_info['country.name'] = country['name']
    country_info['country.continent'] = country['region']
    country_info['country.subregion'] = country['subregion']

    return country_info

def get_country_flag(iso2):
    if iso2 is None or iso2.strip() == '':
        return ''
    offset = 127397
    code_points = [ord(char) + offset for char in iso2.upper()]
    return chr(code_points[0]) + chr(code_points[1])

def group_by_props(result, props):
    # Base case: if there are no grouping properties, return the original list
    if len(props) == 0:
        #print("==================== Leaf ====================")
        return result
    
    # sort the list by property, otherwise groupby will not work
    try:
        result = sorted(result, key=lambda x: x.get(props[0], ''))
    except:
        print(f"Cannot sort on property: '{props[0]}'")

    #result_dict = {k: result_dict[k] for k in sorted(result_dict)}

    grouped = itertools.groupby(result, key=lambda obj: obj.get(props[0], ''))
    
    # Recursively group each subgroup by the remaining grouping properties
    nested = []
    for key, group in grouped:
        subresult = list(group)
        subnested = group_by_props(subresult, props[1:])
        nested.append({
            'property.name': props[0],
            'property.value': key if key != '' else '',
            'count': len(subresult),
            'children': subnested
        })
    # Sort the nested structures by the grouping property value
    nested.sort(key=lambda obj: obj['property.value'] if obj['property.value'] is not None else '')
    return nested

def create_markdown_document(data, level=1):
    if level == 1:
        markdown = '# '
    elif level == 2:
        markdown = '## '
    elif level == 3:
        markdown = '### '
    else:
        markdown = '#' * level + ' '
    markdown += str(data.get('property.value', '')) + '\n'
    
    # leaf level
    if not data.get('children'):
        string_with_placeholders = ' * {country.flag} __{software.name}__ @ {host.name}: {service.website}\n'
        markdown = replace_placeholders(data, string_with_placeholders)
        #markdown = "```json\n" + json.dumps(data, indent=2) + '\n```\n\n'
    else:
        for child in data['children']:
            markdown += create_markdown_document(child, level=level+1)
    return markdown

def replace_placeholders(dict_obj, string_with_placeholders):
    for key, value in dict_obj.items():
        string_with_placeholders = string_with_placeholders.encode('utf-8').decode('utf-8')
        string_with_placeholders = string_with_placeholders.replace("{" + key + "}", str(value))
    return string_with_placeholders

 ######  ########    ###    ########  ######## 
##    ##    ##      ## ##   ##     ##    ##    
##          ##     ##   ##  ##     ##    ##    
 ######     ##    ##     ## ########     ##    
      ##    ##    ######### ##   ##      ##    
##    ##    ##    ##     ## ##    ##     ##    
 ######     ##    ##     ## ##     ##    ##

script_dir = os.path.dirname(os.path.realpath(__file__))

url = 'https://stats.chatons.org/services.json'

response = requests.get(url)
content = response.content.decode('utf-8')

j = json.loads(content)

# make a list
list_identifier = 'services'
j_list = j[list_identifier]

# select required properties - or all
required_property_names = [
    'software.name',
    'software.website',
    'software.source.url',
    'service.website',
    'host.provider.type',
    'host.name',
    'host.server.type',
    'host.country.code'
]

j_list_selected_properties = []
if len(required_property_names) > 0:
    for list_item in j_list:
        new_list_item = {}
        for original_property_name in list_item:
            if original_property_name.lower() in required_property_names:
                new_list_item[original_property_name] = list_item[original_property_name]
        j_list_selected_properties.append(new_list_item)
else:
    j_list_selected_properties = j_list


# normalize values in specified properties
properties_to_normalize = [
    'software.name'
]

j_list_normalized = json_normalize_properties(j_list_selected_properties, properties_to_normalize)

#json_pretty_print(j_list_normalized)

# profile
properties_to_profile = ["software.name.normalized"]

profiling_result = json_profile_elements(j_list_normalized, properties_to_profile)

#json_pretty_print(profiling_result)

# add country text and flag
for item in j_list_normalized:
    country_code = item.get('host.country.code')
    
    country_info = get_country_info(country_code)
    
    flag = {'country.flag':get_country_flag(country_code)}
    
    if(country_info):
        item.update(country_info)
    
    if(flag):
        item.update(flag)

# group
properties_to_group_by = ["software.name.normalized"]

grouping_result = group_by_props(j_list_normalized, properties_to_group_by)

#json_pretty_print(grouping_result)

# ready to output
markdown = create_markdown_document({'property.value': 'Services', 'children': grouping_result})

local_tz = pytz.timezone('CET')
current_datetime = datetime.datetime.now(tz=local_tz)

current_datetime_string = current_datetime.strftime("%Y-%m-%d %H:%M:%S %Z%z")
current_date_string = current_datetime.strftime("%Y-%m-%d")

markdown = f'Source: {url}\n\nCapture date: {current_datetime_string}\n\n' + markdown

subdir = 'services-monthly'

markdown_outout_filename = f'services-{current_date_string}.md'

filename = os.path.join(script_dir, subdir, markdown_outout_filename)
with open(filename, 'w', encoding='utf-8') as f:
    f.write(markdown)

print(filename)