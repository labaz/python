import re
import json
import os
import datetime, time
import sqlite3
from dateutil import parser
import requests
from itertools import islice
from helper_functions import *

current_file_dir = os.path.dirname(os.path.abspath(__file__))
save_subdir = 'openai_job_descriptions'
save_dir_path = os.path.join(current_file_dir, save_subdir)

file_path_regex = r'job_id_(.*)\.json'

# create sqlite table (job_description pk, job_group)
drop_table_if_exists = True

table_name = 'job_qualifications'
fields = ['job_id', 'qualification']
create_sqlite_table(table_name=table_name, fields=fields, drop_table_if_exists=drop_table_if_exists)

table_name = 'job_tasks'
fields = ['job_id', 'task']
create_sqlite_table(table_name=table_name, fields=fields, drop_table_if_exists=drop_table_if_exists)

# read all files and run insert_record_into_table
matching_files = [os.path.join(save_dir_path, filename) for filename in os.listdir(save_dir_path) if re.match(file_path_regex, filename)]

print_duration('overall')

db_file_name = 'jobs.ch.sqlite'

# how many files to insert at once
file_batch_size = 1500

file_counter = 0
elements = {}
for file_path in matching_files:
    file_counter += 1
    # print_time()
    # print(file_path)
    file_name = os.path.basename(file_path)

    job_id = re.sub(r'job_id_(.*)\.json', r'\1', file_name)

    #print(file_counter)
    #print(f'job_id: {job_id}')

    with open(file_path, 'r') as f:
        j = json.load(f)

    table_name = 'job_qualifications'
    array_key = 'qualifications'

    if not array_key in elements:
        print(f'adding key: {array_key}')
        elements[array_key] = []

    elements[array_key].extend([{'job_id': job_id, 'qualification': element} for element in j[array_key]])
    if (file_counter % file_batch_size == 0 or file_counter == len(matching_files) and len(elements[array_key])) > 0:
        print(f'inserting {len(elements[array_key])} records into [{table_name}]')
        insert_multiple_records_into_table(elements[array_key], table_name, db_file_name)
        elements[array_key] = []

    table_name = 'job_tasks'
    array_key = 'tasks'
    
    if not array_key in elements:
        print(f'adding key: {array_key}')
        elements[array_key] = []
    
    elements[array_key].extend([{'job_id': job_id, 'task': element} for element in j[array_key]])

    if (file_counter % file_batch_size == 0 or file_counter == len(matching_files) and len(elements[array_key])) > 0:
        print(f'inserting {len(elements[array_key])} records into [{table_name}]')
        insert_multiple_records_into_table(elements[array_key], table_name, db_file_name)
        elements[array_key] = []
        


print_duration('overall')
