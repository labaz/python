import re
import json
import os
import datetime, time
import sqlite3
from dateutil import parser
import requests

from helper_functions import create_sqlite_table, get_json_path, insert_record_into_table, safe_filename, dump_url, get_sqlite_sql, db_file_name

current_file_dir = os.path.dirname(os.path.abspath(__file__))
subdir = ''
save_dir_path = os.path.join(current_file_dir, subdir)

field_names = """
job_id
job_source_type
title
company_name
template_text
template_profession
template_lead_text
template_contact_address
synonym
street
place
offer_id
headhunter_application_allowed
application_email
application_method
contact_person.firstName
contact_person.lastName
contact_person.gender
contact_person.phone
contact_person.role
"""

fields = [line for line in field_names.splitlines() if line.strip()]

table_name = 'job_descriptions'
pk_field_name = 'job_id'
name_datatype_mapping_regex = None

create_sqlite_table(table_name=table_name, pk_field_name=pk_field_name, fields=fields, name_datatype_mapping_regex=name_datatype_mapping_regex, drop_table_if_exists=False)

file_name_regex = r'^http.*\bjob\b.*\.json$'

matching_files = [filename for filename in os.listdir(save_dir_path) if re.match(file_name_regex, filename)]

#matching_files = matching_files[0:10]

name_function_mapping_regex = None

record_id = 1
for file_name in matching_files:
    print(f'{record_id}: {file_name}')
    with open(file_name, 'r') as f:
        record = json.load(f)
    insert_record_into_table(table_name, record, fields, name_function_mapping_regex)
    record_id += 1
