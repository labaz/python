import re
import json
import os
import datetime, time
import sqlite3
from dateutil import parser
import requests

from helper_functions import create_sqlite_table, get_json_path, insert_record_into_table, safe_filename, dump_url, get_sqlite_sql, db_file_name

"""
curl "https://www.jobs.ch/api/v1/public/search/job/628c00f6-02fa-4d88-8b37-b6bd86ab7995"| gron
"""

current_file_dir = os.path.dirname(os.path.abspath(__file__))
subdir = ''
save_dir_path = os.path.join(current_file_dir, subdir)

query = 'select job_id from records'
column_names, rows = get_sqlite_sql(db_file_name, query)

wait_every = 1000
wait_seconds = 10
count = 1
for row in rows:
    job_id = row[0]
    url = f'https://www.jobs.ch/api/v1/public/search/job/{job_id}'
    if count % wait_every == 0:
        print(f'waiting {wait_seconds}')
        time.sleep(wait_seconds)
    print(f'{count}: {url}')
    dump_url(url)
    count += 1

