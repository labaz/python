import re
import json
import os
import datetime, time
import sqlite3
from dateutil import parser

"""
cat "https：⧸⧸www.jobs.ch⧸api⧸v1⧸public⧸search？category-ids%5B%5D=106&page=100&rows=20.json" | jq '.documents[18]' | gron
"""

current_file_dir = os.path.dirname(os.path.abspath(__file__))
subdir = ''
save_dir_path = os.path.join(current_file_dir, subdir)

db_file_name = 'jobs.ch.sqlite'

def create_sqlite_table(table_name, name_datatype_mapping_regex, fields, pk_field_name, drop_table_if_exists=False):
    # Create a SQLite database connection
    conn = sqlite3.connect(db_file_name)
    cursor = conn.cursor()

    # Initialize the CREATE TABLE query
    create_table_query = f"CREATE TABLE IF NOT EXISTS `{table_name}` ("
    drop_table_query = f"DROP TABLE IF EXISTS `{table_name}`"

    # Iterate through the fields
    for field in fields:
        datatype = 'TEXT'  # Default datatype is TEXT

        # Check if the field name matches any regex pattern in the mapping
        for pattern, mapped_datatype in name_datatype_mapping_regex.items():
            if re.search(pattern, field):
                datatype = mapped_datatype
                break

        # Add the field and datatype to the query, enclosed in backticks
        create_table_query += f"`{field}` {datatype}, "

    # Add the primary key field, enclosed in backticks
    create_table_query += f"PRIMARY KEY (`{pk_field_name}`)"
    create_table_query += ")"
    
#    print(create_table_query)

    # Execute the CREATE TABLE query
    if drop_table_if_exists:
        cursor.execute(drop_table_query)
    cursor.execute(create_table_query)

    # Commit the transaction and close the connection
    conn.commit()
    conn.close()

def get_json_path(json, path):
    keys = path.split('.')  # Split the path string into keys
    current = json  # Start with the original JSON object

    for key in keys:
        if "[" in key and "]" in key:
            # Handle case when key contains an index, like "regions[0]"
            key, index = key.split('[')
            index = int(index.strip(']'))
            try:
                current = current[key][index]
            except (KeyError, IndexError):
                return None  # Return None if the key or index is invalid
        else:
            try:
                current = current[key]
            except KeyError:
                return None  # Return None if the key is not found in the JSON

    return current

def insert_record_into_table(table_name, data_dict, array_of_fields, name_function_mapping_regex=None, pk_field_name="id"):
    # Create a SQLite database connection
    conn = sqlite3.connect(db_file_name)
    cursor = conn.cursor()

    # Build the INSERT OR REPLACE INTO query
    insert_query = f"INSERT OR REPLACE INTO `{table_name}` ("

    # Extract values from the data_dict for the specified fields
    values = []
    for field in array_of_fields:
        value = get_json_path(data_dict, field)  # Use your get_json_path function
        if name_function_mapping_regex:
            for pattern, conversion_function in name_function_mapping_regex.items():
                if re.search(pattern, field) and value != None:
                    value = conversion_function(value)
        values.append(value)

    # Build the column names part of the query
    insert_query += ', '.join([f'`{field}`' for field in array_of_fields])

    # Build the VALUES part of the query with placeholders
    value_placeholders = ', '.join(['?'] * len(array_of_fields))

    # Complete the query
    insert_query += f") VALUES ({value_placeholders})"

#    print(insert_query)

    # Execute the INSERT OR REPLACE INTO query with values
    cursor.execute(insert_query, values)

    # Commit the transaction and close the connection
    conn.commit()
    conn.close()

field_names = """ 
job_id
title
_links.detail_de.href
_links.detail_en.href
categories[0].path
company_name
company_segmentation
regions[0].path
employment_type_ids[0]
initial_publication_date
publication_date
age
is_active
is_paid
place
preview
"""

fields = [line for line in field_names.splitlines() if line.strip()]

table_name = 'records'
pk_field_name = 'job_id'
name_datatype_mapping_regex = {
    r'date$': 'DATETIME',
    r'age': 'INT',
    r'^is_': 'INT',
    r'employment_type_ids': 'INT'
}

create_sqlite_table(table_name=table_name, pk_field_name=pk_field_name, fields=fields, name_datatype_mapping_regex=name_datatype_mapping_regex, drop_table_if_exists=False)

# simple tables (categories etc.)
categories_file_name = 'categories.json'
with open(categories_file_name, 'r') as f:
    categories = json.load(f)

simple_tables = ['categories', 'employment_types', 'industries', 'regions']
for table_name in simple_tables:
    create_sqlite_table(table_name, {r'id':'INT'}, ['id', 'description'], 'id', True)
    key_regex = fr'^{table_name}_track\.(.*)$'
    for key in categories:
        if re.search(key_regex, key):
            id = re.sub(key_regex, r'\1', key)
            description = categories[key]
            dict_to_insert = {'id': id, 'description': description}
            insert_record_into_table(table_name, dict_to_insert, dict_to_insert.keys(), None)


## records
table_name = 'records'
key_array_element = 'documents'
file_name_regex = r'^http.*category.*json$' # must be full string: ^.*$

# this is uncommented to skip
file_name_regex = r'---------'

all_files = os.listdir(save_dir_path)
matching_files = [filename for filename in os.listdir(save_dir_path) if re.match(file_name_regex, filename)]

for file_name in matching_files:
    print(file_name)
    with open(file_name, 'r') as f:
        j = json.load(f)
    if len(j[key_array_element]) == 0:
        continue
    record_id = 0
    for record in j[key_array_element]:
        name_function_mapping_regex = {
            r'date$': parser.isoparse,
        }
        print(record_id)
        insert_record_into_table(table_name, record, fields, name_function_mapping_regex)
        record_id += 1
