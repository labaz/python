import re
import json
import os
import datetime, time
import sqlite3
from dateutil import parser
import requests
from itertools import islice
from helper_functions import *

model = 'gpt-3.5-turbo-1106'
db_file_name = 'jobs.ch.sqlite'
subdir = 'openai_job_descriptions'

def openai_process(role_system_prompt, role_user_prompt):

    messages = [
        {"role": "system", "content": role_system_prompt},
        {"role": "user", "content": role_user_prompt},
    ]

    completion = openai_complete_chat(model, messages)

    return completion


current_file_dir = os.path.dirname(os.path.abspath(__file__))
save_dir_path = os.path.join(current_file_dir, subdir)
os.makedirs(save_dir_path) if not os.path.exists(save_dir_path) else None

db_file_name = os.path.join(current_file_dir, db_file_name)

print(f"getting job descriptions from sqlite: {db_file_name}")

print_duration('sqlite query')
sql = """
select distinct
	job_descriptions.job_id,
	job_groups_aggregated.job_group,
	job_descriptions.template_text
from job_descriptions
left join job_groups_aggregated on job_descriptions.title = job_groups_aggregated.job_title
left join job_tasks on job_tasks.job_id = job_descriptions.job_id
left join job_qualifications  on job_qualifications.job_id = job_descriptions.job_id
where 1=1
	and job_groups_aggregated.job_group like '%market%'
	and job_tasks.job_id is null
	and job_qualifications.job_id is null
order by job_descriptions.job_id
"""
header, rows = get_sqlite_sql(db_file_name, sql)

print(sql)

print_duration('sqlite query')

print('starting openai queries')
# create a list of openai inputs
role_system_prompt = """
You are a text summarization engine specializing in summarizing job descriptions.
You receive job descriptions in different languages. They may be in html format.
Out of those job descriptions, you extract two types of information: required qualifications and expected tasks.
You extract that information and present it in English language.
Qualifications should be in noun form, e.g.: 5 years of experience, German certificate C, Driver's license, ...
Tasks should be in infinitive verb form, e.g.: Process payroll, Train employees, Execute SAP transactions, ...
You output the results in JSON format. The JSON that you produce has 2 keys: 'qualifications' and ' tasks'. The contents under these keys will be arrays.
The structure of the JSON is as follows:
{'qualifications': ['qualification 1', 'qualification 2', ...], 'tasks': ['task 1', 'task 2', ...]}
"""

openai_inputs = [
]

batch_number = 1
for row in rows:
    role_user_prompt = row[2]
    job_id = row[0]
    save_file_path = os.path.join(save_dir_path, f'job_id_{job_id}.json')

    result = {
        "save_file_path": save_file_path,
        "role_system_prompt": role_system_prompt,
        "role_user_prompt": role_user_prompt
    }
    openai_inputs.append(result)
    batch_number += 1

def openai_wrapper(input):

    save_file_path = input["save_file_path"]
    role_user_prompt = input["role_user_prompt"]
    role_system_prompt = input["role_system_prompt"]

    result = openai_process(role_system_prompt, role_user_prompt)
    
    with open(save_file_path, 'w', encoding='utf-8') as f:
        json.dump(result, f, indent=2)

    print(save_file_path)
    return

# Process the data in batches
parallel_max_workers = 24

print_time()
print_duration('openai queries')
print(f"parallel_max_workers: {parallel_max_workers}")
print(f"len(openai_inputs): {len(openai_inputs)}")

process_in_parallel(
    items=openai_inputs,
    function=openai_wrapper,
    batch_size=parallel_max_workers
)

print_duration('openai_queries')
