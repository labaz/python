import requests
import json
import os
import datetime, time
import io
import sys
import argparse

current_file_dir = os.path.dirname(os.path.abspath(__file__))
subdir = ''
save_dir_path = os.path.join(current_file_dir, subdir)

def safe_filename(filename):
    # replacing disallowed chars in filenames a la yt-dlp
    replacements = {
                '<': '＜',  # Fullwidth less-than sign
                '>': '＞',  # Fullwidth greater-than sign
                ':': '：',  # Fullwidth colon
                '"': '＂',  # Fullwidth quotation mark
                '/': chr(10744),
                '\\': chr(10745),
                '|': chr(65372),  # Fullwidth vertical line
                '?': '？',  # Fullwidth question mark
                '*': '＊'   # Fullwidth asterisk
    }

    for disallowed, safe in replacements.items():
        filename = filename.replace(disallowed, safe)

    return filename

def dump_url(url, file_name=None):
    response = requests.get(url).json()
    if not file_name:
        file_name = safe_filename(url)
    
    output_file_name = f"{file_name}.json"
    output_file_path = os.path.join(save_dir_path, output_file_name)
    with open(output_file_path, 'w', encoding='utf-8') as f:
        json.dump(response, f)
    return response

categories_url = 'https://www.jobs.ch/fe/translations/?lang=en&namespaces=pools/categories_track,pools/employment_positions_track,pools/employment_types_track,pools/industries_track,pools/regions_track'
categories_file_name = 'categories'
dump_url(categories_url, categories_file_name)

categories = [100, 101, 102, 103, 104, 105, 106]
"""
json["categories_track.100"] = "Admin. / HR / Consulting / CEO";
json["categories_track.101"] = "Finance / Trusts / Real Estate";
json["categories_track.102"] = "Banking / Insurance";
json["categories_track.103"] = "Purchasing / Logistics / Trading";
json["categories_track.104"] = "Marketing / Communications / Editorial";
json["categories_track.105"] = "Sales / Customer Service / Admin.";
json["categories_track.106"] = "Information Technology / Telecom.";
json["categories_track.107"] = "Chemical / Pharma / Biotechnology";
json["categories_track.108"] = "Electronics / Engineering / Watches";
json["categories_track.109"] = "Machine / Plant Engin. / Manufacturing";
json["categories_track.110"] = "Construction / Architecture / Engineer";
json["categories_track.111"] = "Vehicles / Craft / Warehouse / Transport";
json["categories_track.112"] = "Medicine / Care / Therapy";
json["categories_track.113"] = "Public Admin. / Education / Social";
json["categories_track.114"] = "Catering / Food / Tourism";
json["categories_track.115"] = "Graphic Art / Typography / Printing";
json["categories_track.116"] = "Surveillance / Police / Customs / Rescue";
json["categories_track.117"] = "Sport / Spas&Wellness / Culture";
"""

wait_every = 100
wait_seconds = 10
wait_accumulate = 0
last_page = 100

for category in categories:
    page = 1
    url = f'https://www.jobs.ch/api/v1/public/search?category-ids%5B%5D={category}&page={page}&rows=20'
    response = dump_url(url)
    #while page <= response["num_pages"]:
    while response["current_page"] <= response["num_pages"]:
        if page == last_page:
            break
        page += 1
        wait_accumulate += 1
        if wait_accumulate == wait_every:
            print(f'Waiting {wait_seconds}')
            time.sleep(wait_seconds)
            wait_accumulate = 0
        url = f'https://www.jobs.ch/api/v1/public/search?category-ids%5B%5D={category}&page={page}&rows=20'
        print(url)
        response = dump_url(url)
