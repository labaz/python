import re
import json
import os
import datetime, time
import sqlite3
from dateutil import parser
import requests
from itertools import islice
from helper_functions import *

model = 'gpt-3.5-turbo-1106'

def batch(iterable, size):
    iterator = iter(iterable)
    for first in iterator:
        yield [first] + list(islice(iterator, size - 1))


def openai_process(role_system_prompt, role_user_prompt):
    # Join the job titles in the batch into a single string

    messages = [
        {"role": "system", "content": role_system_prompt},
        {"role": "user", "content": role_user_prompt},
    ]

    completion = openai_complete_chat(model, messages)

    return completion


current_file_dir = os.path.dirname(os.path.abspath(__file__))
subdir = 'openai_title_groups'
save_dir_path = os.path.join(current_file_dir, subdir)
os.makedirs(save_dir_path) if not os.path.exists(save_dir_path) else None

sql = 'select title from records order by job_id'
header, rows = get_sqlite_sql(db_file_name, sql)

batch_size = 40

# create a list of openai inputs
role_system_prompt = """You are a semantic text aggregation engine specializing in determining job groups in English language for white-collar job titles.
You receive a list of job titles, each title on the new line; the job titles may be in German, English, or French.
For each of the job titles, you translate it into English, if necessary. Then you determine the job group in English language. If you consider job titles semantically similar, they should belong to the same job group.
You output the results of the semantic text aggregation in JSON format. The JSON contains an array of N objects, each object corresponding to the job title supplied. The structure of a single object must be: {'job_title':'<original job title>', 'job_group':'<determined job group in English language>'}"""

openai_inputs = [
]
batch_number = 1
for i in range(0, len(rows), batch_size):
    role_user_prompt = '\n'.join(row[0] for row in rows[i:i + batch_size])
    result = {
        "batch_number": batch_number,
        "save_dir_path": save_dir_path,
        "role_system_prompt": role_system_prompt,
        "role_user_prompt": role_user_prompt
    }
    openai_inputs.append(result)
    batch_number += 1


def openai_wrapper(input):

    save_dir_path = input["save_dir_path"]
    batch_number = input["batch_number"]
    role_user_prompt = input["role_user_prompt"]
    role_system_prompt = input["role_system_prompt"]

    result = openai_process(role_system_prompt, role_user_prompt)
    
    file_name = f'openai_responses_{batch_number:05d}.json'
    file_path = os.path.join(save_dir_path, file_name)
    with open(file_path, 'w', encoding='utf-8') as f:
        json.dump(result, f)

    print(file_path)
    return


# Process the data in batches
parallel_max_workers = 24
timeout_seconds = 40

process_in_parallel(
    items=openai_inputs,
    function=openai_wrapper,
    batch_size=parallel_max_workers
)
