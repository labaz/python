import re
import json
import os
import datetime, time
import sqlite3
from dateutil import parser
import requests
from itertools import islice
from helper_functions import *

current_file_dir = os.path.dirname(os.path.abspath(__file__))
save_subdir = 'openai_title_groups'
save_dir_path = os.path.join(current_file_dir, save_subdir)

file_path_regex = r'openai_responses_[\d]{5}\.json'

# create sqlite table (job_description pk, job_group)
table_name = 'job_groups_aggregated'
name_datatype_mapping_regex = []
fields = ['job_title', 'job_group']
pk_field_name = fields[0]
drop_table_if_exists = True

create_sqlite_table(table_name=table_name, name_datatype_mapping_regex=name_datatype_mapping_regex, fields=fields, pk_field_name=pk_field_name, drop_table_if_exists=drop_table_if_exists)

# read all files and run insert_record_into_table
matching_files = [os.path.join(save_dir_path, filename) for filename in os.listdir(save_dir_path) if re.match(file_path_regex, filename)]

print_duration()

#matching_files = matching_files[0:1]

db_file_name = 'jobs.ch.sqlite'
array_key = 'job_results'
for file_path in matching_files:
    print_time()
    print(file_path)
    with open(file_path, 'r') as f:
        j = json.load(f)
    array_key = next(iter(j))
    elements = j[array_key]
    insert_multiple_records_into_table(elements, table_name, db_file_name)
    print_duration()
