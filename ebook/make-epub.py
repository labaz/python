import os
import re
from ebooklib import epub
import json

# access json attributes with . and not [""]
class JsonObject(dict):
    def __getattr__(self, name):
        return self.get(name, None)


def add_chapter_to_toc(toc, chapter, *sections):
    link = epub.Link(chapter.file_name, chapter.title, chapter.id)
    current_level = toc

    for section_name in sections:
        # Check if the section already exists
        for item in current_level:
            if isinstance(item, tuple) and isinstance(item[0], epub.Section) and item[0].title == section_name:
                current_level = item[1]
                break
        else:
            # If the section does not exist, create a new one
            new_section = epub.Section(section_name)
            new_tuple = (new_section, [])
            current_level.append(new_tuple)
            current_level = new_tuple[1]

    # Add the link to the appropriate section
    current_level.append(link)


def save_book(book, spine, toc, book_file_name):
    book.toc = tuple(toc)

    spine.insert(0, 'nav')
    book.spine = spine

    book.add_item(epub.EpubNcx())
    book.add_item(epub.EpubNav())

    script_dir = os.path.dirname(os.path.abspath(__file__))
    epub_path = os.path.join(script_dir, book_file_name)

    epub.write_epub(epub_path, book)
    print(epub_path)


def add_part_to_book(part, book, spine):

    part_number = len(spine) + 1

    author = part.get('author', '')
    group = part.get('group', '')
    source = part.get('source', '')
    file_name = part.get('file_name', '')
    author_years = part.get('author_years', '')
    title = part.get('title', '')
    date = part.get('date', '')
    text_html = part.get('text_html', '')
    sponsored = part.get('sponsored', '')
    readingtime = part.get('readingtime', '')

    sponsored_section = 'Пересказы за пэйволлом'

    part_author = author + f" ({author_years})" if author_years else ''

    part_title = part_author + " | " + title + f" ({date})" if date else ''

    part_source = re.sub(r' Нашли ошибку.*$', '', source)
    part_source = f"<p><i>{part_source}</i></p>"
    
    # make part html
    part_body = "\n".join(text_html)

    # remove picture
    part_body = re.sub(r'<picture.*?</picture>', '', part_body)

    part_body = f"<h1>{part_title}</h1><i>{readingtime}</i><br>{part_body}<br>{part_source}" 
    
    # save
    part_file_name = f"{part_number:03}-{file_name}.xhtml"

    chapter = epub.EpubHtml(title=title, file_name=part_file_name)
    chapter.content = part_body
    chapter.id = file_name

    # add index to the beginning
    spine.append(chapter)
    book.add_item(chapter)

    if (not sponsored):
        add_chapter_to_toc(toc, chapter, group, author)

    if (sponsored):
        add_chapter_to_toc(toc, chapter, sponsored_section, group, author)


def get_part_metadata(part, keys):
    part_metadata = {}
    for key in keys:
        if key in part:
            value = part[key]
            if isinstance(part[key], list): 
                value = "".join(part[key])
            part_metadata[key] = value
    return part_metadata


def get_distinct_values(list_of_dicts, attribute):
    return list({d[attribute] for d in list_of_dicts if attribute in d})


def lookup_by_key_value(list_of_dicts, key, value):
    return next((item for item in list_of_dicts if item.get(key) == value), None)


def filter_by_key_value(list_of_dicts, key, value):
    return [item for item in list_of_dicts if item.get(key) == value]


# let's make the book
parts_dir = '/home/i/sync-rw/briefly/json/'
extension = 'json'
parts_files = [file for file in os.listdir(parts_dir) if file.endswith(extension)]

parts = []

sort_keys = ['sponsored', 'group', 'author', 'title', 'url']
# normalize and get metadata from files, will be used for sorting

parts_metadata = []
for part_file in parts_files:
    part_file_path = os.path.join(parts_dir, part_file)
    try:
        with open(part_file_path, 'rb') as f:
            part = json.load(f)
        parts.append(part)
        parts_metadata.append(get_part_metadata(part, sort_keys))
    except (IOError, json.JSONDecodeError):
        continue

sorted_parts_metadata = sorted(parts_metadata, key=lambda x: (x.get('sponsored', False), x.get('group', 'ЯЯЯЯ'), x.get('author', 'ЯЯЯЯ')))

# break down file by groups
filter_column = 'group'
groups = get_distinct_values(sorted_parts_metadata, filter_column)

# print(groups)

for group in groups:
    book = epub.EpubBook()
    
    spine = []  # the sequence of files
    toc = []  # links and sections
    
    author = "briefly.ru"
    book_title = "Краткие пересказы"
    lang = "ru"
    
    book.set_title(book_title)
    book.add_author(author)
    book.set_language(lang)

    key_column = 'url'

    filtered_by_group = filter_by_key_value(sorted_parts_metadata, filter_column, group) 

    part_count = 0
    for part_key in filtered_by_group:
        key_value = part_key[key_column]
        part = lookup_by_key_value(parts, key_column, key_value)
        add_part_to_book(part, book, spine)
        part_count += 1

    book_file_name = f"{author} - {book_title} - {group} ({part_count}).epub"
    
    save_book(book, spine, toc, book_file_name)



#
# for part in sorted_parts:
#     add_part_to_book(part)
#
# save_book()
