import requests
#from dateutil import parser
from termcolor import colored
import json
from requests.models import Response
from pygments import highlight, lexers, formatters
import os, re, sys

# Set up YouTube API key and specify video link
api_key = os.environ.get('youtube_api_key')

if len(sys.argv) < 2:
  print("first arg must be link to video")
  sys.exit()
  
video_link = sys.argv[1]

script_dir = os.path.dirname(os.path.abspath(__file__))


def print_json(response):
    if isinstance(response, Response):
        parsed = response.json()
    elif isinstance(response, str):
        parsed = json.loads(response)
    elif isinstance(response, dict):
        parsed = response
    else:
        raise ValueError('Unsupported response type')

    colorful_json = highlight(
        json.dumps(parsed, indent=4),
        lexers.JsonLexer(),
        formatters.TerminalFormatter()
    )
    print(colorful_json)

def get_channel_from_video_link(video_link):
    print("video link: " + colored(video_link, 'magenta'))
    # Extract channel ID from video link
    video_id = video_link.split("v=")[1]
    url = f"https://www.googleapis.com/youtube/v3/videos?part=snippet&id={video_id}&key={api_key}"
    response = requests.get(url)
    j = response.json()
    channelId = j['items'][0]['snippet']['channelId']
    channelTitle = j['items'][0]['snippet']['channelTitle']
    print("channelTitle: " + colored(channelTitle, 'magenta'))
    print("channelId:    " + colored(channelId, 'magenta'))

    return channelId, channelTitle

channelId, channelTitle = get_channel_from_video_link(video_link)

# check if channel id file is present in script dir
cached_file_path = None
for _, _, filenames in os.walk(script_dir):
    for filename in filenames:
        if re.match(f'.*{channelId}.*',filename):
            cached_file_path = os.path.join(script_dir,filename)
            print("found cached result: " + colored(cached_file_path, 'green'))

results = []

if cached_file_path:
    with open(cached_file_path, 'r') as json_file:
        results = json.load(json_file)

if not(results):
    pageToken = ""
    i = 1
    while True:
        if i < 0: 
            break
        url = f"https://www.googleapis.com/youtube/v3/playlists?part=contentDetails,snippet&channelId={channelId}&key={api_key}&maxResults=50&pageToken={pageToken}"

        
        print("pageToken: "  + colored(pageToken, 'magenta'))
        print(colored(url,'yellow'))

        response = requests.get(url)

        j = response.json()

        totalResults = j['pageInfo']['totalResults']

        items = j['items']

        results.extend(items)

        print(f"Retrieved results: {len(results)}/{totalResults}")

        nextPageToken = response.json().get("nextPageToken")
        if not nextPageToken:
            break

        pageToken = nextPageToken

        i = i - 1

    filename = f'PLAYLISTS {channelTitle} [{channelId}].json'
    file_path = os.path.join(script_dir,filename)
    with open(file_path, 'w') as json_file:
        json.dump(results, json_file, indent=4)

############## parse
for item in results:
    title = item['snippet']['title']
    date = item['snippet']['publishedAt']
    #date = parser.parse(item['snippet']['publishedAt'], ignoretz=True).strftime('%Y-%m-%d %H:%M:%S')
    itemCount = item['contentDetails']['itemCount']
    id = item['id']
    print(
      colored(f'({itemCount})','cyan'),
      title,
      
      "\n\t", 
      colored(date,'magenta'),
      "\n\t", 
      colored(id, "green")
    )

